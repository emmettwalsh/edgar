import React from 'react';
import { Switch } from 'react-router-dom';

// tslint:disable-next-line:no-unused-variable
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ETLError from './etl-error';
import Sub from './sub';
import Num from './num';
import Pre from './pre';
import Tag from './tag';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}/etl-error`} component={ETLError} />
      <ErrorBoundaryRoute path={`${match.url}/sub`} component={Sub} />
      <ErrorBoundaryRoute path={`${match.url}/num`} component={Num} />
      <ErrorBoundaryRoute path={`${match.url}/pre`} component={Pre} />
      <ErrorBoundaryRoute path={`${match.url}/tag`} component={Tag} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
