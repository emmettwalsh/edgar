import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, byteSize } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './etl-error.reducer';
import { IETLError } from 'app/shared/model/etl-error.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IETLErrorDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ETLErrorDetail extends React.Component<IETLErrorDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { eTLErrorEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            ETLError [<b>{eTLErrorEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="domain">Domain</span>
            </dt>
            <dd>{eTLErrorEntity.domain}</dd>
            <dt>
              <span id="type">Type</span>
            </dt>
            <dd>{eTLErrorEntity.type}</dd>
            <dt>
              <span id="yearAndQuarter">Year And Quarter</span>
            </dt>
            <dd>{eTLErrorEntity.yearAndQuarter}</dd>
            <dt>
              <span id="input">Input</span>
            </dt>
            <dd>{eTLErrorEntity.input}</dd>
            <dt>
              <span id="output">Output</span>
            </dt>
            <dd>{eTLErrorEntity.output}</dd>
          </dl>
          <Button tag={Link} to="/entity/etl-error" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/etl-error/${eTLErrorEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ eTLError }: IRootState) => ({
  eTLErrorEntity: eTLError.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ETLErrorDetail);
