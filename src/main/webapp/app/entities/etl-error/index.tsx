import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import ETLError from './etl-error';
import ETLErrorDetail from './etl-error-detail';
import ETLErrorUpdate from './etl-error-update';
import ETLErrorDeleteDialog from './etl-error-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ETLErrorUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ETLErrorUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ETLErrorDetail} />
      <ErrorBoundaryRoute path={match.url} component={ETLError} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ETLErrorDeleteDialog} />
  </>
);

export default Routes;
