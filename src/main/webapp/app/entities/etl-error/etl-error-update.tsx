import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, setFileData, byteSize, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, setBlob, reset } from './etl-error.reducer';
import { IETLError } from 'app/shared/model/etl-error.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IETLErrorUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IETLErrorUpdateState {
  isNew: boolean;
}

export class ETLErrorUpdate extends React.Component<IETLErrorUpdateProps, IETLErrorUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  onBlobChange = (isAnImage, name) => event => {
    setFileData(event, (contentType, data) => this.props.setBlob(name, data, contentType), isAnImage);
  };

  clearBlob = name => () => {
    this.props.setBlob(name, undefined, undefined);
  };

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { eTLErrorEntity } = this.props;
      const entity = {
        ...eTLErrorEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/etl-error');
  };

  render() {
    const { eTLErrorEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    const { input, output } = eTLErrorEntity;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="edgarApp.eTLError.home.createOrEditLabel">Create or edit a ETLError</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : eTLErrorEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="etl-error-id">ID</Label>
                    <AvInput id="etl-error-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="domainLabel" for="etl-error-domain">
                    Domain
                  </Label>
                  <AvField
                    id="etl-error-domain"
                    type="text"
                    name="domain"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="typeLabel" for="etl-error-type">
                    Type
                  </Label>
                  <AvInput
                    id="etl-error-type"
                    type="select"
                    className="form-control"
                    name="type"
                    value={(!isNew && eTLErrorEntity.type) || 'Extract'}
                  >
                    <option value="Extract">Extract</option>
                    <option value="Transform">Transform</option>
                    <option value="Load">Load</option>
                    <option value="Other">Other</option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="yearAndQuarterLabel" for="etl-error-yearAndQuarter">
                    Year And Quarter
                  </Label>
                  <AvField
                    id="etl-error-yearAndQuarter"
                    type="text"
                    name="yearAndQuarter"
                    validate={{
                      maxLength: { value: 8, errorMessage: 'This field cannot be longer than 8 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="inputLabel" for="etl-error-input">
                    Input
                  </Label>
                  <AvInput id="etl-error-input" type="textarea" name="input" />
                </AvGroup>
                <AvGroup>
                  <Label id="outputLabel" for="etl-error-output">
                    Output
                  </Label>
                  <AvInput id="etl-error-output" type="textarea" name="output" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/etl-error" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  eTLErrorEntity: storeState.eTLError.entity,
  loading: storeState.eTLError.loading,
  updating: storeState.eTLError.updating,
  updateSuccess: storeState.eTLError.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  setBlob,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ETLErrorUpdate);
