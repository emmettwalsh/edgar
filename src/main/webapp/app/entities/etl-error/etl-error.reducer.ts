import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IETLError, defaultValue } from 'app/shared/model/etl-error.model';

export const ACTION_TYPES = {
  FETCH_ETLERROR_LIST: 'eTLError/FETCH_ETLERROR_LIST',
  FETCH_ETLERROR: 'eTLError/FETCH_ETLERROR',
  CREATE_ETLERROR: 'eTLError/CREATE_ETLERROR',
  UPDATE_ETLERROR: 'eTLError/UPDATE_ETLERROR',
  DELETE_ETLERROR: 'eTLError/DELETE_ETLERROR',
  SET_BLOB: 'eTLError/SET_BLOB',
  RESET: 'eTLError/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IETLError>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type ETLErrorState = Readonly<typeof initialState>;

// Reducer

export default (state: ETLErrorState = initialState, action): ETLErrorState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ETLERROR_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ETLERROR):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_ETLERROR):
    case REQUEST(ACTION_TYPES.UPDATE_ETLERROR):
    case REQUEST(ACTION_TYPES.DELETE_ETLERROR):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_ETLERROR_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ETLERROR):
    case FAILURE(ACTION_TYPES.CREATE_ETLERROR):
    case FAILURE(ACTION_TYPES.UPDATE_ETLERROR):
    case FAILURE(ACTION_TYPES.DELETE_ETLERROR):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_ETLERROR_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_ETLERROR):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_ETLERROR):
    case SUCCESS(ACTION_TYPES.UPDATE_ETLERROR):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_ETLERROR):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.SET_BLOB:
      const { name, data, contentType } = action.payload;
      return {
        ...state,
        entity: {
          ...state.entity,
          [name]: data,
          [name + 'ContentType']: contentType
        }
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/etl-errors';

// Actions

export const getEntities: ICrudGetAllAction<IETLError> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ETLERROR_LIST,
    payload: axios.get<IETLError>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IETLError> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ETLERROR,
    payload: axios.get<IETLError>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IETLError> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ETLERROR,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IETLError> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ETLERROR,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IETLError> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ETLERROR,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const setBlob = (name, data, contentType?) => ({
  type: ACTION_TYPES.SET_BLOB,
  payload: {
    name,
    data,
    contentType
  }
});

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
