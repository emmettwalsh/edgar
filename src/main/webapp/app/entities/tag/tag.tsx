import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './tag.reducer';
import { ITag } from 'app/shared/model/tag.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface ITagProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type ITagState = IPaginationBaseState;

export class Tag extends React.Component<ITagProps, ITagState> {
  state: ITagState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { tagList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="tag-heading">
          Tags
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Tag
          </Link>
        </h2>
        <div className="table-responsive">
          {tagList && tagList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    ID <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('tag')}>
                    Tag <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('version')}>
                    Version <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('custom')}>
                    Custom <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('abstrct')}>
                    Abstrct <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('datatype')}>
                    Datatype <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('iord')}>
                    Iord <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('crdr')}>
                    Crdr <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('tlabel')}>
                    Tlabel <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('doc')}>
                    Doc <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {tagList.map((tag, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${tag.id}`} color="link" size="sm">
                        {tag.id}
                      </Button>
                    </td>
                    <td>{tag.tag}</td>
                    <td>{tag.version}</td>
                    <td>{tag.custom ? 'true' : 'false'}</td>
                    <td>{tag.abstrct ? 'true' : 'false'}</td>
                    <td>{tag.datatype}</td>
                    <td>{tag.iord}</td>
                    <td>{tag.crdr}</td>
                    <td>{tag.tlabel}</td>
                    <td>{tag.doc}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${tag.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${tag.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${tag.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Tags found</div>
          )}
        </div>
        <div className={tagList && tagList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={this.state.activePage} total={totalItems} itemsPerPage={this.state.itemsPerPage} />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={this.state.activePage}
              onSelect={this.handlePagination}
              maxButtons={5}
              itemsPerPage={this.state.itemsPerPage}
              totalItems={this.props.totalItems}
            />
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ tag }: IRootState) => ({
  tagList: tag.entities,
  totalItems: tag.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Tag);
