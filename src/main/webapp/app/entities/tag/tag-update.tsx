import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './tag.reducer';
import { ITag } from 'app/shared/model/tag.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITagUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ITagUpdateState {
  isNew: boolean;
}

export class TagUpdate extends React.Component<ITagUpdateProps, ITagUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { tagEntity } = this.props;
      const entity = {
        ...tagEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/tag');
  };

  render() {
    const { tagEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="edgarApp.tag.home.createOrEditLabel">Create or edit a Tag</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : tagEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="tag-id">ID</Label>
                    <AvInput id="tag-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tagLabel" for="tag-tag">
                    Tag
                  </Label>
                  <AvField
                    id="tag-tag"
                    type="text"
                    name="tag"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 256, errorMessage: 'This field cannot be longer than 256 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="versionLabel" for="tag-version">
                    Version
                  </Label>
                  <AvField
                    id="tag-version"
                    type="text"
                    name="version"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 20, errorMessage: 'This field cannot be longer than 20 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="customLabel" check>
                    <AvInput id="tag-custom" type="checkbox" className="form-control" name="custom" />
                    Custom
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="abstrctLabel" check>
                    <AvInput id="tag-abstrct" type="checkbox" className="form-control" name="abstrct" />
                    Abstrct
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="datatypeLabel" for="tag-datatype">
                    Datatype
                  </Label>
                  <AvField
                    id="tag-datatype"
                    type="text"
                    name="datatype"
                    validate={{
                      maxLength: { value: 20, errorMessage: 'This field cannot be longer than 20 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="iordLabel" for="tag-iord">
                    Iord
                  </Label>
                  <AvField
                    id="tag-iord"
                    type="text"
                    name="iord"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 1, errorMessage: 'This field cannot be longer than 1 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="crdrLabel" for="tag-crdr">
                    Crdr
                  </Label>
                  <AvField
                    id="tag-crdr"
                    type="text"
                    name="crdr"
                    validate={{
                      maxLength: { value: 1, errorMessage: 'This field cannot be longer than 1 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="tlabelLabel" for="tag-tlabel">
                    Tlabel
                  </Label>
                  <AvField
                    id="tag-tlabel"
                    type="text"
                    name="tlabel"
                    validate={{
                      maxLength: { value: 512, errorMessage: 'This field cannot be longer than 512 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="docLabel" for="tag-doc">
                    Doc
                  </Label>
                  <AvField
                    id="tag-doc"
                    type="text"
                    name="doc"
                    validate={{
                      maxLength: { value: 2048, errorMessage: 'This field cannot be longer than 2048 characters.' }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/tag" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  tagEntity: storeState.tag.entity,
  loading: storeState.tag.loading,
  updating: storeState.tag.updating,
  updateSuccess: storeState.tag.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagUpdate);
