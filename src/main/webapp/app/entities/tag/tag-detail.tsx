import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './tag.reducer';
import { ITag } from 'app/shared/model/tag.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ITagDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class TagDetail extends React.Component<ITagDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { tagEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Tag [<b>{tagEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="tag">Tag</span>
            </dt>
            <dd>{tagEntity.tag}</dd>
            <dt>
              <span id="version">Version</span>
            </dt>
            <dd>{tagEntity.version}</dd>
            <dt>
              <span id="custom">Custom</span>
            </dt>
            <dd>{tagEntity.custom ? 'true' : 'false'}</dd>
            <dt>
              <span id="abstrct">Abstrct</span>
            </dt>
            <dd>{tagEntity.abstrct ? 'true' : 'false'}</dd>
            <dt>
              <span id="datatype">Datatype</span>
            </dt>
            <dd>{tagEntity.datatype}</dd>
            <dt>
              <span id="iord">Iord</span>
            </dt>
            <dd>{tagEntity.iord}</dd>
            <dt>
              <span id="crdr">Crdr</span>
            </dt>
            <dd>{tagEntity.crdr}</dd>
            <dt>
              <span id="tlabel">Tlabel</span>
            </dt>
            <dd>{tagEntity.tlabel}</dd>
            <dt>
              <span id="doc">Doc</span>
            </dt>
            <dd>{tagEntity.doc}</dd>
          </dl>
          <Button tag={Link} to="/entity/tag" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/tag/${tagEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ tag }: IRootState) => ({
  tagEntity: tag.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagDetail);
