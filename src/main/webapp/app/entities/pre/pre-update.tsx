import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ISub } from 'app/shared/model/sub.model';
import { getEntities as getSubs } from 'app/entities/sub/sub.reducer';
import { ITag } from 'app/shared/model/tag.model';
import { getEntities as getTags } from 'app/entities/tag/tag.reducer';
import { getEntity, updateEntity, createEntity, reset } from './pre.reducer';
import { IPre } from 'app/shared/model/pre.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPreUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPreUpdateState {
  isNew: boolean;
  adshId: string;
  tagId: string;
}

export class PreUpdate extends React.Component<IPreUpdateProps, IPreUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      adshId: '0',
      tagId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getSubs();
    this.props.getTags();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { preEntity } = this.props;
      const entity = {
        ...preEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/pre');
  };

  render() {
    const { preEntity, subs, tags, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="edgarApp.pre.home.createOrEditLabel">Create or edit a Pre</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : preEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="pre-id">ID</Label>
                    <AvInput id="pre-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="reportLabel" for="pre-report">
                    Report
                  </Label>
                  <AvField
                    id="pre-report"
                    type="string"
                    className="form-control"
                    name="report"
                    validate={{
                      max: { value: 999999, errorMessage: 'This field cannot be more than 999999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="lineLabel" for="pre-line">
                    Line
                  </Label>
                  <AvField
                    id="pre-line"
                    type="string"
                    className="form-control"
                    name="line"
                    validate={{
                      max: { value: 999999, errorMessage: 'This field cannot be more than 999999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="statementLabel" for="pre-statement">
                    Statement
                  </Label>
                  <AvField
                    id="pre-statement"
                    type="text"
                    name="statement"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 2, errorMessage: 'This field cannot be longer than 2 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="inpthLabel" check>
                    <AvInput id="pre-inpth" type="checkbox" className="form-control" name="inpth" />
                    Inpth
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="rfileLabel" for="pre-rfile">
                    Rfile
                  </Label>
                  <AvField
                    id="pre-rfile"
                    type="text"
                    name="rfile"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 1, errorMessage: 'This field cannot be longer than 1 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="plabelLabel" for="pre-plabel">
                    Plabel
                  </Label>
                  <AvField
                    id="pre-plabel"
                    type="text"
                    name="plabel"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 512, errorMessage: 'This field cannot be longer than 512 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="pre-adsh">Adsh</Label>
                  <AvInput id="pre-adsh" type="select" className="form-control" name="adshId">
                    <option value="" key="0" />
                    {subs
                      ? subs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="pre-tag">Tag</Label>
                  <AvInput id="pre-tag" type="select" className="form-control" name="tagId">
                    <option value="" key="0" />
                    {tags
                      ? tags.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/pre" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  subs: storeState.sub.entities,
  tags: storeState.tag.entities,
  preEntity: storeState.pre.entity,
  loading: storeState.pre.loading,
  updating: storeState.pre.updating,
  updateSuccess: storeState.pre.updateSuccess
});

const mapDispatchToProps = {
  getSubs,
  getTags,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreUpdate);
