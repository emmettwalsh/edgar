import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Pre from './pre';
import PreDetail from './pre-detail';
import PreUpdate from './pre-update';
import PreDeleteDialog from './pre-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PreUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PreUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PreDetail} />
      <ErrorBoundaryRoute path={match.url} component={Pre} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={PreDeleteDialog} />
  </>
);

export default Routes;
