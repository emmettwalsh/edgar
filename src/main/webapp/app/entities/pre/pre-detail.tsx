import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './pre.reducer';
import { IPre } from 'app/shared/model/pre.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPreDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PreDetail extends React.Component<IPreDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { preEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Pre [<b>{preEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="report">Report</span>
            </dt>
            <dd>{preEntity.report}</dd>
            <dt>
              <span id="line">Line</span>
            </dt>
            <dd>{preEntity.line}</dd>
            <dt>
              <span id="statement">Statement</span>
            </dt>
            <dd>{preEntity.statement}</dd>
            <dt>
              <span id="inpth">Inpth</span>
            </dt>
            <dd>{preEntity.inpth ? 'true' : 'false'}</dd>
            <dt>
              <span id="rfile">Rfile</span>
            </dt>
            <dd>{preEntity.rfile}</dd>
            <dt>
              <span id="plabel">Plabel</span>
            </dt>
            <dd>{preEntity.plabel}</dd>
            <dt>Adsh</dt>
            <dd>{preEntity.adshId ? preEntity.adshText : ''}</dd>
            <dt>Tag</dt>
            <dd>{preEntity.tagId ? preEntity.tagTag + ' ' + preEntity.tagVersion : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/pre" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/pre/${preEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ pre }: IRootState) => ({
  preEntity: pre.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreDetail);
