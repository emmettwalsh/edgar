import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPre, defaultValue } from 'app/shared/model/pre.model';

export const ACTION_TYPES = {
  FETCH_PRE_LIST: 'pre/FETCH_PRE_LIST',
  FETCH_PRE: 'pre/FETCH_PRE',
  CREATE_PRE: 'pre/CREATE_PRE',
  UPDATE_PRE: 'pre/UPDATE_PRE',
  DELETE_PRE: 'pre/DELETE_PRE',
  RESET: 'pre/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPre>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type PreState = Readonly<typeof initialState>;

// Reducer

export default (state: PreState = initialState, action): PreState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PRE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PRE):
    case REQUEST(ACTION_TYPES.UPDATE_PRE):
    case REQUEST(ACTION_TYPES.DELETE_PRE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PRE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PRE):
    case FAILURE(ACTION_TYPES.CREATE_PRE):
    case FAILURE(ACTION_TYPES.UPDATE_PRE):
    case FAILURE(ACTION_TYPES.DELETE_PRE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PRE):
    case SUCCESS(ACTION_TYPES.UPDATE_PRE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PRE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/pres';

// Actions

export const getEntities: ICrudGetAllAction<IPre> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PRE_LIST,
    payload: axios.get<IPre>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<IPre> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRE,
    payload: axios.get<IPre>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPre> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PRE,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPre> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PRE,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPre> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PRE,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
