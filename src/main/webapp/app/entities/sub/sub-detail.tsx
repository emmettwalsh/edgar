import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './sub.reducer';
import { ISub } from 'app/shared/model/sub.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ISubDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class SubDetail extends React.Component<ISubDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { subEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Sub [<b>{subEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="adsh">Adsh</span>
            </dt>
            <dd>{subEntity.adsh}</dd>
            <dt>
              <span id="cik">Cik</span>
            </dt>
            <dd>{subEntity.cik}</dd>
            <dt>
              <span id="name">Name</span>
            </dt>
            <dd>{subEntity.name}</dd>
            <dt>
              <span id="sic">Sic</span>
            </dt>
            <dd>{subEntity.sic}</dd>
            <dt>
              <span id="countryba">Countryba</span>
            </dt>
            <dd>{subEntity.countryba}</dd>
            <dt>
              <span id="stprba">Stprba</span>
            </dt>
            <dd>{subEntity.stprba}</dd>
            <dt>
              <span id="cityba">Cityba</span>
            </dt>
            <dd>{subEntity.cityba}</dd>
            <dt>
              <span id="zipba">Zipba</span>
            </dt>
            <dd>{subEntity.zipba}</dd>
            <dt>
              <span id="bas1">Bas 1</span>
            </dt>
            <dd>{subEntity.bas1}</dd>
            <dt>
              <span id="bas2">Bas 2</span>
            </dt>
            <dd>{subEntity.bas2}</dd>
            <dt>
              <span id="baph">Baph</span>
            </dt>
            <dd>{subEntity.baph}</dd>
            <dt>
              <span id="countryma">Countryma</span>
            </dt>
            <dd>{subEntity.countryma}</dd>
            <dt>
              <span id="stprma">Stprma</span>
            </dt>
            <dd>{subEntity.stprma}</dd>
            <dt>
              <span id="cityma">Cityma</span>
            </dt>
            <dd>{subEntity.cityma}</dd>
            <dt>
              <span id="zipma">Zipma</span>
            </dt>
            <dd>{subEntity.zipma}</dd>
            <dt>
              <span id="mas1">Mas 1</span>
            </dt>
            <dd>{subEntity.mas1}</dd>
            <dt>
              <span id="mas2">Mas 2</span>
            </dt>
            <dd>{subEntity.mas2}</dd>
            <dt>
              <span id="countryinc">Countryinc</span>
            </dt>
            <dd>{subEntity.countryinc}</dd>
            <dt>
              <span id="stprinc">Stprinc</span>
            </dt>
            <dd>{subEntity.stprinc}</dd>
            <dt>
              <span id="ein">Ein</span>
            </dt>
            <dd>{subEntity.ein}</dd>
            <dt>
              <span id="former">Former</span>
            </dt>
            <dd>{subEntity.former}</dd>
            <dt>
              <span id="changed">Changed</span>
            </dt>
            <dd>{subEntity.changed}</dd>
            <dt>
              <span id="afs">Afs</span>
            </dt>
            <dd>{subEntity.afs}</dd>
            <dt>
              <span id="wksi">Wksi</span>
            </dt>
            <dd>{subEntity.wksi ? 'true' : 'false'}</dd>
            <dt>
              <span id="fye">Fye</span>
            </dt>
            <dd>{subEntity.fye}</dd>
            <dt>
              <span id="form">Form</span>
            </dt>
            <dd>{subEntity.form}</dd>
            <dt>
              <span id="period">Period</span>
            </dt>
            <dd>
              <TextFormat value={subEntity.period} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="fy">Fy</span>
            </dt>
            <dd>{subEntity.fy}</dd>
            <dt>
              <span id="fp">Fp</span>
            </dt>
            <dd>{subEntity.fp}</dd>
            <dt>
              <span id="filed">Filed</span>
            </dt>
            <dd>
              <TextFormat value={subEntity.filed} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="accepted">Accepted</span>
            </dt>
            <dd>
              <TextFormat value={subEntity.accepted} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="prevrpt">Prevrpt</span>
            </dt>
            <dd>{subEntity.prevrpt ? 'true' : 'false'}</dd>
            <dt>
              <span id="detail">Detail</span>
            </dt>
            <dd>{subEntity.detail ? 'true' : 'false'}</dd>
            <dt>
              <span id="instance">Instance</span>
            </dt>
            <dd>{subEntity.instance}</dd>
            <dt>
              <span id="nciks">Nciks</span>
            </dt>
            <dd>{subEntity.nciks}</dd>
            <dt>
              <span id="aciks">Aciks</span>
            </dt>
            <dd>{subEntity.aciks}</dd>
            <dt>
              <span id="yearAndQuarter">Year And Quarter</span>
            </dt>
            <dd>{subEntity.yearAndQuarter}</dd>
          </dl>
          <Button tag={Link} to="/entity/sub" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/sub/${subEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ sub }: IRootState) => ({
  subEntity: sub.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubDetail);
