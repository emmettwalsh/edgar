import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './sub.reducer';
import { ISub } from 'app/shared/model/sub.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface ISubProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type ISubState = IPaginationBaseState;

export class Sub extends React.Component<ISubProps, ISubState> {
  state: ISubState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { subList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="sub-heading">
          Subs
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Sub
          </Link>
        </h2>
        <div className="table-responsive">
          {subList && subList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    ID <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('yearAndQuarter')}>
                    Year And Quarter <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('adsh')}>
                    Adsh <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('cik')}>
                    Cik <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('name')}>
                    Name <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('sic')}>
                    Sic <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('countryba')}>
                    Countryba <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('stprba')}>
                    Stprba <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('cityba')}>
                    Cityba <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('zipba')}>
                    Zipba <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('bas1')}>
                    Bas 1 <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('bas2')}>
                    Bas 2 <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('baph')}>
                    Baph <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('countryma')}>
                    Countryma <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('stprma')}>
                    Stprma <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('cityma')}>
                    Cityma <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('zipma')}>
                    Zipma <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('mas1')}>
                    Mas 1 <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('mas2')}>
                    Mas 2 <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('countryinc')}>
                    Countryinc <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('stprinc')}>
                    Stprinc <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('ein')}>
                    Ein <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('former')}>
                    Former <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('changed')}>
                    Changed <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('afs')}>
                    Afs <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('wksi')}>
                    Wksi <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('fye')}>
                    Fye <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('form')}>
                    Form <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('period')}>
                    Period <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('fy')}>
                    Fy <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('fp')}>
                    Fp <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('filed')}>
                    Filed <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('accepted')}>
                    Accepted <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('prevrpt')}>
                    Prevrpt <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('detail')}>
                    Detail <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('instance')}>
                    Instance <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('nciks')}>
                    Nciks <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('aciks')}>
                    Aciks <FontAwesomeIcon icon="sort" />
                  </th>

                  <th />
                </tr>
              </thead>
              <tbody>
                {subList.map((sub, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${sub.id}`} color="link" size="sm">
                        {sub.id}
                      </Button>
                    </td>
                    <td>{sub.yearAndQuarter}</td>
                    <td>{sub.adsh}</td>
                    <td>{sub.cik}</td>
                    <td>{sub.name}</td>
                    <td>{sub.sic}</td>
                    <td>{sub.countryba}</td>
                    <td>{sub.stprba}</td>
                    <td>{sub.cityba}</td>
                    <td>{sub.zipba}</td>
                    <td>{sub.bas1}</td>
                    <td>{sub.bas2}</td>
                    <td>{sub.baph}</td>
                    <td>{sub.countryma}</td>
                    <td>{sub.stprma}</td>
                    <td>{sub.cityma}</td>
                    <td>{sub.zipma}</td>
                    <td>{sub.mas1}</td>
                    <td>{sub.mas2}</td>
                    <td>{sub.countryinc}</td>
                    <td>{sub.stprinc}</td>
                    <td>{sub.ein}</td>
                    <td>{sub.former}</td>
                    <td>{sub.changed}</td>
                    <td>{sub.afs}</td>
                    <td>{sub.wksi ? 'true' : 'false'}</td>
                    <td>{sub.fye}</td>
                    <td>{sub.form}</td>
                    <td>
                      <TextFormat type="date" value={sub.period} format={APP_DATE_FORMAT} />
                    </td>
                    <td>{sub.fy}</td>
                    <td>{sub.fp}</td>
                    <td>
                      <TextFormat type="date" value={sub.filed} format={APP_DATE_FORMAT} />
                    </td>
                    <td>
                      <TextFormat type="date" value={sub.accepted} format={APP_DATE_FORMAT} />
                    </td>
                    <td>{sub.prevrpt ? 'true' : 'false'}</td>
                    <td>{sub.detail ? 'true' : 'false'}</td>
                    <td>{sub.instance}</td>
                    <td>{sub.nciks}</td>
                    <td>{sub.aciks}</td>

                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${sub.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${sub.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${sub.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Subs found</div>
          )}
        </div>
        <div className={subList && subList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={this.state.activePage} total={totalItems} itemsPerPage={this.state.itemsPerPage} />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={this.state.activePage}
              onSelect={this.handlePagination}
              maxButtons={5}
              itemsPerPage={this.state.itemsPerPage}
              totalItems={this.props.totalItems}
            />
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ sub }: IRootState) => ({
  subList: sub.entities,
  totalItems: sub.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Sub);
