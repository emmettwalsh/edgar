import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './sub.reducer';
import { ISub } from 'app/shared/model/sub.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ISubUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ISubUpdateState {
  isNew: boolean;
}

export class SubUpdate extends React.Component<ISubUpdateProps, ISubUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    values.period = convertDateTimeToServer(values.period);
    values.filed = convertDateTimeToServer(values.filed);
    values.accepted = convertDateTimeToServer(values.accepted);

    if (errors.length === 0) {
      const { subEntity } = this.props;
      const entity = {
        ...subEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/sub');
  };

  render() {
    const { subEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="edgarApp.sub.home.createOrEditLabel">Create or edit a Sub</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : subEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="sub-id">ID</Label>
                    <AvInput id="sub-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="adshLabel" for="sub-adsh">
                    Adsh
                  </Label>
                  <AvField
                    id="sub-adsh"
                    type="text"
                    name="adsh"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 20, errorMessage: 'This field cannot be longer than 20 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="cikLabel" for="sub-cik">
                    Cik
                  </Label>
                  <AvField
                    id="sub-cik"
                    type="string"
                    className="form-control"
                    name="cik"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      max: { value: 9999999999, errorMessage: 'This field cannot be more than 9999999999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="nameLabel" for="sub-name">
                    Name
                  </Label>
                  <AvField
                    id="sub-name"
                    type="text"
                    name="name"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 150, errorMessage: 'This field cannot be longer than 150 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="sicLabel" for="sub-sic">
                    Sic
                  </Label>
                  <AvField
                    id="sub-sic"
                    type="string"
                    className="form-control"
                    name="sic"
                    validate={{
                      max: { value: 9999, errorMessage: 'This field cannot be more than 9999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="countrybaLabel" for="sub-countryba">
                    Countryba
                  </Label>
                  <AvField
                    id="sub-countryba"
                    type="text"
                    name="countryba"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 2, errorMessage: 'This field cannot be longer than 2 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="stprbaLabel" for="sub-stprba">
                    Stprba
                  </Label>
                  <AvField
                    id="sub-stprba"
                    type="text"
                    name="stprba"
                    validate={{
                      maxLength: { value: 2, errorMessage: 'This field cannot be longer than 2 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="citybaLabel" for="sub-cityba">
                    Cityba
                  </Label>
                  <AvField
                    id="sub-cityba"
                    type="text"
                    name="cityba"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 30, errorMessage: 'This field cannot be longer than 30 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="zipbaLabel" for="sub-zipba">
                    Zipba
                  </Label>
                  <AvField
                    id="sub-zipba"
                    type="text"
                    name="zipba"
                    validate={{
                      maxLength: { value: 10, errorMessage: 'This field cannot be longer than 10 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="bas1Label" for="sub-bas1">
                    Bas 1
                  </Label>
                  <AvField
                    id="sub-bas1"
                    type="text"
                    name="bas1"
                    validate={{
                      maxLength: { value: 40, errorMessage: 'This field cannot be longer than 40 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="bas2Label" for="sub-bas2">
                    Bas 2
                  </Label>
                  <AvField
                    id="sub-bas2"
                    type="text"
                    name="bas2"
                    validate={{
                      maxLength: { value: 40, errorMessage: 'This field cannot be longer than 40 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="baphLabel" for="sub-baph">
                    Baph
                  </Label>
                  <AvField
                    id="sub-baph"
                    type="text"
                    name="baph"
                    validate={{
                      maxLength: { value: 12, errorMessage: 'This field cannot be longer than 12 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="countrymaLabel" for="sub-countryma">
                    Countryma
                  </Label>
                  <AvField
                    id="sub-countryma"
                    type="text"
                    name="countryma"
                    validate={{
                      maxLength: { value: 2, errorMessage: 'This field cannot be longer than 2 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="stprmaLabel" for="sub-stprma">
                    Stprma
                  </Label>
                  <AvField
                    id="sub-stprma"
                    type="text"
                    name="stprma"
                    validate={{
                      maxLength: { value: 2, errorMessage: 'This field cannot be longer than 2 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="citymaLabel" for="sub-cityma">
                    Cityma
                  </Label>
                  <AvField
                    id="sub-cityma"
                    type="text"
                    name="cityma"
                    validate={{
                      maxLength: { value: 30, errorMessage: 'This field cannot be longer than 30 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="zipmaLabel" for="sub-zipma">
                    Zipma
                  </Label>
                  <AvField
                    id="sub-zipma"
                    type="text"
                    name="zipma"
                    validate={{
                      maxLength: { value: 10, errorMessage: 'This field cannot be longer than 10 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="mas1Label" for="sub-mas1">
                    Mas 1
                  </Label>
                  <AvField
                    id="sub-mas1"
                    type="text"
                    name="mas1"
                    validate={{
                      maxLength: { value: 40, errorMessage: 'This field cannot be longer than 40 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="mas2Label" for="sub-mas2">
                    Mas 2
                  </Label>
                  <AvField
                    id="sub-mas2"
                    type="text"
                    name="mas2"
                    validate={{
                      maxLength: { value: 40, errorMessage: 'This field cannot be longer than 40 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="countryincLabel" for="sub-countryinc">
                    Countryinc
                  </Label>
                  <AvField
                    id="sub-countryinc"
                    type="text"
                    name="countryinc"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 3, errorMessage: 'This field cannot be longer than 3 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="stprincLabel" for="sub-stprinc">
                    Stprinc
                  </Label>
                  <AvField
                    id="sub-stprinc"
                    type="text"
                    name="stprinc"
                    validate={{
                      maxLength: { value: 2, errorMessage: 'This field cannot be longer than 2 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="einLabel" for="sub-ein">
                    Ein
                  </Label>
                  <AvField
                    id="sub-ein"
                    type="string"
                    className="form-control"
                    name="ein"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      max: { value: 9999999999, errorMessage: 'This field cannot be more than 9999999999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="formerLabel" for="sub-former">
                    Former
                  </Label>
                  <AvField
                    id="sub-former"
                    type="text"
                    name="former"
                    validate={{
                      maxLength: { value: 150, errorMessage: 'This field cannot be longer than 150 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="changedLabel" for="sub-changed">
                    Changed
                  </Label>
                  <AvField
                    id="sub-changed"
                    type="text"
                    name="changed"
                    validate={{
                      maxLength: { value: 8, errorMessage: 'This field cannot be longer than 8 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="afsLabel" for="sub-afs">
                    Afs
                  </Label>
                  <AvField
                    id="sub-afs"
                    type="text"
                    name="afs"
                    validate={{
                      maxLength: { value: 5, errorMessage: 'This field cannot be longer than 5 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="wksiLabel" check>
                    <AvInput id="sub-wksi" type="checkbox" className="form-control" name="wksi" />
                    Wksi
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="fyeLabel" for="sub-fye">
                    Fye
                  </Label>
                  <AvField
                    id="sub-fye"
                    type="text"
                    name="fye"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 4, errorMessage: 'This field cannot be longer than 4 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="formLabel" for="sub-form">
                    Form
                  </Label>
                  <AvField
                    id="sub-form"
                    type="text"
                    name="form"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 10, errorMessage: 'This field cannot be longer than 10 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="periodLabel" for="sub-period">
                    Period
                  </Label>
                  <AvInput
                    id="sub-period"
                    type="datetime-local"
                    className="form-control"
                    name="period"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.subEntity.period)}
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="fyLabel" for="sub-fy">
                    Fy
                  </Label>
                  <AvField
                    id="sub-fy"
                    type="string"
                    className="form-control"
                    name="fy"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      max: { value: 9999, errorMessage: 'This field cannot be more than 9999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="fpLabel" for="sub-fp">
                    Fp
                  </Label>
                  <AvField
                    id="sub-fp"
                    type="text"
                    name="fp"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 2, errorMessage: 'This field cannot be longer than 2 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="filedLabel" for="sub-filed">
                    Filed
                  </Label>
                  <AvInput
                    id="sub-filed"
                    type="datetime-local"
                    className="form-control"
                    name="filed"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.subEntity.filed)}
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="acceptedLabel" for="sub-accepted">
                    Accepted
                  </Label>
                  <AvInput
                    id="sub-accepted"
                    type="datetime-local"
                    className="form-control"
                    name="accepted"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.subEntity.accepted)}
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="prevrptLabel" check>
                    <AvInput id="sub-prevrpt" type="checkbox" className="form-control" name="prevrpt" />
                    Prevrpt
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="detailLabel" check>
                    <AvInput id="sub-detail" type="checkbox" className="form-control" name="detail" />
                    Detail
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="instanceLabel" for="sub-instance">
                    Instance
                  </Label>
                  <AvField
                    id="sub-instance"
                    type="text"
                    name="instance"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 32, errorMessage: 'This field cannot be longer than 32 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="nciksLabel" for="sub-nciks">
                    Nciks
                  </Label>
                  <AvField
                    id="sub-nciks"
                    type="string"
                    className="form-control"
                    name="nciks"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      max: { value: 9999, errorMessage: 'This field cannot be more than 9999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="aciksLabel" for="sub-aciks">
                    Aciks
                  </Label>
                  <AvField
                    id="sub-aciks"
                    type="text"
                    name="aciks"
                    validate={{
                      maxLength: { value: 120, errorMessage: 'This field cannot be longer than 120 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="yearAndQuarterLabel" for="sub-yearAndQuarter">
                    Year And Quarter
                  </Label>
                  <AvField
                    id="sub-yearAndQuarter"
                    type="text"
                    name="yearAndQuarter"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 6, errorMessage: 'This field cannot be longer than 6 characters.' }
                    }}
                  />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/sub" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  subEntity: storeState.sub.entity,
  loading: storeState.sub.loading,
  updating: storeState.sub.updating,
  updateSuccess: storeState.sub.updateSuccess
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubUpdate);
