import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { INum, defaultValue } from 'app/shared/model/num.model';

export const ACTION_TYPES = {
  FETCH_NUM_LIST: 'num/FETCH_NUM_LIST',
  FETCH_NUM: 'num/FETCH_NUM',
  CREATE_NUM: 'num/CREATE_NUM',
  UPDATE_NUM: 'num/UPDATE_NUM',
  DELETE_NUM: 'num/DELETE_NUM',
  RESET: 'num/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<INum>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false
};

export type NumState = Readonly<typeof initialState>;

// Reducer

export default (state: NumState = initialState, action): NumState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_NUM_LIST):
    case REQUEST(ACTION_TYPES.FETCH_NUM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_NUM):
    case REQUEST(ACTION_TYPES.UPDATE_NUM):
    case REQUEST(ACTION_TYPES.DELETE_NUM):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_NUM_LIST):
    case FAILURE(ACTION_TYPES.FETCH_NUM):
    case FAILURE(ACTION_TYPES.CREATE_NUM):
    case FAILURE(ACTION_TYPES.UPDATE_NUM):
    case FAILURE(ACTION_TYPES.DELETE_NUM):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_NUM_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10)
      };
    case SUCCESS(ACTION_TYPES.FETCH_NUM):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_NUM):
    case SUCCESS(ACTION_TYPES.UPDATE_NUM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_NUM):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/nums';

// Actions

export const getEntities: ICrudGetAllAction<INum> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_NUM_LIST,
    payload: axios.get<INum>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`)
  };
};

export const getEntity: ICrudGetAction<INum> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_NUM,
    payload: axios.get<INum>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<INum> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_NUM,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<INum> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_NUM,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<INum> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_NUM,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
