import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat, getSortState, IPaginationBaseState, JhiPagination, JhiItemCount } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './num.reducer';
import { INum } from 'app/shared/model/num.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ITEMS_PER_PAGE } from 'app/shared/util/pagination.constants';

export interface INumProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export type INumState = IPaginationBaseState;

export class Num extends React.Component<INumProps, INumState> {
  state: INumState = {
    ...getSortState(this.props.location, ITEMS_PER_PAGE)
  };

  componentDidMount() {
    this.getEntities();
  }

  sort = prop => () => {
    this.setState(
      {
        order: this.state.order === 'asc' ? 'desc' : 'asc',
        sort: prop
      },
      () => this.sortEntities()
    );
  };

  sortEntities() {
    this.getEntities();
    this.props.history.push(`${this.props.location.pathname}?page=${this.state.activePage}&sort=${this.state.sort},${this.state.order}`);
  }

  handlePagination = activePage => this.setState({ activePage }, () => this.sortEntities());

  getEntities = () => {
    const { activePage, itemsPerPage, sort, order } = this.state;
    this.props.getEntities(activePage - 1, itemsPerPage, `${sort},${order}`);
  };

  render() {
    const { numList, match, totalItems } = this.props;
    return (
      <div>
        <h2 id="num-heading">
          Nums
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Num
          </Link>
        </h2>
        <div className="table-responsive">
          {numList && numList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th className="hand" onClick={this.sort('id')}>
                    ID <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('coreg')}>
                    Coreg <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('ddate')}>
                    Ddate <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('qtrs')}>
                    Qtrs <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('uom')}>
                    Uom <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('value')}>
                    Value <FontAwesomeIcon icon="sort" />
                  </th>
                  <th className="hand" onClick={this.sort('footnote')}>
                    Footnote <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    Adsh <FontAwesomeIcon icon="sort" />
                  </th>
                  <th>
                    Tag <FontAwesomeIcon icon="sort" />
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {numList.map((num, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${num.id}`} color="link" size="sm">
                        {num.id}
                      </Button>
                    </td>
                    <td>{num.coreg}</td>
                    <td>
                      <TextFormat type="date" value={num.ddate} format={APP_DATE_FORMAT} />
                    </td>
                    <td>{num.qtrs}</td>
                    <td>{num.uom}</td>
                    <td>{num.value}</td>
                    <td>{num.footnote}</td>
                    <td>{num.adshId ? <Link to={`sub/${num.adshId}`}>{num.adshText}</Link> : ''}</td>
                    <td>
                      {num.tagId ? (
                        <Link to={`tag/${num.tagId}`}>
                          {num.tagTag} {num.tagVersion}
                        </Link>
                      ) : (
                        ''
                      )}
                    </td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${num.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${num.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${num.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Nums found</div>
          )}
        </div>
        <div className={numList && numList.length > 0 ? '' : 'd-none'}>
          <Row className="justify-content-center">
            <JhiItemCount page={this.state.activePage} total={totalItems} itemsPerPage={this.state.itemsPerPage} />
          </Row>
          <Row className="justify-content-center">
            <JhiPagination
              activePage={this.state.activePage}
              onSelect={this.handlePagination}
              maxButtons={5}
              itemsPerPage={this.state.itemsPerPage}
              totalItems={this.props.totalItems}
            />
          </Row>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ num }: IRootState) => ({
  numList: num.entities,
  totalItems: num.totalItems
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Num);
