import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { ISub } from 'app/shared/model/sub.model';
import { getEntities as getSubs } from 'app/entities/sub/sub.reducer';
import { ITag } from 'app/shared/model/tag.model';
import { getEntities as getTags } from 'app/entities/tag/tag.reducer';
import { getEntity, updateEntity, createEntity, reset } from './num.reducer';
import { INum } from 'app/shared/model/num.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface INumUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface INumUpdateState {
  isNew: boolean;
  adshId: string;
  tagId: string;
}

export class NumUpdate extends React.Component<INumUpdateProps, INumUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      adshId: '0',
      tagId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getSubs();
    this.props.getTags();
  }

  saveEntity = (event, errors, values) => {
    values.ddate = convertDateTimeToServer(values.ddate);

    if (errors.length === 0) {
      const { numEntity } = this.props;
      const entity = {
        ...numEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/num');
  };

  render() {
    const { numEntity, subs, tags, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="edgarApp.num.home.createOrEditLabel">Create or edit a Num</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : numEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="num-id">ID</Label>
                    <AvInput id="num-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="coregLabel" for="num-coreg">
                    Coreg
                  </Label>
                  <AvField
                    id="num-coreg"
                    type="string"
                    className="form-control"
                    name="coreg"
                    validate={{
                      max: { value: 9999, errorMessage: 'This field cannot be more than 9999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="ddateLabel" for="num-ddate">
                    Ddate
                  </Label>
                  <AvInput
                    id="num-ddate"
                    type="datetime-local"
                    className="form-control"
                    name="ddate"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.numEntity.ddate)}
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="qtrsLabel" for="num-qtrs">
                    Qtrs
                  </Label>
                  <AvField
                    id="num-qtrs"
                    type="string"
                    className="form-control"
                    name="qtrs"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      max: { value: 99999999, errorMessage: 'This field cannot be more than 99999999.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="uomLabel" for="num-uom">
                    Uom
                  </Label>
                  <AvField
                    id="num-uom"
                    type="text"
                    name="uom"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      maxLength: { value: 20, errorMessage: 'This field cannot be longer than 20 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="valueLabel" for="num-value">
                    Value
                  </Label>
                  <AvField
                    id="num-value"
                    type="text"
                    name="value"
                    validate={{
                      required: { value: true, errorMessage: 'This field is required.' },
                      max: { value: 1e30, errorMessage: 'This field cannot be more than 1e+30.' },
                      number: { value: true, errorMessage: 'This field should be a number.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="footnoteLabel" for="num-footnote">
                    Footnote
                  </Label>
                  <AvField
                    id="num-footnote"
                    type="text"
                    name="footnote"
                    validate={{
                      maxLength: { value: 512, errorMessage: 'This field cannot be longer than 512 characters.' }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="num-adsh">Adsh</Label>
                  <AvInput id="num-adsh" type="select" className="form-control" name="adshId">
                    <option value="" key="0" />
                    {subs
                      ? subs.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="num-tag">Tag</Label>
                  <AvInput id="num-tag" type="select" className="form-control" name="tagId">
                    <option value="" key="0" />
                    {tags
                      ? tags.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/num" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  subs: storeState.sub.entities,
  tags: storeState.tag.entities,
  numEntity: storeState.num.entity,
  loading: storeState.num.loading,
  updating: storeState.num.updating,
  updateSuccess: storeState.num.updateSuccess
});

const mapDispatchToProps = {
  getSubs,
  getTags,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NumUpdate);
