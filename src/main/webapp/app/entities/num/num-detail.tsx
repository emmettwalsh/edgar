import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './num.reducer';
import { INum } from 'app/shared/model/num.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface INumDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class NumDetail extends React.Component<INumDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { numEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Num [<b>{numEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="coreg">Coreg</span>
            </dt>
            <dd>{numEntity.coreg}</dd>
            <dt>
              <span id="ddate">Ddate</span>
            </dt>
            <dd>
              <TextFormat value={numEntity.ddate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="qtrs">Qtrs</span>
            </dt>
            <dd>{numEntity.qtrs}</dd>
            <dt>
              <span id="uom">Uom</span>
            </dt>
            <dd>{numEntity.uom}</dd>
            <dt>
              <span id="value">Value</span>
            </dt>
            <dd>{numEntity.value}</dd>
            <dt>
              <span id="footnote">Footnote</span>
            </dt>
            <dd>{numEntity.footnote}</dd>
            <dt>Adsh</dt>
            <dd>{numEntity.adshId ? numEntity.adshText : ''}</dd>
            <dt>Tag</dt>
            <dd>{numEntity.tagId ? numEntity.tagTag + ' ' + numEntity.tagVersion : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/num" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/num/${numEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ num }: IRootState) => ({
  numEntity: num.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NumDetail);
