import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Num from './num';
import NumDetail from './num-detail';
import NumUpdate from './num-update';
import NumDeleteDialog from './num-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={NumUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={NumUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={NumDetail} />
      <ErrorBoundaryRoute path={match.url} component={Num} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={NumDeleteDialog} />
  </>
);

export default Routes;
