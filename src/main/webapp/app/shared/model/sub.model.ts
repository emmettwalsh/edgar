import { Moment } from 'moment';

export interface ISub {
  id?: number;
  adsh?: string;
  cik?: number;
  name?: string;
  sic?: number;
  countryba?: string;
  stprba?: string;
  cityba?: string;
  zipba?: string;
  bas1?: string;
  bas2?: string;
  baph?: string;
  countryma?: string;
  stprma?: string;
  cityma?: string;
  zipma?: string;
  mas1?: string;
  mas2?: string;
  countryinc?: string;
  stprinc?: string;
  ein?: number;
  former?: string;
  changed?: string;
  afs?: string;
  wksi?: boolean;
  fye?: string;
  form?: string;
  period?: Moment;
  fy?: number;
  fp?: string;
  filed?: Moment;
  accepted?: Moment;
  prevrpt?: boolean;
  detail?: boolean;
  instance?: string;
  nciks?: number;
  aciks?: string;
  yearAndQuarter?: string;
}

export const defaultValue: Readonly<ISub> = {
  wksi: false,
  prevrpt: false,
  detail: false
};
