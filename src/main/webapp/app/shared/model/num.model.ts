import { Moment } from 'moment';

export interface INum {
  id?: number;
  coreg?: number;
  ddate?: Moment;
  qtrs?: number;
  uom?: string;
  value?: number;
  footnote?: string;
  adshId?: number;
  adshText?: string;
  tagId?: number;
  tagTag?: string;
  tagVersion?: string;
}

export const defaultValue: Readonly<INum> = {};
