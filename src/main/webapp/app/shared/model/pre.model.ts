export interface IPre {
  id?: number;
  report?: number;
  line?: number;
  statement?: string;
  inpth?: boolean;
  rfile?: string;
  plabel?: string;
  adshId?: number;
  adshText?: string;
  tagId?: number;
  tagTag?: string;
  tagVersion?: string;
}

export const defaultValue: Readonly<IPre> = {
  inpth: false
};
