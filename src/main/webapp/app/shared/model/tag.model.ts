export interface ITag {
  id?: number;
  tag?: string;
  version?: string;
  custom?: boolean;
  abstrct?: boolean;
  datatype?: string;
  iord?: string;
  crdr?: string;
  tlabel?: string;
  doc?: string;
}

export const defaultValue: Readonly<ITag> = {
  custom: false,
  abstrct: false
};
