export const enum EtlErrorType {
  Extract = 'Extract',
  Transform = 'Transform',
  Load = 'Load',
  Other = 'Other'
}

export interface IETLError {
  id?: number;
  domain?: string;
  type?: EtlErrorType;
  yearAndQuarter?: string;
  input?: any;
  output?: any;
}

export const defaultValue: Readonly<IETLError> = {};
