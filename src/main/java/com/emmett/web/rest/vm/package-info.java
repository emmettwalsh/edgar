/**
 * View Models used by Spring MVC REST controllers.
 */
package com.emmett.web.rest.vm;
