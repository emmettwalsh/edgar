package com.emmett.web.rest;

import com.emmett.service.SubService;
import com.emmett.web.rest.errors.BadRequestAlertException;
import com.emmett.service.dto.SubDTO;
import com.emmett.service.dto.SubCriteria;
import com.emmett.service.SubQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.emmett.domain.Sub}.
 */
@RestController
@RequestMapping("/api")
public class SubResource {

    private final Logger log = LoggerFactory.getLogger(SubResource.class);

    private static final String ENTITY_NAME = "sub";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SubService subService;

    private final SubQueryService subQueryService;

    public SubResource(SubService subService, SubQueryService subQueryService) {
        this.subService = subService;
        this.subQueryService = subQueryService;
    }

    /**
     * {@code POST  /subs} : Create a new sub.
     *
     * @param subDTO the subDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subDTO, or with status {@code 400 (Bad Request)} if the sub has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/subs")
    public ResponseEntity<SubDTO> createSub(@Valid @RequestBody SubDTO subDTO) throws URISyntaxException {
        log.debug("REST request to save Sub : {}", subDTO);
        if (subDTO.getId() != null) {
            throw new BadRequestAlertException("A new sub cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SubDTO result = subService.save(subDTO);
        return ResponseEntity.created(new URI("/api/subs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /subs} : Updates an existing sub.
     *
     * @param subDTO the subDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subDTO,
     * or with status {@code 400 (Bad Request)} if the subDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/subs")
    public ResponseEntity<SubDTO> updateSub(@Valid @RequestBody SubDTO subDTO) throws URISyntaxException {
        log.debug("REST request to update Sub : {}", subDTO);
        if (subDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SubDTO result = subService.save(subDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, subDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /subs} : get all the subs.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subs in body.
     */
    @GetMapping("/subs")
    public ResponseEntity<List<SubDTO>> getAllSubs(SubCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Subs by criteria: {}", criteria);
        Page<SubDTO> page = subQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /subs/count} : count all the subs.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/subs/count")
    public ResponseEntity<Long> countSubs(SubCriteria criteria) {
        log.debug("REST request to count Subs by criteria: {}", criteria);
        return ResponseEntity.ok().body(subQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /subs/:id} : get the "id" sub.
     *
     * @param id the id of the subDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/subs/{id}")
    public ResponseEntity<SubDTO> getSub(@PathVariable Long id) {
        log.debug("REST request to get Sub : {}", id);
        Optional<SubDTO> subDTO = subService.findOne(id);
        return ResponseUtil.wrapOrNotFound(subDTO);
    }

    /**
     * {@code DELETE  /subs/:id} : delete the "id" sub.
     *
     * @param id the id of the subDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/subs/{id}")
    public ResponseEntity<Void> deleteSub(@PathVariable Long id) {
        log.debug("REST request to delete Sub : {}", id);
        subService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
