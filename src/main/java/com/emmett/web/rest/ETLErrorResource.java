package com.emmett.web.rest;

import com.emmett.service.ETLErrorService;
import com.emmett.web.rest.errors.BadRequestAlertException;
import com.emmett.service.dto.ETLErrorDTO;
import com.emmett.service.dto.ETLErrorCriteria;
import com.emmett.service.ETLErrorQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.emmett.domain.ETLError}.
 */
@RestController
@RequestMapping("/api")
public class ETLErrorResource {

    private final Logger log = LoggerFactory.getLogger(ETLErrorResource.class);

    private static final String ENTITY_NAME = "eTLError";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ETLErrorService eTLErrorService;

    private final ETLErrorQueryService eTLErrorQueryService;

    public ETLErrorResource(ETLErrorService eTLErrorService, ETLErrorQueryService eTLErrorQueryService) {
        this.eTLErrorService = eTLErrorService;
        this.eTLErrorQueryService = eTLErrorQueryService;
    }

    /**
     * {@code POST  /etl-errors} : Create a new eTLError.
     *
     * @param eTLErrorDTO the eTLErrorDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new eTLErrorDTO, or with status {@code 400 (Bad Request)} if the eTLError has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/etl-errors")
    public ResponseEntity<ETLErrorDTO> createETLError(@Valid @RequestBody ETLErrorDTO eTLErrorDTO) throws URISyntaxException {
        log.debug("REST request to save ETLError : {}", eTLErrorDTO);
        if (eTLErrorDTO.getId() != null) {
            throw new BadRequestAlertException("A new eTLError cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ETLErrorDTO result = eTLErrorService.save(eTLErrorDTO);
        return ResponseEntity.created(new URI("/api/etl-errors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /etl-errors} : Updates an existing eTLError.
     *
     * @param eTLErrorDTO the eTLErrorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated eTLErrorDTO,
     * or with status {@code 400 (Bad Request)} if the eTLErrorDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the eTLErrorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/etl-errors")
    public ResponseEntity<ETLErrorDTO> updateETLError(@Valid @RequestBody ETLErrorDTO eTLErrorDTO) throws URISyntaxException {
        log.debug("REST request to update ETLError : {}", eTLErrorDTO);
        if (eTLErrorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ETLErrorDTO result = eTLErrorService.save(eTLErrorDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, eTLErrorDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /etl-errors} : get all the eTLErrors.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of eTLErrors in body.
     */
    @GetMapping("/etl-errors")
    public ResponseEntity<List<ETLErrorDTO>> getAllETLErrors(ETLErrorCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get ETLErrors by criteria: {}", criteria);
        Page<ETLErrorDTO> page = eTLErrorQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /etl-errors/count} : count all the eTLErrors.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/etl-errors/count")
    public ResponseEntity<Long> countETLErrors(ETLErrorCriteria criteria) {
        log.debug("REST request to count ETLErrors by criteria: {}", criteria);
        return ResponseEntity.ok().body(eTLErrorQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /etl-errors/:id} : get the "id" eTLError.
     *
     * @param id the id of the eTLErrorDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the eTLErrorDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/etl-errors/{id}")
    public ResponseEntity<ETLErrorDTO> getETLError(@PathVariable Long id) {
        log.debug("REST request to get ETLError : {}", id);
        Optional<ETLErrorDTO> eTLErrorDTO = eTLErrorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(eTLErrorDTO);
    }

    /**
     * {@code DELETE  /etl-errors/:id} : delete the "id" eTLError.
     *
     * @param id the id of the eTLErrorDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/etl-errors/{id}")
    public ResponseEntity<Void> deleteETLError(@PathVariable Long id) {
        log.debug("REST request to delete ETLError : {}", id);
        eTLErrorService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
