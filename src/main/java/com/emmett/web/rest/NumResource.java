package com.emmett.web.rest;

import com.emmett.service.NumService;
import com.emmett.web.rest.errors.BadRequestAlertException;
import com.emmett.service.dto.NumDTO;
import com.emmett.service.dto.NumCriteria;
import com.emmett.service.NumQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.emmett.domain.Num}.
 */
@RestController
@RequestMapping("/api")
public class NumResource {

    private final Logger log = LoggerFactory.getLogger(NumResource.class);

    private static final String ENTITY_NAME = "num";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NumService numService;

    private final NumQueryService numQueryService;

    public NumResource(NumService numService, NumQueryService numQueryService) {
        this.numService = numService;
        this.numQueryService = numQueryService;
    }

    /**
     * {@code POST  /nums} : Create a new num.
     *
     * @param numDTO the numDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new numDTO, or with status {@code 400 (Bad Request)} if the num has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nums")
    public ResponseEntity<NumDTO> createNum(@Valid @RequestBody NumDTO numDTO) throws URISyntaxException {
        log.debug("REST request to save Num : {}", numDTO);
        if (numDTO.getId() != null) {
            throw new BadRequestAlertException("A new num cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NumDTO result = numService.save(numDTO);
        return ResponseEntity.created(new URI("/api/nums/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nums} : Updates an existing num.
     *
     * @param numDTO the numDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated numDTO,
     * or with status {@code 400 (Bad Request)} if the numDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the numDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nums")
    public ResponseEntity<NumDTO> updateNum(@Valid @RequestBody NumDTO numDTO) throws URISyntaxException {
        log.debug("REST request to update Num : {}", numDTO);
        if (numDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NumDTO result = numService.save(numDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, numDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /nums} : get all the nums.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of nums in body.
     */
    @GetMapping("/nums")
    public ResponseEntity<List<NumDTO>> getAllNums(NumCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Nums by criteria: {}", criteria);
        Page<NumDTO> page = numQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /nums/count} : count all the nums.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/nums/count")
    public ResponseEntity<Long> countNums(NumCriteria criteria) {
        log.debug("REST request to count Nums by criteria: {}", criteria);
        return ResponseEntity.ok().body(numQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /nums/:id} : get the "id" num.
     *
     * @param id the id of the numDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the numDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nums/{id}")
    public ResponseEntity<NumDTO> getNum(@PathVariable Long id) {
        log.debug("REST request to get Num : {}", id);
        Optional<NumDTO> numDTO = numService.findOne(id);
        return ResponseUtil.wrapOrNotFound(numDTO);
    }

    /**
     * {@code DELETE  /nums/:id} : delete the "id" num.
     *
     * @param id the id of the numDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nums/{id}")
    public ResponseEntity<Void> deleteNum(@PathVariable Long id) {
        log.debug("REST request to delete Num : {}", id);
        numService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
