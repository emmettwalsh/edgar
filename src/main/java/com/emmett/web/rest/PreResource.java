package com.emmett.web.rest;

import com.emmett.service.PreService;
import com.emmett.web.rest.errors.BadRequestAlertException;
import com.emmett.service.dto.PreDTO;
import com.emmett.service.dto.PreCriteria;
import com.emmett.service.PreQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.emmett.domain.Pre}.
 */
@RestController
@RequestMapping("/api")
public class PreResource {

    private final Logger log = LoggerFactory.getLogger(PreResource.class);

    private static final String ENTITY_NAME = "pre";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PreService preService;

    private final PreQueryService preQueryService;

    public PreResource(PreService preService, PreQueryService preQueryService) {
        this.preService = preService;
        this.preQueryService = preQueryService;
    }

    /**
     * {@code POST  /pres} : Create a new pre.
     *
     * @param preDTO the preDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new preDTO, or with status {@code 400 (Bad Request)} if the pre has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pres")
    public ResponseEntity<PreDTO> createPre(@Valid @RequestBody PreDTO preDTO) throws URISyntaxException {
        log.debug("REST request to save Pre : {}", preDTO);
        if (preDTO.getId() != null) {
            throw new BadRequestAlertException("A new pre cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PreDTO result = preService.save(preDTO);
        return ResponseEntity.created(new URI("/api/pres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pres} : Updates an existing pre.
     *
     * @param preDTO the preDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated preDTO,
     * or with status {@code 400 (Bad Request)} if the preDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the preDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pres")
    public ResponseEntity<PreDTO> updatePre(@Valid @RequestBody PreDTO preDTO) throws URISyntaxException {
        log.debug("REST request to update Pre : {}", preDTO);
        if (preDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PreDTO result = preService.save(preDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, preDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /pres} : get all the pres.
     *
     * @param pageable the pagination information.
     * @param queryParams a {@link MultiValueMap} query parameters.
     * @param uriBuilder a {@link UriComponentsBuilder} URI builder.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of pres in body.
     */
    @GetMapping("/pres")
    public ResponseEntity<List<PreDTO>> getAllPres(PreCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Pres by criteria: {}", criteria);
        Page<PreDTO> page = preQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /pres/count} : count all the pres.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/pres/count")
    public ResponseEntity<Long> countPres(PreCriteria criteria) {
        log.debug("REST request to count Pres by criteria: {}", criteria);
        return ResponseEntity.ok().body(preQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /pres/:id} : get the "id" pre.
     *
     * @param id the id of the preDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the preDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pres/{id}")
    public ResponseEntity<PreDTO> getPre(@PathVariable Long id) {
        log.debug("REST request to get Pre : {}", id);
        Optional<PreDTO> preDTO = preService.findOne(id);
        return ResponseUtil.wrapOrNotFound(preDTO);
    }

    /**
     * {@code DELETE  /pres/:id} : delete the "id" pre.
     *
     * @param id the id of the preDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pres/{id}")
    public ResponseEntity<Void> deletePre(@PathVariable Long id) {
        log.debug("REST request to delete Pre : {}", id);
        preService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
