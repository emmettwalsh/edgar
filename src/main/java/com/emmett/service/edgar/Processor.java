package com.emmett.service.edgar;

import com.emmett.repository.SubRepository;
import com.emmett.service.edgar.etl.NumETLProcessor;
import com.emmett.service.edgar.etl.PreETLProcessor;
import com.emmett.service.edgar.etl.SubETLProcessor;
import com.emmett.service.edgar.etl.TagETLProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Stream;


@Service
public class Processor {

    private final Logger log = LoggerFactory.getLogger(Processor.class);

    @Autowired
    SubRepository subRepository;

    @Autowired
    SubETLProcessor subETLProcessor;

    @Autowired
    TagETLProcessor tagETLProcessor;

    @Autowired
    NumETLProcessor numETLProcessor;

    @Autowired
    PreETLProcessor preETLProcessor;

    /**
     * process subs file first and insert each
     * next process tags file and insert each
     * next process nums file, fr each one link the sub and the tags
     * next process pre file, fr each one link the sub and the tags
     * @throws Exception
     */
    public void processFiles() throws Exception {

        // 1. find the edgar data directory
        log.info("Looking for edgar data directory..");
        File edgarDirectory = ResourceUtils.getFile("classpath:edgar-data");

        if (edgarDirectory.exists() && edgarDirectory.isDirectory()) {

            // 2. Look for  YYYYQQ folders in there
            log.info("Looking for YYYYQQ folders");
            Arrays.stream(edgarDirectory.listFiles(File::isDirectory)).forEach(yearAndQuarterDir -> {


                //3. get the data files in there
                log.info("Found year and quarter directory {}", yearAndQuarterDir.getName());
                Supplier<Stream<File>> yearAndQuarterFilesSupplier = () -> Arrays.stream(yearAndQuarterDir.listFiles());


                // process sub file
                yearAndQuarterFilesSupplier.get()
                    .filter(file -> file.getName().equals("sub.txt"))
                    .findFirst()
                    .ifPresent( file -> {
                        log.info("Found sub file in {} directory", yearAndQuarterDir.getName());
                        subETLProcessor.runETL(yearAndQuarterDir.getName(), file);
                    });

                // process tag file
                yearAndQuarterFilesSupplier.get()
                    .filter(file -> file.getName().equals("tag.txt"))
                    .findFirst()
                    .ifPresent( file -> {
                        log.info("Found tag file in {} directory, running tag ETL..", yearAndQuarterDir.getName());
                        tagETLProcessor.runETL(yearAndQuarterDir.getName(), file);
                    });

                // process num file
                yearAndQuarterFilesSupplier.get()
                    .filter(file -> file.getName().equals("num.txt"))
                    .findFirst()
                    .ifPresent( file -> {
                        log.info("Found num file in {} directory, running num ETL..", yearAndQuarterDir.getName());
                        numETLProcessor.runETL(yearAndQuarterDir.getName(), file);
                    });

                // process pre file
                yearAndQuarterFilesSupplier.get()
                    .filter(file -> file.getName().equals("pre.txt"))
                    .findFirst()
                    .ifPresent( file -> {
                        log.info("Found pre file in {} directory running pre ETL..", yearAndQuarterDir.getName());
                        preETLProcessor.runETL(yearAndQuarterDir.getName(), file);
                    });



            });

        }



    }




}
