package com.emmett.service.edgar.etl;

import com.emmett.domain.ETLError;
import com.emmett.domain.Sub;
import com.emmett.domain.Tag;
import com.emmett.domain.enumeration.EtlErrorType;
import com.emmett.repository.ETLErrorRepository;
import com.emmett.repository.SubRepository;
import com.emmett.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.io.File;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TagETLProcessor {


    private final Logger log = LoggerFactory.getLogger(TagETLProcessor.class);

    @Autowired
    TagRepository tagRepository;

    @Autowired
    ETLErrorRepository etlErrorRepository;

    /**
     *
     * @param yearAndQuarterName
     * @param tagFile the tag.txt file
     */
    public void runETL(String yearAndQuarterName, File tagFile){


        try {
            Files.lines(tagFile.toPath()).skip(1)
                .map(line -> extract(yearAndQuarterName, line)).filter(optionalStrings -> optionalStrings.isPresent())
                .map(values -> transform(yearAndQuarterName, values.get())).filter(optionalSubs -> optionalSubs.isPresent())
                .forEach(tag -> load(yearAndQuarterName, tag.get()));

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }


    }

    /**
     *
     * @param line a raw line form tag.txt
     * @return an array of strings
     */
    private Optional<String[]> extract(String yearAndQuarterName, String line) {
        try {
            return Optional.of(line.split("\\t"));
        } catch (Throwable t) {

            etlErrorRepository.save(new ETLError()
                .domain("Tag")
                .type(EtlErrorType.Extract)
                .yearAndQuarter(yearAndQuarterName)
                .input(line)
                .output(t.getMessage()));

            return Optional.empty();
        }


    }

    /**
     * Transforms raw array of values into a Tag entity
     * @param yearAndQuarterName
     * @param rawValues
     * @return a Sub entity
     */
    private Optional<Tag> transform(String yearAndQuarterName, String[] rawValues){

        try {

            Tag tag = new Tag();

            for (int i = 0; i < rawValues.length; i++) {

                String value = rawValues[i];

                if (value == null || value.equals("")) {
                    continue;
                }


                switch (i) {

                    case 0:
                        tag.setTag(value);
                        break;
                    case 1:
                        tag.setVersion(value);
                        break;

                    case 2:
                        tag.setCustom(Boolean.valueOf(value));
                        break;
                    case 3:
                        tag.setAbstrct(Boolean.valueOf(value));
                        break;
                    case 4:
                        tag.setDatatype(value);
                        break;

                    case 5:
                        tag.iord(value);
                        break;

                    case 6:
                        tag.setCrdr(value);
                        break;
                    case 7:
                        tag.tlabel(value);
                        break;
                    case 8:
                        tag.setDoc(value);
                        break;

                    default:

                }

            }

            return Optional.of(tag);

        }catch (Throwable t){
            etlErrorRepository.save(new ETLError()
                .domain("Tag")
                .type(EtlErrorType.Transform)
                .yearAndQuarter(yearAndQuarterName)
                .input(Arrays.stream(rawValues).collect(Collectors.joining(",")))
                .output(t.toString()));
            return Optional.empty();
        }


    }


    /**
     * Loads tag entity into our data repository
     * @param tag
     * @return optional - an error message if there was an error with load
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = ConstraintViolationException.class)
    private void load(String yearAndQuarter, Tag tag){
        try {
            tagRepository.saveAndFlush(tag);

        }catch (ConstraintViolationException ex){
            etlErrorRepository.save(new ETLError()
                .domain("Tag")
                .type(EtlErrorType.Load)
                .yearAndQuarter(yearAndQuarter)
                .input(tag.toString())
                .output(ex.getMessage()));
        }
    }



}
