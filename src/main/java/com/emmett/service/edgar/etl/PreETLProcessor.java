package com.emmett.service.edgar.etl;

import com.emmett.domain.ETLError;
import com.emmett.domain.Pre;
import com.emmett.domain.Sub;
import com.emmett.domain.Tag;
import com.emmett.domain.enumeration.EtlErrorType;
import com.emmett.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PreETLProcessor {

    private final Logger log = LoggerFactory.getLogger(PreETLProcessor.class);

    @Autowired
    NumRepository numRepository;

    @Autowired
    SubRepository subRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    PreRepository preRepository;

    @Autowired
    ETLErrorRepository etlErrorRepository;

    /**
     *
     * @param yearAndQuarterName
     * @param prefile the pre.txt file
     */
    public void runETL(String yearAndQuarterName, File preFile){

        try {
            Files.lines(preFile.toPath()).skip(1)
                .map(line -> extract(yearAndQuarterName, line)).filter(optionalStrings -> optionalStrings.isPresent())
                .map(values -> transform(yearAndQuarterName, values.get())).filter(optionalSubs -> optionalSubs.isPresent())
                .forEach(pre -> load(yearAndQuarterName, pre.get()));

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }



    }

    /**
     *
     * @param line a raw line form pre.txt
     * @return an array of strings
     */
    private Optional<String[]> extract(String yearAndQuarterName, String line) {
        try {
            return Optional.of(line.split("\\t"));
        } catch (Throwable t) {

            etlErrorRepository.save(new ETLError()
                .domain("Pre")
                .type(EtlErrorType.Extract)
                .yearAndQuarter(yearAndQuarterName)
                .input(line)
                .output(t.getMessage()));

            return Optional.empty();
        }


    }

    /**
     * Transforms raw array of values into a Pre entity
     * @param yearAndQuarterName
     * @param rawValues
     * @return a Pre entity
     */
    private Optional<Pre> transform(String yearAndQuarterName, String[] rawValues){

        try {

            Pre pre = new Pre();

            for (int i = 0; i < rawValues.length; i++) {

                String value = rawValues[i];

                if (value == null || value.equals("")) {
                    continue;
                }


                switch (i) {

                    case 0:
                        //each num relates to a Sub via adsh
                        List<Sub> subs = subRepository.findByAdsh(value);
                        if(subs.size() == 1){
                            pre.setAdsh(subs.get(0));
                        }

                        break;

                    case 1:

                        pre.setReport(Integer.valueOf(value));

                        break;

                    case 2:

                        pre.setLine(Integer.valueOf(value));

                        break;

                    case 3:

                        pre.setStatement(value);

                        break;

                    case 4:

                        pre.setInpth(Boolean.valueOf(value));

                        break;

                    case 5:

                        pre.setRfile(value);


                    case 7:

                        String tag = rawValues[6];
                        String version = value;

                        //find the tag based on these values and set

                        List<Tag> tags = tagRepository.findByTagAndVersion(tag, version);
                        if(tags.size() == 1){
                            pre.setTag(tags.get(0));
                        }

                        break;

                    case 8:

                        pre.setPlabel(value);

                        break;


                    default:

                }

            }

            return Optional.of(pre);

        }catch (Throwable t){
            etlErrorRepository.save(new ETLError()
                .domain("Pre")
                .type(EtlErrorType.Transform)
                .yearAndQuarter(yearAndQuarterName)
                .input(Arrays.stream(rawValues).collect(Collectors.joining(",")))
                .output(t.toString()));

            return Optional.empty();
        }


    }


    /**
     * Loads pre entity into our data repository
     * @param pre
     * @return optional - an error message if there was an error with load
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = ConstraintViolationException.class)
    private void load(String yearAndQuarter, Pre pre){
        try {
            preRepository.saveAndFlush(pre);

        }catch (ConstraintViolationException ex){
            etlErrorRepository.save(new ETLError()
                .domain("Pre")
                .type(EtlErrorType.Load)
                .yearAndQuarter(yearAndQuarter)
                .input(pre.toString())
                .output(ex.getMessage()));
        }
    }



}
