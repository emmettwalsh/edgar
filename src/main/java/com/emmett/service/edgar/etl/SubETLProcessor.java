package com.emmett.service.edgar.etl;

import com.emmett.domain.ETLError;
import com.emmett.domain.Sub;
import com.emmett.domain.enumeration.EtlErrorType;
import com.emmett.repository.ETLErrorRepository;
import com.emmett.repository.SubRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SubETLProcessor {


    private final Logger log = LoggerFactory.getLogger(SubETLProcessor.class);

    @Autowired
    SubRepository subRepository;

    @Autowired
    ETLErrorRepository etlErrorRepository;

    /**
     * @param yearAndQuarterName
     * @param subFile            the sub.txt file
     */
    public void runETL(String yearAndQuarterName, File subFile) {

        try {
            Files.lines(subFile.toPath()).skip(1)
                .map(line -> extract(yearAndQuarterName, line)).filter(optionalStrings -> optionalStrings.isPresent())
                .map(values -> transform(yearAndQuarterName, values.get())).filter(optionalSubs -> optionalSubs.isPresent())
                .forEach(sub -> load(sub.get()));

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * @param line a raw line form sub.txt
     * @return an array of strings
     */
    private Optional<String[]> extract(String yearAndQuarterName, String line) {
        try {
            return Optional.of(line.split("\\t"));
        } catch (Throwable t) {

            etlErrorRepository.save(new ETLError()
                .domain("Sub")
                .type(EtlErrorType.Extract)
                .yearAndQuarter(yearAndQuarterName)
                .input(line)
                .output(t.getMessage()));

            return Optional.empty();
        }


    }

    /**
     * Transforms raw array of values into a Sub entity
     *
     * @param yearAndQuarterName
     * @param rawValues
     * @return a Sub entity
     */
    private Optional<Sub> transform(String yearAndQuarterName, String[] rawValues) {

        try {
            Sub sub = new Sub();
            sub.setYearAndQuarter(yearAndQuarterName);

            for (int i = 0; i < rawValues.length; i++) {

                String value = rawValues[i];

                if (value == null || value.equals("")) {
                    continue;
                }


                switch (i) {

                    case 0:
                        sub.setAdsh(value);
                        break;
                    case 1:
                        sub.setCik(Long.valueOf(value));
                        break;

                    case 2:
                        sub.setName(value);
                        break;
                    case 3:
                        sub.setSic(Integer.valueOf(value));
                        break;
                    case 4:
                        sub.setCountryba(value);
                        break;
                    case 5:
                        sub.setStprba(value);
                        break;
                    case 6:
                        sub.setCityba(value);
                        break;
                    case 7:
                        sub.setZipba(value);
                        break;
                    case 8:
                        sub.setBas1(value);
                        break;
                    case 9:
                        sub.setBas2(value);
                        break;
                    case 10:
                        sub.setBaph(value);
                        break;
                    case 11:
                        sub.setCountryma(value);
                        break;
                    case 12:
                        sub.setStprma(value);
                        break;
                    case 13:
                        sub.setCityma(value);
                        break;
                    case 14:
                        sub.setZipma(value);
                        break;
                    case 15:
                        sub.setMas1(value);
                        break;
                    case 16:
                        sub.setMas2(value);
                        break;
                    case 17:
                        sub.setCountryinc(value);
                        break;
                    case 18:
                        sub.setStprinc(value);
                        break;
                    case 19:
                        sub.setEin(Long.valueOf(value));
                        break;
                    case 20:
                        sub.setFormer(value);
                        break;
                    case 21:
                        sub.setChanged(value);
                        break;
                    case 22:
                        sub.setAfs(value);
                        break;
                    case 23:
                        sub.setWksi(Boolean.valueOf(value));
                        break;
                    case 24:
                        sub.setFye(value);
                        break;
                    case 25:
                        sub.setForm(value);
                        break;
                    case 26:

                        Date period = new SimpleDateFormat("yyMMdd").parse(value);
                        sub.setPeriod(period.toInstant());


                        break;
                    case 27:
                        sub.setFy(Integer.valueOf(value));
                        break;
                    case 28:
                        sub.setFp(value);
                        break;
                    case 29:

                        Date filed = new SimpleDateFormat("yyMMdd").parse(value);
                        sub.setFiled(filed.toInstant());

                        break;
                    case 30:

                        Date accepted = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(value);
                        sub.accepted(accepted.toInstant());

                        break;
                    case 31:
                        sub.setPrevrpt(Boolean.valueOf(value));
                        break;
                    case 32:
                        sub.setDetail(Boolean.valueOf(value));
                        break;
                    case 33:
                        sub.setInstance(value);
                        break;
                    case 34:
                        sub.setNciks(Integer.valueOf(value));
                        break;
                    case 35:
                        sub.setAciks(value);
                        break;

                    default:

                }

            }

            return Optional.of(sub);
        } catch (Throwable t) {
            etlErrorRepository.save(new ETLError()
                .domain("Sub")
                .type(EtlErrorType.Transform)
                .yearAndQuarter(yearAndQuarterName)
                .input(Arrays.stream(rawValues).collect(Collectors.joining(",")))
                .output(t.toString()));
            return Optional.empty();
        }


    }


    /**
     * Loads Sub entity into our data repository
     *
     * @param sub
     * @return optional - an error message if there was an error with load
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = Throwable.class)
    private void load(Sub sub) {
        try {
            subRepository.saveAndFlush(sub);

        } catch (Throwable ex) {
            etlErrorRepository.save(new ETLError()
                .domain("Sub")
                .type(EtlErrorType.Load)
                .yearAndQuarter(sub.getYearAndQuarter())
                .input(sub.toString())
                .output(ex.getMessage()));
        }
    }


}


