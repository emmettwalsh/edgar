package com.emmett.service.edgar.etl;

import com.emmett.domain.ETLError;
import com.emmett.domain.Num;
import com.emmett.domain.Sub;
import com.emmett.domain.Tag;
import com.emmett.domain.enumeration.EtlErrorType;
import com.emmett.repository.ETLErrorRepository;
import com.emmett.repository.NumRepository;
import com.emmett.repository.SubRepository;
import com.emmett.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NumETLProcessor {

    private final Logger log = LoggerFactory.getLogger(NumETLProcessor.class);

    @Autowired
    NumRepository numRepository;

    @Autowired
    SubRepository subRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    ETLErrorRepository etlErrorRepository;

    /**
     * @param yearAndQuarterName
     * @param numFile            the num.txt file
     */
    public void runETL(String yearAndQuarterName, File numFile) {

        try {
            Files.lines(numFile.toPath()).skip(1)
                .map(line -> extract(yearAndQuarterName, line)).filter(optionalStrings -> optionalStrings.isPresent())
                .map(values -> transform(yearAndQuarterName, values.get())).filter(optionalSubs -> optionalSubs.isPresent())
                .forEach(sub -> load(yearAndQuarterName, sub.get()));

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    /**
     * @param line a raw line form num.txt
     * @return an array of strings
     */
    private Optional<String[]> extract(String yearAndQuarterName, String line) {
        try {
            return Optional.of(line.split("\\t"));
        } catch (Throwable t) {

            etlErrorRepository.save(new ETLError()
                .domain("Num")
                .type(EtlErrorType.Extract)
                .yearAndQuarter(yearAndQuarterName)
                .input(line)
                .output(t.getMessage()));

            return Optional.empty();
        }


    }

    /**
     * Transforms raw array of values into a Num entity
     *
     * @param yearAndQuarterName
     * @param rawValues
     * @return a Num entity
     */
    private Optional<Num> transform(String yearAndQuarterName, String[] rawValues) {


        try {

            Num num = new Num();

            for (int i = 0; i < rawValues.length; i++) {

                String value = rawValues[i];

                if (value == null || value.equals("")) {
                    continue;
                }


                switch (i) {

                    case 0:
                        //each num relates to a Sub via adsh
                        List<Sub> subs = subRepository.findByAdsh(value);
                        if (subs.size() == 1) {
                            num.setAdsh(subs.get(0));
                        }

                        break;

                    case 2:

                        String tag = rawValues[1];
                        String version = value;

                        //find the tag based on these values and set

                        List<Tag> tags = tagRepository.findByTagAndVersion(tag, version);
                        if (tags.size() == 1) {
                            num.setTag(tags.get(0));
                        }

                        break;

                    case 3:

                        num.setCoreg(Integer.valueOf(value));


                        break;

                    case 4:


                        Date dDate = new SimpleDateFormat("yyyyMMdd").parse(value);
                        num.setDdate(dDate.toInstant());

                        break;


                    case 5:

                        num.setQtrs(Long.valueOf(value));

                        break;

                    case 6:

                        num.setUom(value);

                        break;

                    case 7:
                        num.setValue(new BigDecimal(value).setScale(4));
                        break;

                    case 8:

                        num.setFootnote(value);

                        break;

                    default:

                }

            }

            return Optional.of(num);

        } catch (Throwable t) {
            etlErrorRepository.save(new ETLError()
                .domain("Num")
                .type(EtlErrorType.Transform)
                .yearAndQuarter(yearAndQuarterName)
                .input(Arrays.stream(rawValues).collect(Collectors.joining(",")))
                .output(t.toString()));

            return Optional.empty();
        }


    }


    /**
     * Loads num entity into our data repository
     *
     * @param num
     * @return optional - an error message if there was an error with load
     */
    @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = ConstraintViolationException.class)
    private void load(String yearAndQuarter, Num num) {
        try {
            numRepository.saveAndFlush(num);

        } catch (ConstraintViolationException ex) {
            etlErrorRepository.save(new ETLError()
                .domain("Num")
                .type(EtlErrorType.Load)
                .yearAndQuarter(yearAndQuarter)
                .input(num.toString())
                .output(ex.getMessage()));
        }
    }


}
