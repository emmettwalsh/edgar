package com.emmett.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.emmett.domain.Sub} entity. This class is used
 * in {@link com.emmett.web.rest.SubResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /subs?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class SubCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter adsh;

    private LongFilter cik;

    private StringFilter name;

    private IntegerFilter sic;

    private StringFilter countryba;

    private StringFilter stprba;

    private StringFilter cityba;

    private StringFilter zipba;

    private StringFilter bas1;

    private StringFilter bas2;

    private StringFilter baph;

    private StringFilter countryma;

    private StringFilter stprma;

    private StringFilter cityma;

    private StringFilter zipma;

    private StringFilter mas1;

    private StringFilter mas2;

    private StringFilter countryinc;

    private StringFilter stprinc;

    private LongFilter ein;

    private StringFilter former;

    private StringFilter changed;

    private StringFilter afs;

    private BooleanFilter wksi;

    private StringFilter fye;

    private StringFilter form;

    private InstantFilter period;

    private IntegerFilter fy;

    private StringFilter fp;

    private InstantFilter filed;

    private InstantFilter accepted;

    private BooleanFilter prevrpt;

    private BooleanFilter detail;

    private StringFilter instance;

    private IntegerFilter nciks;

    private StringFilter aciks;

    private StringFilter yearAndQuarter;

    public SubCriteria(){
    }

    public SubCriteria(SubCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.adsh = other.adsh == null ? null : other.adsh.copy();
        this.cik = other.cik == null ? null : other.cik.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.sic = other.sic == null ? null : other.sic.copy();
        this.countryba = other.countryba == null ? null : other.countryba.copy();
        this.stprba = other.stprba == null ? null : other.stprba.copy();
        this.cityba = other.cityba == null ? null : other.cityba.copy();
        this.zipba = other.zipba == null ? null : other.zipba.copy();
        this.bas1 = other.bas1 == null ? null : other.bas1.copy();
        this.bas2 = other.bas2 == null ? null : other.bas2.copy();
        this.baph = other.baph == null ? null : other.baph.copy();
        this.countryma = other.countryma == null ? null : other.countryma.copy();
        this.stprma = other.stprma == null ? null : other.stprma.copy();
        this.cityma = other.cityma == null ? null : other.cityma.copy();
        this.zipma = other.zipma == null ? null : other.zipma.copy();
        this.mas1 = other.mas1 == null ? null : other.mas1.copy();
        this.mas2 = other.mas2 == null ? null : other.mas2.copy();
        this.countryinc = other.countryinc == null ? null : other.countryinc.copy();
        this.stprinc = other.stprinc == null ? null : other.stprinc.copy();
        this.ein = other.ein == null ? null : other.ein.copy();
        this.former = other.former == null ? null : other.former.copy();
        this.changed = other.changed == null ? null : other.changed.copy();
        this.afs = other.afs == null ? null : other.afs.copy();
        this.wksi = other.wksi == null ? null : other.wksi.copy();
        this.fye = other.fye == null ? null : other.fye.copy();
        this.form = other.form == null ? null : other.form.copy();
        this.period = other.period == null ? null : other.period.copy();
        this.fy = other.fy == null ? null : other.fy.copy();
        this.fp = other.fp == null ? null : other.fp.copy();
        this.filed = other.filed == null ? null : other.filed.copy();
        this.accepted = other.accepted == null ? null : other.accepted.copy();
        this.prevrpt = other.prevrpt == null ? null : other.prevrpt.copy();
        this.detail = other.detail == null ? null : other.detail.copy();
        this.instance = other.instance == null ? null : other.instance.copy();
        this.nciks = other.nciks == null ? null : other.nciks.copy();
        this.aciks = other.aciks == null ? null : other.aciks.copy();
        this.yearAndQuarter = other.yearAndQuarter == null ? null : other.yearAndQuarter.copy();
    }

    @Override
    public SubCriteria copy() {
        return new SubCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAdsh() {
        return adsh;
    }

    public void setAdsh(StringFilter adsh) {
        this.adsh = adsh;
    }

    public LongFilter getCik() {
        return cik;
    }

    public void setCik(LongFilter cik) {
        this.cik = cik;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public IntegerFilter getSic() {
        return sic;
    }

    public void setSic(IntegerFilter sic) {
        this.sic = sic;
    }

    public StringFilter getCountryba() {
        return countryba;
    }

    public void setCountryba(StringFilter countryba) {
        this.countryba = countryba;
    }

    public StringFilter getStprba() {
        return stprba;
    }

    public void setStprba(StringFilter stprba) {
        this.stprba = stprba;
    }

    public StringFilter getCityba() {
        return cityba;
    }

    public void setCityba(StringFilter cityba) {
        this.cityba = cityba;
    }

    public StringFilter getZipba() {
        return zipba;
    }

    public void setZipba(StringFilter zipba) {
        this.zipba = zipba;
    }

    public StringFilter getBas1() {
        return bas1;
    }

    public void setBas1(StringFilter bas1) {
        this.bas1 = bas1;
    }

    public StringFilter getBas2() {
        return bas2;
    }

    public void setBas2(StringFilter bas2) {
        this.bas2 = bas2;
    }

    public StringFilter getBaph() {
        return baph;
    }

    public void setBaph(StringFilter baph) {
        this.baph = baph;
    }

    public StringFilter getCountryma() {
        return countryma;
    }

    public void setCountryma(StringFilter countryma) {
        this.countryma = countryma;
    }

    public StringFilter getStprma() {
        return stprma;
    }

    public void setStprma(StringFilter stprma) {
        this.stprma = stprma;
    }

    public StringFilter getCityma() {
        return cityma;
    }

    public void setCityma(StringFilter cityma) {
        this.cityma = cityma;
    }

    public StringFilter getZipma() {
        return zipma;
    }

    public void setZipma(StringFilter zipma) {
        this.zipma = zipma;
    }

    public StringFilter getMas1() {
        return mas1;
    }

    public void setMas1(StringFilter mas1) {
        this.mas1 = mas1;
    }

    public StringFilter getMas2() {
        return mas2;
    }

    public void setMas2(StringFilter mas2) {
        this.mas2 = mas2;
    }

    public StringFilter getCountryinc() {
        return countryinc;
    }

    public void setCountryinc(StringFilter countryinc) {
        this.countryinc = countryinc;
    }

    public StringFilter getStprinc() {
        return stprinc;
    }

    public void setStprinc(StringFilter stprinc) {
        this.stprinc = stprinc;
    }

    public LongFilter getEin() {
        return ein;
    }

    public void setEin(LongFilter ein) {
        this.ein = ein;
    }

    public StringFilter getFormer() {
        return former;
    }

    public void setFormer(StringFilter former) {
        this.former = former;
    }

    public StringFilter getChanged() {
        return changed;
    }

    public void setChanged(StringFilter changed) {
        this.changed = changed;
    }

    public StringFilter getAfs() {
        return afs;
    }

    public void setAfs(StringFilter afs) {
        this.afs = afs;
    }

    public BooleanFilter getWksi() {
        return wksi;
    }

    public void setWksi(BooleanFilter wksi) {
        this.wksi = wksi;
    }

    public StringFilter getFye() {
        return fye;
    }

    public void setFye(StringFilter fye) {
        this.fye = fye;
    }

    public StringFilter getForm() {
        return form;
    }

    public void setForm(StringFilter form) {
        this.form = form;
    }

    public InstantFilter getPeriod() {
        return period;
    }

    public void setPeriod(InstantFilter period) {
        this.period = period;
    }

    public IntegerFilter getFy() {
        return fy;
    }

    public void setFy(IntegerFilter fy) {
        this.fy = fy;
    }

    public StringFilter getFp() {
        return fp;
    }

    public void setFp(StringFilter fp) {
        this.fp = fp;
    }

    public InstantFilter getFiled() {
        return filed;
    }

    public void setFiled(InstantFilter filed) {
        this.filed = filed;
    }

    public InstantFilter getAccepted() {
        return accepted;
    }

    public void setAccepted(InstantFilter accepted) {
        this.accepted = accepted;
    }

    public BooleanFilter getPrevrpt() {
        return prevrpt;
    }

    public void setPrevrpt(BooleanFilter prevrpt) {
        this.prevrpt = prevrpt;
    }

    public BooleanFilter getDetail() {
        return detail;
    }

    public void setDetail(BooleanFilter detail) {
        this.detail = detail;
    }

    public StringFilter getInstance() {
        return instance;
    }

    public void setInstance(StringFilter instance) {
        this.instance = instance;
    }

    public IntegerFilter getNciks() {
        return nciks;
    }

    public void setNciks(IntegerFilter nciks) {
        this.nciks = nciks;
    }

    public StringFilter getAciks() {
        return aciks;
    }

    public void setAciks(StringFilter aciks) {
        this.aciks = aciks;
    }

    public StringFilter getYearAndQuarter() {
        return yearAndQuarter;
    }

    public void setYearAndQuarter(StringFilter yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SubCriteria that = (SubCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(adsh, that.adsh) &&
            Objects.equals(cik, that.cik) &&
            Objects.equals(name, that.name) &&
            Objects.equals(sic, that.sic) &&
            Objects.equals(countryba, that.countryba) &&
            Objects.equals(stprba, that.stprba) &&
            Objects.equals(cityba, that.cityba) &&
            Objects.equals(zipba, that.zipba) &&
            Objects.equals(bas1, that.bas1) &&
            Objects.equals(bas2, that.bas2) &&
            Objects.equals(baph, that.baph) &&
            Objects.equals(countryma, that.countryma) &&
            Objects.equals(stprma, that.stprma) &&
            Objects.equals(cityma, that.cityma) &&
            Objects.equals(zipma, that.zipma) &&
            Objects.equals(mas1, that.mas1) &&
            Objects.equals(mas2, that.mas2) &&
            Objects.equals(countryinc, that.countryinc) &&
            Objects.equals(stprinc, that.stprinc) &&
            Objects.equals(ein, that.ein) &&
            Objects.equals(former, that.former) &&
            Objects.equals(changed, that.changed) &&
            Objects.equals(afs, that.afs) &&
            Objects.equals(wksi, that.wksi) &&
            Objects.equals(fye, that.fye) &&
            Objects.equals(form, that.form) &&
            Objects.equals(period, that.period) &&
            Objects.equals(fy, that.fy) &&
            Objects.equals(fp, that.fp) &&
            Objects.equals(filed, that.filed) &&
            Objects.equals(accepted, that.accepted) &&
            Objects.equals(prevrpt, that.prevrpt) &&
            Objects.equals(detail, that.detail) &&
            Objects.equals(instance, that.instance) &&
            Objects.equals(nciks, that.nciks) &&
            Objects.equals(aciks, that.aciks) &&
            Objects.equals(yearAndQuarter, that.yearAndQuarter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        adsh,
        cik,
        name,
        sic,
        countryba,
        stprba,
        cityba,
        zipba,
        bas1,
        bas2,
        baph,
        countryma,
        stprma,
        cityma,
        zipma,
        mas1,
        mas2,
        countryinc,
        stprinc,
        ein,
        former,
        changed,
        afs,
        wksi,
        fye,
        form,
        period,
        fy,
        fp,
        filed,
        accepted,
        prevrpt,
        detail,
        instance,
        nciks,
        aciks,
        yearAndQuarter
        );
    }

    @Override
    public String toString() {
        return "SubCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (adsh != null ? "adsh=" + adsh + ", " : "") +
                (cik != null ? "cik=" + cik + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (sic != null ? "sic=" + sic + ", " : "") +
                (countryba != null ? "countryba=" + countryba + ", " : "") +
                (stprba != null ? "stprba=" + stprba + ", " : "") +
                (cityba != null ? "cityba=" + cityba + ", " : "") +
                (zipba != null ? "zipba=" + zipba + ", " : "") +
                (bas1 != null ? "bas1=" + bas1 + ", " : "") +
                (bas2 != null ? "bas2=" + bas2 + ", " : "") +
                (baph != null ? "baph=" + baph + ", " : "") +
                (countryma != null ? "countryma=" + countryma + ", " : "") +
                (stprma != null ? "stprma=" + stprma + ", " : "") +
                (cityma != null ? "cityma=" + cityma + ", " : "") +
                (zipma != null ? "zipma=" + zipma + ", " : "") +
                (mas1 != null ? "mas1=" + mas1 + ", " : "") +
                (mas2 != null ? "mas2=" + mas2 + ", " : "") +
                (countryinc != null ? "countryinc=" + countryinc + ", " : "") +
                (stprinc != null ? "stprinc=" + stprinc + ", " : "") +
                (ein != null ? "ein=" + ein + ", " : "") +
                (former != null ? "former=" + former + ", " : "") +
                (changed != null ? "changed=" + changed + ", " : "") +
                (afs != null ? "afs=" + afs + ", " : "") +
                (wksi != null ? "wksi=" + wksi + ", " : "") +
                (fye != null ? "fye=" + fye + ", " : "") +
                (form != null ? "form=" + form + ", " : "") +
                (period != null ? "period=" + period + ", " : "") +
                (fy != null ? "fy=" + fy + ", " : "") +
                (fp != null ? "fp=" + fp + ", " : "") +
                (filed != null ? "filed=" + filed + ", " : "") +
                (accepted != null ? "accepted=" + accepted + ", " : "") +
                (prevrpt != null ? "prevrpt=" + prevrpt + ", " : "") +
                (detail != null ? "detail=" + detail + ", " : "") +
                (instance != null ? "instance=" + instance + ", " : "") +
                (nciks != null ? "nciks=" + nciks + ", " : "") +
                (aciks != null ? "aciks=" + aciks + ", " : "") +
                (yearAndQuarter != null ? "yearAndQuarter=" + yearAndQuarter + ", " : "") +
            "}";
    }

}
