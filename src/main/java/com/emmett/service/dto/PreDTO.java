package com.emmett.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.emmett.domain.Pre} entity.
 */
public class PreDTO implements Serializable {

    private Long id;

    @Max(value = 999999)
    private Integer report;

    @Max(value = 999999)
    private Integer line;

    @NotNull
    @Size(max = 2)
    private String statement;

    @NotNull
    private Boolean inpth;

    @NotNull
    @Size(max = 1)
    private String rfile;

    @NotNull
    @Size(max = 512)
    private String plabel;


    private Long adshId;

    private String adshText;

    private Long tagId;

    private String tagTag;

    private String tagVersion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getReport() {
        return report;
    }

    public void setReport(Integer report) {
        this.report = report;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public Boolean isInpth() {
        return inpth;
    }

    public void setInpth(Boolean inpth) {
        this.inpth = inpth;
    }

    public String getRfile() {
        return rfile;
    }

    public void setRfile(String rfile) {
        this.rfile = rfile;
    }

    public String getPlabel() {
        return plabel;
    }

    public void setPlabel(String plabel) {
        this.plabel = plabel;
    }

    public Long getAdshId() {
        return adshId;
    }

    public void setAdshId(Long subId) {
        this.adshId = subId;
    }

    public String getAdshText() {
        return adshText;
    }

    public void setAdshText(String adshText) {
        this.adshText = adshText;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    public void setTagTag(String tagTag) {
        this.tagTag = tagTag;
    }

    public String getTagTag() {
        return tagTag;
    }

    public void setTagVersion(String tagVersion) {
        this.tagVersion = tagVersion;
    }

    public String getTagVersion() {
        return tagVersion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PreDTO preDTO = (PreDTO) o;
        if (preDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), preDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PreDTO{" +
            "id=" + getId() +
            ", report=" + getReport() +
            ", line=" + getLine() +
            ", statement='" + getStatement() + "'" +
            ", inpth='" + isInpth() + "'" +
            ", rfile='" + getRfile() + "'" +
            ", plabel='" + getPlabel() + "'" +
            ", adsh=" + getAdshId() +
            ", tag=" + getTagId() +
            "}";
    }
}
