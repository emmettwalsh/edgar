package com.emmett.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;
import com.emmett.domain.enumeration.EtlErrorType;

/**
 * A DTO for the {@link com.emmett.domain.ETLError} entity.
 */
public class ETLErrorDTO implements Serializable {

    private Long id;

    @NotNull
    private String domain;

    @NotNull
    private EtlErrorType type;

    @Size(max = 8)
    private String yearAndQuarter;

    @Lob
    private String input;

    @Lob
    private String output;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public EtlErrorType getType() {
        return type;
    }

    public void setType(EtlErrorType type) {
        this.type = type;
    }

    public String getYearAndQuarter() {
        return yearAndQuarter;
    }

    public void setYearAndQuarter(String yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ETLErrorDTO eTLErrorDTO = (ETLErrorDTO) o;
        if (eTLErrorDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eTLErrorDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ETLErrorDTO{" +
            "id=" + getId() +
            ", domain='" + getDomain() + "'" +
            ", type='" + getType() + "'" +
            ", yearAndQuarter='" + getYearAndQuarter() + "'" +
            ", input='" + getInput() + "'" +
            ", output='" + getOutput() + "'" +
            "}";
    }
}
