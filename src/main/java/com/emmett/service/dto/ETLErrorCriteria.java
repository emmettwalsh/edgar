package com.emmett.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.emmett.domain.enumeration.EtlErrorType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.emmett.domain.ETLError} entity. This class is used
 * in {@link com.emmett.web.rest.ETLErrorResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /etl-errors?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ETLErrorCriteria implements Serializable, Criteria {
    /**
     * Class for filtering EtlErrorType
     */
    public static class EtlErrorTypeFilter extends Filter<EtlErrorType> {

        public EtlErrorTypeFilter() {
        }

        public EtlErrorTypeFilter(EtlErrorTypeFilter filter) {
            super(filter);
        }

        @Override
        public EtlErrorTypeFilter copy() {
            return new EtlErrorTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter domain;

    private EtlErrorTypeFilter type;

    private StringFilter yearAndQuarter;

    public ETLErrorCriteria(){
    }

    public ETLErrorCriteria(ETLErrorCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.domain = other.domain == null ? null : other.domain.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.yearAndQuarter = other.yearAndQuarter == null ? null : other.yearAndQuarter.copy();
    }

    @Override
    public ETLErrorCriteria copy() {
        return new ETLErrorCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDomain() {
        return domain;
    }

    public void setDomain(StringFilter domain) {
        this.domain = domain;
    }

    public EtlErrorTypeFilter getType() {
        return type;
    }

    public void setType(EtlErrorTypeFilter type) {
        this.type = type;
    }

    public StringFilter getYearAndQuarter() {
        return yearAndQuarter;
    }

    public void setYearAndQuarter(StringFilter yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ETLErrorCriteria that = (ETLErrorCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(domain, that.domain) &&
            Objects.equals(type, that.type) &&
            Objects.equals(yearAndQuarter, that.yearAndQuarter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        domain,
        type,
        yearAndQuarter
        );
    }

    @Override
    public String toString() {
        return "ETLErrorCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (domain != null ? "domain=" + domain + ", " : "") +
                (type != null ? "type=" + type + ", " : "") +
                (yearAndQuarter != null ? "yearAndQuarter=" + yearAndQuarter + ", " : "") +
            "}";
    }

}
