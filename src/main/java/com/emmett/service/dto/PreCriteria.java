package com.emmett.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.emmett.domain.Pre} entity. This class is used
 * in {@link com.emmett.web.rest.PreResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /pres?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PreCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter report;

    private IntegerFilter line;

    private StringFilter statement;

    private BooleanFilter inpth;

    private StringFilter rfile;

    private StringFilter plabel;

    private LongFilter adshId;

    private LongFilter tagId;

    public PreCriteria(){
    }

    public PreCriteria(PreCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.report = other.report == null ? null : other.report.copy();
        this.line = other.line == null ? null : other.line.copy();
        this.statement = other.statement == null ? null : other.statement.copy();
        this.inpth = other.inpth == null ? null : other.inpth.copy();
        this.rfile = other.rfile == null ? null : other.rfile.copy();
        this.plabel = other.plabel == null ? null : other.plabel.copy();
        this.adshId = other.adshId == null ? null : other.adshId.copy();
        this.tagId = other.tagId == null ? null : other.tagId.copy();
    }

    @Override
    public PreCriteria copy() {
        return new PreCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getReport() {
        return report;
    }

    public void setReport(IntegerFilter report) {
        this.report = report;
    }

    public IntegerFilter getLine() {
        return line;
    }

    public void setLine(IntegerFilter line) {
        this.line = line;
    }

    public StringFilter getStatement() {
        return statement;
    }

    public void setStatement(StringFilter statement) {
        this.statement = statement;
    }

    public BooleanFilter getInpth() {
        return inpth;
    }

    public void setInpth(BooleanFilter inpth) {
        this.inpth = inpth;
    }

    public StringFilter getRfile() {
        return rfile;
    }

    public void setRfile(StringFilter rfile) {
        this.rfile = rfile;
    }

    public StringFilter getPlabel() {
        return plabel;
    }

    public void setPlabel(StringFilter plabel) {
        this.plabel = plabel;
    }

    public LongFilter getAdshId() {
        return adshId;
    }

    public void setAdshId(LongFilter adshId) {
        this.adshId = adshId;
    }

    public LongFilter getTagId() {
        return tagId;
    }

    public void setTagId(LongFilter tagId) {
        this.tagId = tagId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PreCriteria that = (PreCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(report, that.report) &&
            Objects.equals(line, that.line) &&
            Objects.equals(statement, that.statement) &&
            Objects.equals(inpth, that.inpth) &&
            Objects.equals(rfile, that.rfile) &&
            Objects.equals(plabel, that.plabel) &&
            Objects.equals(adshId, that.adshId) &&
            Objects.equals(tagId, that.tagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        report,
        line,
        statement,
        inpth,
        rfile,
        plabel,
        adshId,
        tagId
        );
    }

    @Override
    public String toString() {
        return "PreCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (report != null ? "report=" + report + ", " : "") +
                (line != null ? "line=" + line + ", " : "") +
                (statement != null ? "statement=" + statement + ", " : "") +
                (inpth != null ? "inpth=" + inpth + ", " : "") +
                (rfile != null ? "rfile=" + rfile + ", " : "") +
                (plabel != null ? "plabel=" + plabel + ", " : "") +
                (adshId != null ? "adshId=" + adshId + ", " : "") +
                (tagId != null ? "tagId=" + tagId + ", " : "") +
            "}";
    }

}
