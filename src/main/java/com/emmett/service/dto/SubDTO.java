package com.emmett.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.emmett.domain.Sub} entity.
 */
public class SubDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 20)
    private String adsh;

    @NotNull
    @Max(value = 9999999999L)
    private Long cik;

    @NotNull
    @Size(max = 150)
    private String name;

    @Max(value = 9999)
    private Integer sic;

    @NotNull
    @Size(max = 2)
    private String countryba;

    @Size(max = 2)
    private String stprba;

    @NotNull
    @Size(max = 30)
    private String cityba;

    @Size(max = 10)
    private String zipba;

    @Size(max = 40)
    private String bas1;

    @Size(max = 40)
    private String bas2;

    @Size(max = 12)
    private String baph;

    @Size(max = 2)
    private String countryma;

    @Size(max = 2)
    private String stprma;

    @Size(max = 30)
    private String cityma;

    @Size(max = 10)
    private String zipma;

    @Size(max = 40)
    private String mas1;

    @Size(max = 40)
    private String mas2;

    @NotNull
    @Size(max = 3)
    private String countryinc;

    @Size(max = 2)
    private String stprinc;

    @NotNull
    @Max(value = 9999999999L)
    private Long ein;

    @Size(max = 150)
    private String former;

    @Size(max = 8)
    private String changed;

    @Size(max = 5)
    private String afs;

    @NotNull
    private Boolean wksi;

    @NotNull
    @Size(max = 4)
    private String fye;

    @NotNull
    @Size(max = 10)
    private String form;

    @NotNull
    private Instant period;

    @NotNull
    @Max(value = 9999)
    private Integer fy;

    @NotNull
    @Size(max = 2)
    private String fp;

    @NotNull
    private Instant filed;

    @NotNull
    private Instant accepted;

    @NotNull
    private Boolean prevrpt;

    @NotNull
    private Boolean detail;

    @NotNull
    @Size(max = 32)
    private String instance;

    @NotNull
    @Max(value = 9999)
    private Integer nciks;

    @Size(max = 120)
    private String aciks;

    @NotNull
    @Size(max = 6)
    private String yearAndQuarter;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdsh() {
        return adsh;
    }

    public void setAdsh(String adsh) {
        this.adsh = adsh;
    }

    public Long getCik() {
        return cik;
    }

    public void setCik(Long cik) {
        this.cik = cik;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSic() {
        return sic;
    }

    public void setSic(Integer sic) {
        this.sic = sic;
    }

    public String getCountryba() {
        return countryba;
    }

    public void setCountryba(String countryba) {
        this.countryba = countryba;
    }

    public String getStprba() {
        return stprba;
    }

    public void setStprba(String stprba) {
        this.stprba = stprba;
    }

    public String getCityba() {
        return cityba;
    }

    public void setCityba(String cityba) {
        this.cityba = cityba;
    }

    public String getZipba() {
        return zipba;
    }

    public void setZipba(String zipba) {
        this.zipba = zipba;
    }

    public String getBas1() {
        return bas1;
    }

    public void setBas1(String bas1) {
        this.bas1 = bas1;
    }

    public String getBas2() {
        return bas2;
    }

    public void setBas2(String bas2) {
        this.bas2 = bas2;
    }

    public String getBaph() {
        return baph;
    }

    public void setBaph(String baph) {
        this.baph = baph;
    }

    public String getCountryma() {
        return countryma;
    }

    public void setCountryma(String countryma) {
        this.countryma = countryma;
    }

    public String getStprma() {
        return stprma;
    }

    public void setStprma(String stprma) {
        this.stprma = stprma;
    }

    public String getCityma() {
        return cityma;
    }

    public void setCityma(String cityma) {
        this.cityma = cityma;
    }

    public String getZipma() {
        return zipma;
    }

    public void setZipma(String zipma) {
        this.zipma = zipma;
    }

    public String getMas1() {
        return mas1;
    }

    public void setMas1(String mas1) {
        this.mas1 = mas1;
    }

    public String getMas2() {
        return mas2;
    }

    public void setMas2(String mas2) {
        this.mas2 = mas2;
    }

    public String getCountryinc() {
        return countryinc;
    }

    public void setCountryinc(String countryinc) {
        this.countryinc = countryinc;
    }

    public String getStprinc() {
        return stprinc;
    }

    public void setStprinc(String stprinc) {
        this.stprinc = stprinc;
    }

    public Long getEin() {
        return ein;
    }

    public void setEin(Long ein) {
        this.ein = ein;
    }

    public String getFormer() {
        return former;
    }

    public void setFormer(String former) {
        this.former = former;
    }

    public String getChanged() {
        return changed;
    }

    public void setChanged(String changed) {
        this.changed = changed;
    }

    public String getAfs() {
        return afs;
    }

    public void setAfs(String afs) {
        this.afs = afs;
    }

    public Boolean isWksi() {
        return wksi;
    }

    public void setWksi(Boolean wksi) {
        this.wksi = wksi;
    }

    public String getFye() {
        return fye;
    }

    public void setFye(String fye) {
        this.fye = fye;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public Instant getPeriod() {
        return period;
    }

    public void setPeriod(Instant period) {
        this.period = period;
    }

    public Integer getFy() {
        return fy;
    }

    public void setFy(Integer fy) {
        this.fy = fy;
    }

    public String getFp() {
        return fp;
    }

    public void setFp(String fp) {
        this.fp = fp;
    }

    public Instant getFiled() {
        return filed;
    }

    public void setFiled(Instant filed) {
        this.filed = filed;
    }

    public Instant getAccepted() {
        return accepted;
    }

    public void setAccepted(Instant accepted) {
        this.accepted = accepted;
    }

    public Boolean isPrevrpt() {
        return prevrpt;
    }

    public void setPrevrpt(Boolean prevrpt) {
        this.prevrpt = prevrpt;
    }

    public Boolean isDetail() {
        return detail;
    }

    public void setDetail(Boolean detail) {
        this.detail = detail;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public Integer getNciks() {
        return nciks;
    }

    public void setNciks(Integer nciks) {
        this.nciks = nciks;
    }

    public String getAciks() {
        return aciks;
    }

    public void setAciks(String aciks) {
        this.aciks = aciks;
    }

    public String getYearAndQuarter() {
        return yearAndQuarter;
    }

    public void setYearAndQuarter(String yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SubDTO subDTO = (SubDTO) o;
        if (subDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), subDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SubDTO{" +
            "id=" + getId() +
            ", adsh='" + getAdsh() + "'" +
            ", cik=" + getCik() +
            ", name='" + getName() + "'" +
            ", sic=" + getSic() +
            ", countryba='" + getCountryba() + "'" +
            ", stprba='" + getStprba() + "'" +
            ", cityba='" + getCityba() + "'" +
            ", zipba='" + getZipba() + "'" +
            ", bas1='" + getBas1() + "'" +
            ", bas2='" + getBas2() + "'" +
            ", baph='" + getBaph() + "'" +
            ", countryma='" + getCountryma() + "'" +
            ", stprma='" + getStprma() + "'" +
            ", cityma='" + getCityma() + "'" +
            ", zipma='" + getZipma() + "'" +
            ", mas1='" + getMas1() + "'" +
            ", mas2='" + getMas2() + "'" +
            ", countryinc='" + getCountryinc() + "'" +
            ", stprinc='" + getStprinc() + "'" +
            ", ein=" + getEin() +
            ", former='" + getFormer() + "'" +
            ", changed='" + getChanged() + "'" +
            ", afs='" + getAfs() + "'" +
            ", wksi='" + isWksi() + "'" +
            ", fye='" + getFye() + "'" +
            ", form='" + getForm() + "'" +
            ", period='" + getPeriod() + "'" +
            ", fy=" + getFy() +
            ", fp='" + getFp() + "'" +
            ", filed='" + getFiled() + "'" +
            ", accepted='" + getAccepted() + "'" +
            ", prevrpt='" + isPrevrpt() + "'" +
            ", detail='" + isDetail() + "'" +
            ", instance='" + getInstance() + "'" +
            ", nciks=" + getNciks() +
            ", aciks='" + getAciks() + "'" +
            ", yearAndQuarter='" + getYearAndQuarter() + "'" +
            "}";
    }
}
