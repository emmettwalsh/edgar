package com.emmett.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.BigDecimalFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.emmett.domain.Num} entity. This class is used
 * in {@link com.emmett.web.rest.NumResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /nums?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class NumCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter coreg;

    private InstantFilter ddate;

    private LongFilter qtrs;

    private StringFilter uom;

    private BigDecimalFilter value;

    private StringFilter footnote;

    private LongFilter adshId;

    private LongFilter tagId;

    public NumCriteria(){
    }

    public NumCriteria(NumCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.coreg = other.coreg == null ? null : other.coreg.copy();
        this.ddate = other.ddate == null ? null : other.ddate.copy();
        this.qtrs = other.qtrs == null ? null : other.qtrs.copy();
        this.uom = other.uom == null ? null : other.uom.copy();
        this.value = other.value == null ? null : other.value.copy();
        this.footnote = other.footnote == null ? null : other.footnote.copy();
        this.adshId = other.adshId == null ? null : other.adshId.copy();
        this.tagId = other.tagId == null ? null : other.tagId.copy();
    }

    @Override
    public NumCriteria copy() {
        return new NumCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getCoreg() {
        return coreg;
    }

    public void setCoreg(IntegerFilter coreg) {
        this.coreg = coreg;
    }

    public InstantFilter getDdate() {
        return ddate;
    }

    public void setDdate(InstantFilter ddate) {
        this.ddate = ddate;
    }

    public LongFilter getQtrs() {
        return qtrs;
    }

    public void setQtrs(LongFilter qtrs) {
        this.qtrs = qtrs;
    }

    public StringFilter getUom() {
        return uom;
    }

    public void setUom(StringFilter uom) {
        this.uom = uom;
    }

    public BigDecimalFilter getValue() {
        return value;
    }

    public void setValue(BigDecimalFilter value) {
        this.value = value;
    }

    public StringFilter getFootnote() {
        return footnote;
    }

    public void setFootnote(StringFilter footnote) {
        this.footnote = footnote;
    }

    public LongFilter getAdshId() {
        return adshId;
    }

    public void setAdshId(LongFilter adshId) {
        this.adshId = adshId;
    }

    public LongFilter getTagId() {
        return tagId;
    }

    public void setTagId(LongFilter tagId) {
        this.tagId = tagId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final NumCriteria that = (NumCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(coreg, that.coreg) &&
            Objects.equals(ddate, that.ddate) &&
            Objects.equals(qtrs, that.qtrs) &&
            Objects.equals(uom, that.uom) &&
            Objects.equals(value, that.value) &&
            Objects.equals(footnote, that.footnote) &&
            Objects.equals(adshId, that.adshId) &&
            Objects.equals(tagId, that.tagId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        coreg,
        ddate,
        qtrs,
        uom,
        value,
        footnote,
        adshId,
        tagId
        );
    }

    @Override
    public String toString() {
        return "NumCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (coreg != null ? "coreg=" + coreg + ", " : "") +
                (ddate != null ? "ddate=" + ddate + ", " : "") +
                (qtrs != null ? "qtrs=" + qtrs + ", " : "") +
                (uom != null ? "uom=" + uom + ", " : "") +
                (value != null ? "value=" + value + ", " : "") +
                (footnote != null ? "footnote=" + footnote + ", " : "") +
                (adshId != null ? "adshId=" + adshId + ", " : "") +
                (tagId != null ? "tagId=" + tagId + ", " : "") +
            "}";
    }

}
