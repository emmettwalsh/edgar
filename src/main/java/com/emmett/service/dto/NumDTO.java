package com.emmett.service.dto;
import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the {@link com.emmett.domain.Num} entity.
 */
public class NumDTO implements Serializable {

    private Long id;

    @Max(value = 9999)
    private Integer coreg;

    @NotNull
    private Instant ddate;

    @NotNull
    @Max(value = 99999999L)
    private Long qtrs;

    @NotNull
    @Size(max = 20)
    private String uom;

    @NotNull
    @DecimalMax(value = "1e+30")
    private BigDecimal value;

    @Size(max = 512)
    private String footnote;


    private Long adshId;

    private String adshText;

    private Long tagId;

    private String tagTag;

    private String tagVersion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCoreg() {
        return coreg;
    }

    public void setCoreg(Integer coreg) {
        this.coreg = coreg;
    }

    public Instant getDdate() {
        return ddate;
    }

    public void setDdate(Instant ddate) {
        this.ddate = ddate;
    }

    public Long getQtrs() {
        return qtrs;
    }

    public void setQtrs(Long qtrs) {
        this.qtrs = qtrs;
    }

    public String getUom() {
        return uom;
    }

    public void setTagTag(String tagTag) {
        this.tagTag = tagTag;
    }

    public String getTagTag() {
        return tagTag;
    }

    public void setTagVersion(String tagVersion) {
        this.tagVersion = tagVersion;
    }

    public String getTagVersion() {
        return tagVersion;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getFootnote() {
        return footnote;
    }

    public void setFootnote(String footnote) {
        this.footnote = footnote;
    }

    public Long getAdshId() {
        return adshId;
    }

    public void setAdshId(Long subId) {
        this.adshId = subId;
    }

    public String getAdshText() {
        return adshText;
    }

    public void setAdshText(String adshText) {
        this.adshText = adshText;
    }

    public Long getTagId() {
        return tagId;
    }

    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NumDTO numDTO = (NumDTO) o;
        if (numDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), numDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "NumDTO{" +
            "id=" + getId() +
            ", coreg=" + getCoreg() +
            ", ddate='" + getDdate() + "'" +
            ", qtrs=" + getQtrs() +
            ", uom='" + getUom() + "'" +
            ", value=" + getValue() +
            ", footnote='" + getFootnote() + "'" +
            ", adsh=" + getAdshId() +
            ", tag=" + getTagId() +
            "}";
    }
}
