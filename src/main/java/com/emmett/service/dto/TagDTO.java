package com.emmett.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.emmett.domain.Tag} entity.
 */
public class TagDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 256)
    private String tag;

    @NotNull
    @Size(max = 20)
    private String version;

    @NotNull
    private Boolean custom;

    @NotNull
    private Boolean abstrct;

    @Size(max = 20)
    private String datatype;

    @NotNull
    @Size(max = 1)
    private String iord;

    @Size(max = 1)
    private String crdr;

    @Size(max = 512)
    private String tlabel;

    @Size(max = 2048)
    private String doc;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean isCustom() {
        return custom;
    }

    public void setCustom(Boolean custom) {
        this.custom = custom;
    }

    public Boolean isAbstrct() {
        return abstrct;
    }

    public void setAbstrct(Boolean abstrct) {
        this.abstrct = abstrct;
    }

    public String getDatatype() {
        return datatype;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public String getIord() {
        return iord;
    }

    public void setIord(String iord) {
        this.iord = iord;
    }

    public String getCrdr() {
        return crdr;
    }

    public void setCrdr(String crdr) {
        this.crdr = crdr;
    }

    public String getTlabel() {
        return tlabel;
    }

    public void setTlabel(String tlabel) {
        this.tlabel = tlabel;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TagDTO tagDTO = (TagDTO) o;
        if (tagDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tagDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TagDTO{" +
            "id=" + getId() +
            ", tag='" + getTag() + "'" +
            ", version='" + getVersion() + "'" +
            ", custom='" + isCustom() + "'" +
            ", abstrct='" + isAbstrct() + "'" +
            ", datatype='" + getDatatype() + "'" +
            ", iord='" + getIord() + "'" +
            ", crdr='" + getCrdr() + "'" +
            ", tlabel='" + getTlabel() + "'" +
            ", doc='" + getDoc() + "'" +
            "}";
    }
}
