package com.emmett.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.emmett.domain.Tag} entity. This class is used
 * in {@link com.emmett.web.rest.TagResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /tags?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class TagCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tag;

    private StringFilter version;

    private BooleanFilter custom;

    private BooleanFilter abstrct;

    private StringFilter datatype;

    private StringFilter iord;

    private StringFilter crdr;

    private StringFilter tlabel;

    private StringFilter doc;

    public TagCriteria(){
    }

    public TagCriteria(TagCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.tag = other.tag == null ? null : other.tag.copy();
        this.version = other.version == null ? null : other.version.copy();
        this.custom = other.custom == null ? null : other.custom.copy();
        this.abstrct = other.abstrct == null ? null : other.abstrct.copy();
        this.datatype = other.datatype == null ? null : other.datatype.copy();
        this.iord = other.iord == null ? null : other.iord.copy();
        this.crdr = other.crdr == null ? null : other.crdr.copy();
        this.tlabel = other.tlabel == null ? null : other.tlabel.copy();
        this.doc = other.doc == null ? null : other.doc.copy();
    }

    @Override
    public TagCriteria copy() {
        return new TagCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTag() {
        return tag;
    }

    public void setTag(StringFilter tag) {
        this.tag = tag;
    }

    public StringFilter getVersion() {
        return version;
    }

    public void setVersion(StringFilter version) {
        this.version = version;
    }

    public BooleanFilter getCustom() {
        return custom;
    }

    public void setCustom(BooleanFilter custom) {
        this.custom = custom;
    }

    public BooleanFilter getAbstrct() {
        return abstrct;
    }

    public void setAbstrct(BooleanFilter abstrct) {
        this.abstrct = abstrct;
    }

    public StringFilter getDatatype() {
        return datatype;
    }

    public void setDatatype(StringFilter datatype) {
        this.datatype = datatype;
    }

    public StringFilter getIord() {
        return iord;
    }

    public void setIord(StringFilter iord) {
        this.iord = iord;
    }

    public StringFilter getCrdr() {
        return crdr;
    }

    public void setCrdr(StringFilter crdr) {
        this.crdr = crdr;
    }

    public StringFilter getTlabel() {
        return tlabel;
    }

    public void setTlabel(StringFilter tlabel) {
        this.tlabel = tlabel;
    }

    public StringFilter getDoc() {
        return doc;
    }

    public void setDoc(StringFilter doc) {
        this.doc = doc;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final TagCriteria that = (TagCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tag, that.tag) &&
            Objects.equals(version, that.version) &&
            Objects.equals(custom, that.custom) &&
            Objects.equals(abstrct, that.abstrct) &&
            Objects.equals(datatype, that.datatype) &&
            Objects.equals(iord, that.iord) &&
            Objects.equals(crdr, that.crdr) &&
            Objects.equals(tlabel, that.tlabel) &&
            Objects.equals(doc, that.doc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tag,
        version,
        custom,
        abstrct,
        datatype,
        iord,
        crdr,
        tlabel,
        doc
        );
    }

    @Override
    public String toString() {
        return "TagCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tag != null ? "tag=" + tag + ", " : "") +
                (version != null ? "version=" + version + ", " : "") +
                (custom != null ? "custom=" + custom + ", " : "") +
                (abstrct != null ? "abstrct=" + abstrct + ", " : "") +
                (datatype != null ? "datatype=" + datatype + ", " : "") +
                (iord != null ? "iord=" + iord + ", " : "") +
                (crdr != null ? "crdr=" + crdr + ", " : "") +
                (tlabel != null ? "tlabel=" + tlabel + ", " : "") +
                (doc != null ? "doc=" + doc + ", " : "") +
            "}";
    }

}
