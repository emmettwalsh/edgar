package com.emmett.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.emmett.domain.Sub;
import com.emmett.domain.*; // for static metamodels
import com.emmett.repository.SubRepository;
import com.emmett.service.dto.SubCriteria;
import com.emmett.service.dto.SubDTO;
import com.emmett.service.mapper.SubMapper;

/**
 * Service for executing complex queries for {@link Sub} entities in the database.
 * The main input is a {@link SubCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link SubDTO} or a {@link Page} of {@link SubDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SubQueryService extends QueryService<Sub> {

    private final Logger log = LoggerFactory.getLogger(SubQueryService.class);

    private final SubRepository subRepository;

    private final SubMapper subMapper;

    public SubQueryService(SubRepository subRepository, SubMapper subMapper) {
        this.subRepository = subRepository;
        this.subMapper = subMapper;
    }

    /**
     * Return a {@link List} of {@link SubDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<SubDTO> findByCriteria(SubCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sub> specification = createSpecification(criteria);
        return subMapper.toDto(subRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link SubDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<SubDTO> findByCriteria(SubCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sub> specification = createSpecification(criteria);
        return subRepository.findAll(specification, page)
            .map(subMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SubCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Sub> specification = createSpecification(criteria);
        return subRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<Sub> createSpecification(SubCriteria criteria) {
        Specification<Sub> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Sub_.id));
            }
            if (criteria.getAdsh() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAdsh(), Sub_.adsh));
            }
            if (criteria.getCik() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCik(), Sub_.cik));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Sub_.name));
            }
            if (criteria.getSic() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSic(), Sub_.sic));
            }
            if (criteria.getCountryba() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryba(), Sub_.countryba));
            }
            if (criteria.getStprba() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStprba(), Sub_.stprba));
            }
            if (criteria.getCityba() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCityba(), Sub_.cityba));
            }
            if (criteria.getZipba() != null) {
                specification = specification.and(buildStringSpecification(criteria.getZipba(), Sub_.zipba));
            }
            if (criteria.getBas1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBas1(), Sub_.bas1));
            }
            if (criteria.getBas2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBas2(), Sub_.bas2));
            }
            if (criteria.getBaph() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBaph(), Sub_.baph));
            }
            if (criteria.getCountryma() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryma(), Sub_.countryma));
            }
            if (criteria.getStprma() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStprma(), Sub_.stprma));
            }
            if (criteria.getCityma() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCityma(), Sub_.cityma));
            }
            if (criteria.getZipma() != null) {
                specification = specification.and(buildStringSpecification(criteria.getZipma(), Sub_.zipma));
            }
            if (criteria.getMas1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMas1(), Sub_.mas1));
            }
            if (criteria.getMas2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMas2(), Sub_.mas2));
            }
            if (criteria.getCountryinc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCountryinc(), Sub_.countryinc));
            }
            if (criteria.getStprinc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStprinc(), Sub_.stprinc));
            }
            if (criteria.getEin() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEin(), Sub_.ein));
            }
            if (criteria.getFormer() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFormer(), Sub_.former));
            }
            if (criteria.getChanged() != null) {
                specification = specification.and(buildStringSpecification(criteria.getChanged(), Sub_.changed));
            }
            if (criteria.getAfs() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAfs(), Sub_.afs));
            }
            if (criteria.getWksi() != null) {
                specification = specification.and(buildSpecification(criteria.getWksi(), Sub_.wksi));
            }
            if (criteria.getFye() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFye(), Sub_.fye));
            }
            if (criteria.getForm() != null) {
                specification = specification.and(buildStringSpecification(criteria.getForm(), Sub_.form));
            }
            if (criteria.getPeriod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPeriod(), Sub_.period));
            }
            if (criteria.getFy() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFy(), Sub_.fy));
            }
            if (criteria.getFp() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFp(), Sub_.fp));
            }
            if (criteria.getFiled() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFiled(), Sub_.filed));
            }
            if (criteria.getAccepted() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAccepted(), Sub_.accepted));
            }
            if (criteria.getPrevrpt() != null) {
                specification = specification.and(buildSpecification(criteria.getPrevrpt(), Sub_.prevrpt));
            }
            if (criteria.getDetail() != null) {
                specification = specification.and(buildSpecification(criteria.getDetail(), Sub_.detail));
            }
            if (criteria.getInstance() != null) {
                specification = specification.and(buildStringSpecification(criteria.getInstance(), Sub_.instance));
            }
            if (criteria.getNciks() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNciks(), Sub_.nciks));
            }
            if (criteria.getAciks() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAciks(), Sub_.aciks));
            }
            if (criteria.getYearAndQuarter() != null) {
                specification = specification.and(buildStringSpecification(criteria.getYearAndQuarter(), Sub_.yearAndQuarter));
            }
        }
        return specification;
    }
}
