package com.emmett.service;

import com.emmett.domain.ETLError;
import com.emmett.repository.ETLErrorRepository;
import com.emmett.service.dto.ETLErrorDTO;
import com.emmett.service.mapper.ETLErrorMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ETLError}.
 */
@Service
@Transactional
public class ETLErrorService {

    private final Logger log = LoggerFactory.getLogger(ETLErrorService.class);

    private final ETLErrorRepository eTLErrorRepository;

    private final ETLErrorMapper eTLErrorMapper;

    public ETLErrorService(ETLErrorRepository eTLErrorRepository, ETLErrorMapper eTLErrorMapper) {
        this.eTLErrorRepository = eTLErrorRepository;
        this.eTLErrorMapper = eTLErrorMapper;
    }

    /**
     * Save a eTLError.
     *
     * @param eTLErrorDTO the entity to save.
     * @return the persisted entity.
     */
    public ETLErrorDTO save(ETLErrorDTO eTLErrorDTO) {
        log.debug("Request to save ETLError : {}", eTLErrorDTO);
        ETLError eTLError = eTLErrorMapper.toEntity(eTLErrorDTO);
        eTLError = eTLErrorRepository.save(eTLError);
        return eTLErrorMapper.toDto(eTLError);
    }

    /**
     * Get all the eTLErrors.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<ETLErrorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ETLErrors");
        return eTLErrorRepository.findAll(pageable)
            .map(eTLErrorMapper::toDto);
    }


    /**
     * Get one eTLError by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ETLErrorDTO> findOne(Long id) {
        log.debug("Request to get ETLError : {}", id);
        return eTLErrorRepository.findById(id)
            .map(eTLErrorMapper::toDto);
    }

    /**
     * Delete the eTLError by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ETLError : {}", id);
        eTLErrorRepository.deleteById(id);
    }
}
