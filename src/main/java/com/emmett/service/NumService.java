package com.emmett.service;

import com.emmett.domain.Num;
import com.emmett.repository.NumRepository;
import com.emmett.service.dto.NumDTO;
import com.emmett.service.mapper.NumMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Num}.
 */
@Service
@Transactional
public class NumService {

    private final Logger log = LoggerFactory.getLogger(NumService.class);

    private final NumRepository numRepository;

    private final NumMapper numMapper;

    public NumService(NumRepository numRepository, NumMapper numMapper) {
        this.numRepository = numRepository;
        this.numMapper = numMapper;
    }

    /**
     * Save a num.
     *
     * @param numDTO the entity to save.
     * @return the persisted entity.
     */
    public NumDTO save(NumDTO numDTO) {
        log.debug("Request to save Num : {}", numDTO);
        Num num = numMapper.toEntity(numDTO);
        num = numRepository.save(num);
        return numMapper.toDto(num);
    }

    /**
     * Get all the nums.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NumDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Nums");
        return numRepository.findAll(pageable)
            .map(numMapper::toDto);
    }


    /**
     * Get one num by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NumDTO> findOne(Long id) {
        log.debug("Request to get Num : {}", id);
        return numRepository.findById(id)
            .map(numMapper::toDto);
    }

    /**
     * Delete the num by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Num : {}", id);
        numRepository.deleteById(id);
    }
}
