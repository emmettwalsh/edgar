package com.emmett.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.emmett.domain.Num;
import com.emmett.domain.*; // for static metamodels
import com.emmett.repository.NumRepository;
import com.emmett.service.dto.NumCriteria;
import com.emmett.service.dto.NumDTO;
import com.emmett.service.mapper.NumMapper;

/**
 * Service for executing complex queries for {@link Num} entities in the database.
 * The main input is a {@link NumCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link NumDTO} or a {@link Page} of {@link NumDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class NumQueryService extends QueryService<Num> {

    private final Logger log = LoggerFactory.getLogger(NumQueryService.class);

    private final NumRepository numRepository;

    private final NumMapper numMapper;

    public NumQueryService(NumRepository numRepository, NumMapper numMapper) {
        this.numRepository = numRepository;
        this.numMapper = numMapper;
    }

    /**
     * Return a {@link List} of {@link NumDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<NumDTO> findByCriteria(NumCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Num> specification = createSpecification(criteria);
        return numMapper.toDto(numRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link NumDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<NumDTO> findByCriteria(NumCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Num> specification = createSpecification(criteria);
        return numRepository.findAll(specification, page)
            .map(numMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(NumCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Num> specification = createSpecification(criteria);
        return numRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<Num> createSpecification(NumCriteria criteria) {
        Specification<Num> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Num_.id));
            }
            if (criteria.getCoreg() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCoreg(), Num_.coreg));
            }
            if (criteria.getDdate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDdate(), Num_.ddate));
            }
            if (criteria.getQtrs() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQtrs(), Num_.qtrs));
            }
            if (criteria.getUom() != null) {
                specification = specification.and(buildStringSpecification(criteria.getUom(), Num_.uom));
            }
            if (criteria.getValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getValue(), Num_.value));
            }
            if (criteria.getFootnote() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFootnote(), Num_.footnote));
            }
            if (criteria.getAdshId() != null) {
                specification = specification.and(buildSpecification(criteria.getAdshId(),
                    root -> root.join(Num_.adsh, JoinType.LEFT).get(Sub_.id)));
            }
            if (criteria.getTagId() != null) {
                specification = specification.and(buildSpecification(criteria.getTagId(),
                    root -> root.join(Num_.tag, JoinType.LEFT).get(Tag_.id)));
            }
        }
        return specification;
    }
}
