package com.emmett.service.mapper;

import com.emmett.domain.*;
import com.emmett.service.dto.PreDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Pre} and its DTO {@link PreDTO}.
 */
@Mapper(componentModel = "spring", uses = {SubMapper.class, TagMapper.class})
public interface PreMapper extends EntityMapper<PreDTO, Pre> {

    @Mapping(source = "adsh.id", target = "adshId")
    @Mapping(source = "adsh.adsh", target = "adshText")
    @Mapping(source = "tag.id", target = "tagId")
    @Mapping(source = "tag.tag", target = "tagTag")
    @Mapping(source = "tag.version", target = "tagVersion")
    PreDTO toDto(Pre pre);

    @Mapping(source = "adshId", target = "adsh")
    @Mapping(source = "tagId", target = "tag")
    Pre toEntity(PreDTO preDTO);

    default Pre fromId(Long id) {
        if (id == null) {
            return null;
        }
        Pre pre = new Pre();
        pre.setId(id);
        return pre;
    }
}
