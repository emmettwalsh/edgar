package com.emmett.service.mapper;

import com.emmett.domain.*;
import com.emmett.service.dto.ETLErrorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ETLError} and its DTO {@link ETLErrorDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ETLErrorMapper extends EntityMapper<ETLErrorDTO, ETLError> {



    default ETLError fromId(Long id) {
        if (id == null) {
            return null;
        }
        ETLError eTLError = new ETLError();
        eTLError.setId(id);
        return eTLError;
    }
}
