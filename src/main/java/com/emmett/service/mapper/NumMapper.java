package com.emmett.service.mapper;

import com.emmett.domain.*;
import com.emmett.service.dto.NumDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Num} and its DTO {@link NumDTO}.
 */
@Mapper(componentModel = "spring", uses = {SubMapper.class, TagMapper.class})
public interface NumMapper extends EntityMapper<NumDTO, Num> {

    @Mapping(source = "adsh.id", target = "adshId")
    @Mapping(source = "adsh.adsh", target = "adshText")
    @Mapping(source = "tag.id", target = "tagId")
    @Mapping(source = "tag.tag", target = "tagTag")
    @Mapping(source = "tag.version", target = "tagVersion")
    NumDTO toDto(Num num);

    @Mapping(source = "adshId", target = "adsh")
    @Mapping(source = "tagId", target = "tag")
    Num toEntity(NumDTO numDTO);

    default Num fromId(Long id) {
        if (id == null) {
            return null;
        }
        Num num = new Num();
        num.setId(id);
        return num;
    }
}
