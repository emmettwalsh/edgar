package com.emmett.service.mapper;

import com.emmett.domain.*;
import com.emmett.service.dto.SubDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sub} and its DTO {@link SubDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface SubMapper extends EntityMapper<SubDTO, Sub> {



    default Sub fromId(Long id) {
        if (id == null) {
            return null;
        }
        Sub sub = new Sub();
        sub.setId(id);
        return sub;
    }
}
