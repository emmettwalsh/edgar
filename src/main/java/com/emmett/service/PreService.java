package com.emmett.service;

import com.emmett.domain.Pre;
import com.emmett.repository.PreRepository;
import com.emmett.service.dto.PreDTO;
import com.emmett.service.mapper.PreMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Pre}.
 */
@Service
@Transactional
public class PreService {

    private final Logger log = LoggerFactory.getLogger(PreService.class);

    private final PreRepository preRepository;

    private final PreMapper preMapper;

    public PreService(PreRepository preRepository, PreMapper preMapper) {
        this.preRepository = preRepository;
        this.preMapper = preMapper;
    }

    /**
     * Save a pre.
     *
     * @param preDTO the entity to save.
     * @return the persisted entity.
     */
    public PreDTO save(PreDTO preDTO) {
        log.debug("Request to save Pre : {}", preDTO);
        Pre pre = preMapper.toEntity(preDTO);
        pre = preRepository.save(pre);
        return preMapper.toDto(pre);
    }

    /**
     * Get all the pres.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PreDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Pres");
        return preRepository.findAll(pageable)
            .map(preMapper::toDto);
    }


    /**
     * Get one pre by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PreDTO> findOne(Long id) {
        log.debug("Request to get Pre : {}", id);
        return preRepository.findById(id)
            .map(preMapper::toDto);
    }

    /**
     * Delete the pre by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Pre : {}", id);
        preRepository.deleteById(id);
    }
}
