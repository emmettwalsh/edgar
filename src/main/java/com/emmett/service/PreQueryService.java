package com.emmett.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.emmett.domain.Pre;
import com.emmett.domain.*; // for static metamodels
import com.emmett.repository.PreRepository;
import com.emmett.service.dto.PreCriteria;
import com.emmett.service.dto.PreDTO;
import com.emmett.service.mapper.PreMapper;

/**
 * Service for executing complex queries for {@link Pre} entities in the database.
 * The main input is a {@link PreCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PreDTO} or a {@link Page} of {@link PreDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PreQueryService extends QueryService<Pre> {

    private final Logger log = LoggerFactory.getLogger(PreQueryService.class);

    private final PreRepository preRepository;

    private final PreMapper preMapper;

    public PreQueryService(PreRepository preRepository, PreMapper preMapper) {
        this.preRepository = preRepository;
        this.preMapper = preMapper;
    }

    /**
     * Return a {@link List} of {@link PreDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PreDTO> findByCriteria(PreCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Pre> specification = createSpecification(criteria);
        return preMapper.toDto(preRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PreDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PreDTO> findByCriteria(PreCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Pre> specification = createSpecification(criteria);
        return preRepository.findAll(specification, page)
            .map(preMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PreCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Pre> specification = createSpecification(criteria);
        return preRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<Pre> createSpecification(PreCriteria criteria) {
        Specification<Pre> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Pre_.id));
            }
            if (criteria.getReport() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getReport(), Pre_.report));
            }
            if (criteria.getLine() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLine(), Pre_.line));
            }
            if (criteria.getStatement() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatement(), Pre_.statement));
            }
            if (criteria.getInpth() != null) {
                specification = specification.and(buildSpecification(criteria.getInpth(), Pre_.inpth));
            }
            if (criteria.getRfile() != null) {
                specification = specification.and(buildStringSpecification(criteria.getRfile(), Pre_.rfile));
            }
            if (criteria.getPlabel() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPlabel(), Pre_.plabel));
            }
            if (criteria.getAdshId() != null) {
                specification = specification.and(buildSpecification(criteria.getAdshId(),
                    root -> root.join(Pre_.adsh, JoinType.LEFT).get(Sub_.id)));
            }
            if (criteria.getTagId() != null) {
                specification = specification.and(buildSpecification(criteria.getTagId(),
                    root -> root.join(Pre_.tag, JoinType.LEFT).get(Tag_.id)));
            }
        }
        return specification;
    }
}
