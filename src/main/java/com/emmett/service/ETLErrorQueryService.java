package com.emmett.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.emmett.domain.ETLError;
import com.emmett.domain.*; // for static metamodels
import com.emmett.repository.ETLErrorRepository;
import com.emmett.service.dto.ETLErrorCriteria;
import com.emmett.service.dto.ETLErrorDTO;
import com.emmett.service.mapper.ETLErrorMapper;

/**
 * Service for executing complex queries for {@link ETLError} entities in the database.
 * The main input is a {@link ETLErrorCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ETLErrorDTO} or a {@link Page} of {@link ETLErrorDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ETLErrorQueryService extends QueryService<ETLError> {

    private final Logger log = LoggerFactory.getLogger(ETLErrorQueryService.class);

    private final ETLErrorRepository eTLErrorRepository;

    private final ETLErrorMapper eTLErrorMapper;

    public ETLErrorQueryService(ETLErrorRepository eTLErrorRepository, ETLErrorMapper eTLErrorMapper) {
        this.eTLErrorRepository = eTLErrorRepository;
        this.eTLErrorMapper = eTLErrorMapper;
    }

    /**
     * Return a {@link List} of {@link ETLErrorDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ETLErrorDTO> findByCriteria(ETLErrorCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ETLError> specification = createSpecification(criteria);
        return eTLErrorMapper.toDto(eTLErrorRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ETLErrorDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ETLErrorDTO> findByCriteria(ETLErrorCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ETLError> specification = createSpecification(criteria);
        return eTLErrorRepository.findAll(specification, page)
            .map(eTLErrorMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ETLErrorCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ETLError> specification = createSpecification(criteria);
        return eTLErrorRepository.count(specification);
    }

    /**
     * Function to convert ConsumerCriteria to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */    
    private Specification<ETLError> createSpecification(ETLErrorCriteria criteria) {
        Specification<ETLError> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), ETLError_.id));
            }
            if (criteria.getDomain() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDomain(), ETLError_.domain));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), ETLError_.type));
            }
            if (criteria.getYearAndQuarter() != null) {
                specification = specification.and(buildStringSpecification(criteria.getYearAndQuarter(), ETLError_.yearAndQuarter));
            }
        }
        return specification;
    }
}
