package com.emmett.service;

import com.emmett.domain.Sub;
import com.emmett.repository.SubRepository;
import com.emmett.service.dto.SubDTO;
import com.emmett.service.mapper.SubMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Sub}.
 */
@Service
@Transactional
public class SubService {

    private final Logger log = LoggerFactory.getLogger(SubService.class);

    private final SubRepository subRepository;

    private final SubMapper subMapper;

    public SubService(SubRepository subRepository, SubMapper subMapper) {
        this.subRepository = subRepository;
        this.subMapper = subMapper;
    }

    /**
     * Save a sub.
     *
     * @param subDTO the entity to save.
     * @return the persisted entity.
     */
    public SubDTO save(SubDTO subDTO) {
        log.debug("Request to save Sub : {}", subDTO);
        Sub sub = subMapper.toEntity(subDTO);
        sub = subRepository.save(sub);
        return subMapper.toDto(sub);
    }

    /**
     * Get all the subs.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<SubDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Subs");
        return subRepository.findAll(pageable)
            .map(subMapper::toDto);
    }


    /**
     * Get one sub by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SubDTO> findOne(Long id) {
        log.debug("Request to get Sub : {}", id);
        return subRepository.findById(id)
            .map(subMapper::toDto);
    }

    /**
     * Delete the sub by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sub : {}", id);
        subRepository.deleteById(id);
    }
}
