

package com.emmett.repository;

import com.emmett.domain.Sub;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Sub entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SubRepository extends JpaRepository<Sub, Long>, JpaSpecificationExecutor<Sub> {

    List<Sub > findByAdsh(String adsh);
}

