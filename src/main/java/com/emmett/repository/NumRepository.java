package com.emmett.repository;

import com.emmett.domain.Num;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Num entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NumRepository extends JpaRepository<Num, Long>, JpaSpecificationExecutor<Num> {

}
