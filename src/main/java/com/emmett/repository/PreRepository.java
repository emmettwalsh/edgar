package com.emmett.repository;

import com.emmett.domain.Pre;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Pre entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PreRepository extends JpaRepository<Pre, Long>, JpaSpecificationExecutor<Pre> {

}
