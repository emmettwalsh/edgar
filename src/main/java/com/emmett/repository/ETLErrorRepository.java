package com.emmett.repository;

import com.emmett.domain.ETLError;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the ETLError entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ETLErrorRepository extends JpaRepository<ETLError, Long>, JpaSpecificationExecutor<ETLError> {

}
