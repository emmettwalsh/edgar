package com.emmett.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.emmett.domain.enumeration.EtlErrorType;

/**
 * A ETLError.
 */
@Entity
@Table(name = "etl_error")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class ETLError implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "domain", nullable = false)
    private String domain;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private EtlErrorType type;

    @Size(max = 8)
    @Column(name = "year_and_quarter", length = 8)
    private String yearAndQuarter;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "input")
    private String input;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "output")
    private String output;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public ETLError domain(String domain) {
        this.domain = domain;
        return this;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public EtlErrorType getType() {
        return type;
    }

    public ETLError type(EtlErrorType type) {
        this.type = type;
        return this;
    }

    public void setType(EtlErrorType type) {
        this.type = type;
    }

    public String getYearAndQuarter() {
        return yearAndQuarter;
    }

    public ETLError yearAndQuarter(String yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
        return this;
    }

    public void setYearAndQuarter(String yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
    }

    public String getInput() {
        return input;
    }

    public ETLError input(String input) {
        this.input = input;
        return this;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getOutput() {
        return output;
    }

    public ETLError output(String output) {
        this.output = output;
        return this;
    }

    public void setOutput(String output) {
        this.output = output;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ETLError)) {
            return false;
        }
        return id != null && id.equals(((ETLError) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "ETLError{" +
            "id=" + getId() +
            ", domain='" + getDomain() + "'" +
            ", type='" + getType() + "'" +
            ", yearAndQuarter='" + getYearAndQuarter() + "'" +
            ", input='" + getInput() + "'" +
            ", output='" + getOutput() + "'" +
            "}";
    }
}
