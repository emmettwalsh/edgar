package com.emmett.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Tag.
 */
@Entity
@Table(name = "tag")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Tag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 256)
    @Column(name = "tag", length = 256, nullable = false)
    private String tag;

    @NotNull
    @Size(max = 20)
    @Column(name = "version", length = 20, nullable = false)
    private String version;

    @NotNull
    @Column(name = "custom", nullable = false)
    private Boolean custom;

    @NotNull
    @Column(name = "abstrct", nullable = false)
    private Boolean abstrct;

    @Size(max = 20)
    @Column(name = "datatype", length = 20)
    private String datatype;

    @NotNull
    @Size(max = 1)
    @Column(name = "iord", length = 1, nullable = false)
    private String iord;

    @Size(max = 1)
    @Column(name = "crdr", length = 1)
    private String crdr;

    @Size(max = 512)
    @Column(name = "tlabel", length = 512)
    private String tlabel;

    @Size(max = 2048)
    @Column(name = "doc", length = 2048)
    private String doc;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTag() {
        return tag;
    }

    public Tag tag(String tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getVersion() {
        return version;
    }

    public Tag version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Boolean isCustom() {
        return custom;
    }

    public Tag custom(Boolean custom) {
        this.custom = custom;
        return this;
    }

    public void setCustom(Boolean custom) {
        this.custom = custom;
    }

    public Boolean isAbstrct() {
        return abstrct;
    }

    public Tag abstrct(Boolean abstrct) {
        this.abstrct = abstrct;
        return this;
    }

    public void setAbstrct(Boolean abstrct) {
        this.abstrct = abstrct;
    }

    public String getDatatype() {
        return datatype;
    }

    public Tag datatype(String datatype) {
        this.datatype = datatype;
        return this;
    }

    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    public String getIord() {
        return iord;
    }

    public Tag iord(String iord) {
        this.iord = iord;
        return this;
    }

    public void setIord(String iord) {
        this.iord = iord;
    }

    public String getCrdr() {
        return crdr;
    }

    public Tag crdr(String crdr) {
        this.crdr = crdr;
        return this;
    }

    public void setCrdr(String crdr) {
        this.crdr = crdr;
    }

    public String getTlabel() {
        return tlabel;
    }

    public Tag tlabel(String tlabel) {
        this.tlabel = tlabel;
        return this;
    }

    public void setTlabel(String tlabel) {
        this.tlabel = tlabel;
    }

    public String getDoc() {
        return doc;
    }

    public Tag doc(String doc) {
        this.doc = doc;
        return this;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Tag)) {
            return false;
        }
        return id != null && id.equals(((Tag) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Tag{" +
            "id=" + getId() +
            ", tag='" + getTag() + "'" +
            ", version='" + getVersion() + "'" +
            ", custom='" + isCustom() + "'" +
            ", abstrct='" + isAbstrct() + "'" +
            ", datatype='" + getDatatype() + "'" +
            ", iord='" + getIord() + "'" +
            ", crdr='" + getCrdr() + "'" +
            ", tlabel='" + getTlabel() + "'" +
            ", doc='" + getDoc() + "'" +
            "}";
    }
}
