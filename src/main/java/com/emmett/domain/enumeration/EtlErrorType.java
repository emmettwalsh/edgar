package com.emmett.domain.enumeration;

/**
 * The EtlErrorType enumeration.
 */
public enum EtlErrorType {
    Extract, Transform, Load, Other
}
