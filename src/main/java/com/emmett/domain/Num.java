package com.emmett.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

/**
 * A Num.
 */
@Entity
@Table(name = "num")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Num implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Max(value = 9999)
    @Column(name = "coreg")
    private Integer coreg;

    @NotNull
    @Column(name = "ddate", nullable = false)
    private Instant ddate;

    @NotNull
    @Max(value = 99999999L)
    @Column(name = "qtrs", nullable = false)
    private Long qtrs;

    @NotNull
    @Size(max = 20)
    @Column(name = "uom", length = 20, nullable = false)
    private String uom;

    @NotNull
    @DecimalMax(value = "1e+30")
    @Column(name = "value", precision = 21, scale = 4, nullable = false)
    private BigDecimal value;

    @Size(max = 512)
    @Column(name = "footnote", length = 512, unique = true)
    private String footnote;

    @ManyToOne
    @JsonIgnoreProperties("nums")
    private Sub adsh;

    @ManyToOne
    @JsonIgnoreProperties("nums")
    private Tag tag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCoreg() {
        return coreg;
    }

    public Num coreg(Integer coreg) {
        this.coreg = coreg;
        return this;
    }

    public void setCoreg(Integer coreg) {
        this.coreg = coreg;
    }

    public Instant getDdate() {
        return ddate;
    }

    public Num ddate(Instant ddate) {
        this.ddate = ddate;
        return this;
    }

    public void setDdate(Instant ddate) {
        this.ddate = ddate;
    }

    public Long getQtrs() {
        return qtrs;
    }

    public Num qtrs(Long qtrs) {
        this.qtrs = qtrs;
        return this;
    }

    public void setQtrs(Long qtrs) {
        this.qtrs = qtrs;
    }

    public String getUom() {
        return uom;
    }

    public Num uom(String uom) {
        this.uom = uom;
        return this;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Num value(BigDecimal value) {
        this.value = value;
        return this;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getFootnote() {
        return footnote;
    }

    public Num footnote(String footnote) {
        this.footnote = footnote;
        return this;
    }

    public void setFootnote(String footnote) {
        this.footnote = footnote;
    }

    public Sub getAdsh() {
        return adsh;
    }

    public Num adsh(Sub sub) {
        this.adsh = sub;
        return this;
    }

    public void setAdsh(Sub sub) {
        this.adsh = sub;
    }

    public Tag getTag() {
        return tag;
    }

    public Num tag(Tag tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Num)) {
            return false;
        }
        return id != null && id.equals(((Num) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Num{" +
            "id=" + getId() +
            ", coreg=" + getCoreg() +
            ", ddate='" + getDdate() + "'" +
            ", qtrs=" + getQtrs() +
            ", uom='" + getUom() + "'" +
            ", value=" + getValue() +
            ", footnote='" + getFootnote() + "'" +
            "}";
    }
}
