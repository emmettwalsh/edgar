package com.emmett.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A Sub.
 */
@Entity
@Table(name = "sub")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Sub implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "adsh", length = 20, nullable = false, unique = true)
    private String adsh;

    @NotNull
    @Max(value = 9999999999L)
    @Column(name = "cik", nullable = false)
    private Long cik;

    @NotNull
    @Size(max = 150)
    @Column(name = "name", length = 150, nullable = false)
    private String name;

    @Max(value = 9999)
    @Column(name = "sic")
    private Integer sic;

    @NotNull
    @Size(max = 2)
    @Column(name = "countryba", length = 2, nullable = false)
    private String countryba;

    @Size(max = 2)
    @Column(name = "stprba", length = 2)
    private String stprba;

    @NotNull
    @Size(max = 30)
    @Column(name = "cityba", length = 30, nullable = false)
    private String cityba;

    @Size(max = 10)
    @Column(name = "zipba", length = 10)
    private String zipba;

    @Size(max = 40)
    @Column(name = "bas_1", length = 40)
    private String bas1;

    @Size(max = 40)
    @Column(name = "bas_2", length = 40)
    private String bas2;

    @Size(max = 12)
    @Column(name = "baph", length = 12)
    private String baph;

    @Size(max = 2)
    @Column(name = "countryma", length = 2)
    private String countryma;

    @Size(max = 2)
    @Column(name = "stprma", length = 2)
    private String stprma;

    @Size(max = 30)
    @Column(name = "cityma", length = 30)
    private String cityma;

    @Size(max = 10)
    @Column(name = "zipma", length = 10)
    private String zipma;

    @Size(max = 40)
    @Column(name = "mas_1", length = 40)
    private String mas1;

    @Size(max = 40)
    @Column(name = "mas_2", length = 40)
    private String mas2;

    @NotNull
    @Size(max = 3)
    @Column(name = "countryinc", length = 3, nullable = false)
    private String countryinc;

    @Size(max = 2)
    @Column(name = "stprinc", length = 2)
    private String stprinc;

    @NotNull
    @Max(value = 9999999999L)
    @Column(name = "ein", nullable = false)
    private Long ein;

    @Size(max = 150)
    @Column(name = "former", length = 150)
    private String former;

    @Size(max = 8)
    @Column(name = "changed", length = 8)
    private String changed;

    @Size(max = 5)
    @Column(name = "afs", length = 5)
    private String afs;

    @NotNull
    @Column(name = "wksi", nullable = false)
    private Boolean wksi;

    @NotNull
    @Size(max = 4)
    @Column(name = "fye", length = 4, nullable = false)
    private String fye;

    @NotNull
    @Size(max = 10)
    @Column(name = "form", length = 10, nullable = false)
    private String form;

    @NotNull
    @Column(name = "period", nullable = false)
    private Instant period;

    @NotNull
    @Max(value = 9999)
    @Column(name = "fy", nullable = false)
    private Integer fy;

    @NotNull
    @Size(max = 2)
    @Column(name = "fp", length = 2, nullable = false)
    private String fp;

    @NotNull
    @Column(name = "filed", nullable = false)
    private Instant filed;

    @NotNull
    @Column(name = "accepted", nullable = false)
    private Instant accepted;

    @NotNull
    @Column(name = "prevrpt", nullable = false)
    private Boolean prevrpt;

    @NotNull
    @Column(name = "detail", nullable = false)
    private Boolean detail;

    @NotNull
    @Size(max = 32)
    @Column(name = "instance", length = 32, nullable = false)
    private String instance;

    @NotNull
    @Max(value = 9999)
    @Column(name = "nciks", nullable = false)
    private Integer nciks;

    @Size(max = 120)
    @Column(name = "aciks", length = 120)
    private String aciks;

    @NotNull
    @Size(max = 6)
    @Column(name = "year_and_quarter", length = 6, nullable = false)
    private String yearAndQuarter;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdsh() {
        return adsh;
    }

    public Sub adsh(String adsh) {
        this.adsh = adsh;
        return this;
    }

    public void setAdsh(String adsh) {
        this.adsh = adsh;
    }

    public Long getCik() {
        return cik;
    }

    public Sub cik(Long cik) {
        this.cik = cik;
        return this;
    }

    public void setCik(Long cik) {
        this.cik = cik;
    }

    public String getName() {
        return name;
    }

    public Sub name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSic() {
        return sic;
    }

    public Sub sic(Integer sic) {
        this.sic = sic;
        return this;
    }

    public void setSic(Integer sic) {
        this.sic = sic;
    }

    public String getCountryba() {
        return countryba;
    }

    public Sub countryba(String countryba) {
        this.countryba = countryba;
        return this;
    }

    public void setCountryba(String countryba) {
        this.countryba = countryba;
    }

    public String getStprba() {
        return stprba;
    }

    public Sub stprba(String stprba) {
        this.stprba = stprba;
        return this;
    }

    public void setStprba(String stprba) {
        this.stprba = stprba;
    }

    public String getCityba() {
        return cityba;
    }

    public Sub cityba(String cityba) {
        this.cityba = cityba;
        return this;
    }

    public void setCityba(String cityba) {
        this.cityba = cityba;
    }

    public String getZipba() {
        return zipba;
    }

    public Sub zipba(String zipba) {
        this.zipba = zipba;
        return this;
    }

    public void setZipba(String zipba) {
        this.zipba = zipba;
    }

    public String getBas1() {
        return bas1;
    }

    public Sub bas1(String bas1) {
        this.bas1 = bas1;
        return this;
    }

    public void setBas1(String bas1) {
        this.bas1 = bas1;
    }

    public String getBas2() {
        return bas2;
    }

    public Sub bas2(String bas2) {
        this.bas2 = bas2;
        return this;
    }

    public void setBas2(String bas2) {
        this.bas2 = bas2;
    }

    public String getBaph() {
        return baph;
    }

    public Sub baph(String baph) {
        this.baph = baph;
        return this;
    }

    public void setBaph(String baph) {
        this.baph = baph;
    }

    public String getCountryma() {
        return countryma;
    }

    public Sub countryma(String countryma) {
        this.countryma = countryma;
        return this;
    }

    public void setCountryma(String countryma) {
        this.countryma = countryma;
    }

    public String getStprma() {
        return stprma;
    }

    public Sub stprma(String stprma) {
        this.stprma = stprma;
        return this;
    }

    public void setStprma(String stprma) {
        this.stprma = stprma;
    }

    public String getCityma() {
        return cityma;
    }

    public Sub cityma(String cityma) {
        this.cityma = cityma;
        return this;
    }

    public void setCityma(String cityma) {
        this.cityma = cityma;
    }

    public String getZipma() {
        return zipma;
    }

    public Sub zipma(String zipma) {
        this.zipma = zipma;
        return this;
    }

    public void setZipma(String zipma) {
        this.zipma = zipma;
    }

    public String getMas1() {
        return mas1;
    }

    public Sub mas1(String mas1) {
        this.mas1 = mas1;
        return this;
    }

    public void setMas1(String mas1) {
        this.mas1 = mas1;
    }

    public String getMas2() {
        return mas2;
    }

    public Sub mas2(String mas2) {
        this.mas2 = mas2;
        return this;
    }

    public void setMas2(String mas2) {
        this.mas2 = mas2;
    }

    public String getCountryinc() {
        return countryinc;
    }

    public Sub countryinc(String countryinc) {
        this.countryinc = countryinc;
        return this;
    }

    public void setCountryinc(String countryinc) {
        this.countryinc = countryinc;
    }

    public String getStprinc() {
        return stprinc;
    }

    public Sub stprinc(String stprinc) {
        this.stprinc = stprinc;
        return this;
    }

    public void setStprinc(String stprinc) {
        this.stprinc = stprinc;
    }

    public Long getEin() {
        return ein;
    }

    public Sub ein(Long ein) {
        this.ein = ein;
        return this;
    }

    public void setEin(Long ein) {
        this.ein = ein;
    }

    public String getFormer() {
        return former;
    }

    public Sub former(String former) {
        this.former = former;
        return this;
    }

    public void setFormer(String former) {
        this.former = former;
    }

    public String getChanged() {
        return changed;
    }

    public Sub changed(String changed) {
        this.changed = changed;
        return this;
    }

    public void setChanged(String changed) {
        this.changed = changed;
    }

    public String getAfs() {
        return afs;
    }

    public Sub afs(String afs) {
        this.afs = afs;
        return this;
    }

    public void setAfs(String afs) {
        this.afs = afs;
    }

    public Boolean isWksi() {
        return wksi;
    }

    public Sub wksi(Boolean wksi) {
        this.wksi = wksi;
        return this;
    }

    public void setWksi(Boolean wksi) {
        this.wksi = wksi;
    }

    public String getFye() {
        return fye;
    }

    public Sub fye(String fye) {
        this.fye = fye;
        return this;
    }

    public void setFye(String fye) {
        this.fye = fye;
    }

    public String getForm() {
        return form;
    }

    public Sub form(String form) {
        this.form = form;
        return this;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public Instant getPeriod() {
        return period;
    }

    public Sub period(Instant period) {
        this.period = period;
        return this;
    }

    public void setPeriod(Instant period) {
        this.period = period;
    }

    public Integer getFy() {
        return fy;
    }

    public Sub fy(Integer fy) {
        this.fy = fy;
        return this;
    }

    public void setFy(Integer fy) {
        this.fy = fy;
    }

    public String getFp() {
        return fp;
    }

    public Sub fp(String fp) {
        this.fp = fp;
        return this;
    }

    public void setFp(String fp) {
        this.fp = fp;
    }

    public Instant getFiled() {
        return filed;
    }

    public Sub filed(Instant filed) {
        this.filed = filed;
        return this;
    }

    public void setFiled(Instant filed) {
        this.filed = filed;
    }

    public Instant getAccepted() {
        return accepted;
    }

    public Sub accepted(Instant accepted) {
        this.accepted = accepted;
        return this;
    }

    public void setAccepted(Instant accepted) {
        this.accepted = accepted;
    }

    public Boolean isPrevrpt() {
        return prevrpt;
    }

    public Sub prevrpt(Boolean prevrpt) {
        this.prevrpt = prevrpt;
        return this;
    }

    public void setPrevrpt(Boolean prevrpt) {
        this.prevrpt = prevrpt;
    }

    public Boolean isDetail() {
        return detail;
    }

    public Sub detail(Boolean detail) {
        this.detail = detail;
        return this;
    }

    public void setDetail(Boolean detail) {
        this.detail = detail;
    }

    public String getInstance() {
        return instance;
    }

    public Sub instance(String instance) {
        this.instance = instance;
        return this;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public Integer getNciks() {
        return nciks;
    }

    public Sub nciks(Integer nciks) {
        this.nciks = nciks;
        return this;
    }

    public void setNciks(Integer nciks) {
        this.nciks = nciks;
    }

    public String getAciks() {
        return aciks;
    }

    public Sub aciks(String aciks) {
        this.aciks = aciks;
        return this;
    }

    public void setAciks(String aciks) {
        this.aciks = aciks;
    }

    public String getYearAndQuarter() {
        return yearAndQuarter;
    }

    public Sub yearAndQuarter(String yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
        return this;
    }

    public void setYearAndQuarter(String yearAndQuarter) {
        this.yearAndQuarter = yearAndQuarter;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sub)) {
            return false;
        }
        return id != null && id.equals(((Sub) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Sub{" +
            "id=" + getId() +
            ", adsh='" + getAdsh() + "'" +
            ", cik=" + getCik() +
            ", name='" + getName() + "'" +
            ", sic=" + getSic() +
            ", countryba='" + getCountryba() + "'" +
            ", stprba='" + getStprba() + "'" +
            ", cityba='" + getCityba() + "'" +
            ", zipba='" + getZipba() + "'" +
            ", bas1='" + getBas1() + "'" +
            ", bas2='" + getBas2() + "'" +
            ", baph='" + getBaph() + "'" +
            ", countryma='" + getCountryma() + "'" +
            ", stprma='" + getStprma() + "'" +
            ", cityma='" + getCityma() + "'" +
            ", zipma='" + getZipma() + "'" +
            ", mas1='" + getMas1() + "'" +
            ", mas2='" + getMas2() + "'" +
            ", countryinc='" + getCountryinc() + "'" +
            ", stprinc='" + getStprinc() + "'" +
            ", ein=" + getEin() +
            ", former='" + getFormer() + "'" +
            ", changed='" + getChanged() + "'" +
            ", afs='" + getAfs() + "'" +
            ", wksi='" + isWksi() + "'" +
            ", fye='" + getFye() + "'" +
            ", form='" + getForm() + "'" +
            ", period='" + getPeriod() + "'" +
            ", fy=" + getFy() +
            ", fp='" + getFp() + "'" +
            ", filed='" + getFiled() + "'" +
            ", accepted='" + getAccepted() + "'" +
            ", prevrpt='" + isPrevrpt() + "'" +
            ", detail='" + isDetail() + "'" +
            ", instance='" + getInstance() + "'" +
            ", nciks=" + getNciks() +
            ", aciks='" + getAciks() + "'" +
            ", yearAndQuarter='" + getYearAndQuarter() + "'" +
            "}";
    }
}
