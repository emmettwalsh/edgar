package com.emmett.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Pre.
 */
@Entity
@Table(name = "pre")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Pre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Max(value = 999999)
    @Column(name = "report")
    private Integer report;

    @Max(value = 999999)
    @Column(name = "line")
    private Integer line;

    @NotNull
    @Size(max = 2)
    @Column(name = "statement", length = 2, nullable = false)
    private String statement;

    @NotNull
    @Column(name = "inpth", nullable = false)
    private Boolean inpth;

    @NotNull
    @Size(max = 1)
    @Column(name = "rfile", length = 1, nullable = false)
    private String rfile;

    @NotNull
    @Size(max = 512)
    @Column(name = "plabel", length = 512, nullable = false)
    private String plabel;

    @ManyToOne
    @JsonIgnoreProperties("pres")
    private Sub adsh;

    @ManyToOne
    @JsonIgnoreProperties("pres")
    private Tag tag;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getReport() {
        return report;
    }

    public Pre report(Integer report) {
        this.report = report;
        return this;
    }

    public void setReport(Integer report) {
        this.report = report;
    }

    public Integer getLine() {
        return line;
    }

    public Pre line(Integer line) {
        this.line = line;
        return this;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public String getStatement() {
        return statement;
    }

    public Pre statement(String statement) {
        this.statement = statement;
        return this;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public Boolean isInpth() {
        return inpth;
    }

    public Pre inpth(Boolean inpth) {
        this.inpth = inpth;
        return this;
    }

    public void setInpth(Boolean inpth) {
        this.inpth = inpth;
    }

    public String getRfile() {
        return rfile;
    }

    public Pre rfile(String rfile) {
        this.rfile = rfile;
        return this;
    }

    public void setRfile(String rfile) {
        this.rfile = rfile;
    }

    public String getPlabel() {
        return plabel;
    }

    public Pre plabel(String plabel) {
        this.plabel = plabel;
        return this;
    }

    public void setPlabel(String plabel) {
        this.plabel = plabel;
    }

    public Sub getAdsh() {
        return adsh;
    }

    public Pre adsh(Sub sub) {
        this.adsh = sub;
        return this;
    }

    public void setAdsh(Sub sub) {
        this.adsh = sub;
    }

    public Tag getTag() {
        return tag;
    }

    public Pre tag(Tag tag) {
        this.tag = tag;
        return this;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Pre)) {
            return false;
        }
        return id != null && id.equals(((Pre) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Pre{" +
            "id=" + getId() +
            ", report=" + getReport() +
            ", line=" + getLine() +
            ", statement='" + getStatement() + "'" +
            ", inpth='" + isInpth() + "'" +
            ", rfile='" + getRfile() + "'" +
            ", plabel='" + getPlabel() + "'" +
            "}";
    }
}
