package com.emmett.service;

import com.emmett.EdgarApp;
import com.emmett.repository.NumRepository;
import com.emmett.repository.SubRepository;
import com.emmett.repository.TagRepository;
import com.emmett.service.edgar.Processor;
import io.github.jhipster.config.JHipsterProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Integration tests for {@link com.emmett.service.edgar.Processor}.
 */
@SpringBootTest(classes = EdgarApp.class)
public class ProcessorIT {


    @Autowired
    private JHipsterProperties jHipsterProperties;

    @Autowired
    SubRepository subRepository;

    @Autowired
    TagRepository tagRepository;

    @Autowired
    NumRepository numRepository;

    @Autowired
    Processor processor;

    @BeforeEach
    public void setup() {


    }

    @Test
    public void testProcessEdgarDataDir() throws Exception {

        assertThat(subRepository.count()).isEqualTo(0);
        assertThat(tagRepository.count()).isEqualTo(0);
        assertThat(numRepository.count()).isEqualTo(0);

        processor.processFiles();

        assertThat(subRepository.count()).isGreaterThan(0);
        assertThat(tagRepository.count()).isGreaterThan(0);
        assertThat(numRepository.count()).isGreaterThan(0);

    }
}
