package com.emmett.web.rest;

import com.emmett.EdgarApp;
import com.emmett.domain.Sub;
import com.emmett.repository.SubRepository;
import com.emmett.service.SubService;
import com.emmett.service.dto.SubDTO;
import com.emmett.service.mapper.SubMapper;
import com.emmett.web.rest.errors.ExceptionTranslator;
import com.emmett.service.dto.SubCriteria;
import com.emmett.service.SubQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.emmett.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link SubResource} REST controller.
 */
@SpringBootTest(classes = EdgarApp.class)
public class SubResourceIT {

    private static final String DEFAULT_ADSH = "AAAAAAAAAA";
    private static final String UPDATED_ADSH = "BBBBBBBBBB";

    private static final Long DEFAULT_CIK = 9999999999L;
    private static final Long UPDATED_CIK = 9999999998L;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_SIC = 9999;
    private static final Integer UPDATED_SIC = 9998;

    private static final String DEFAULT_COUNTRYBA = "AA";
    private static final String UPDATED_COUNTRYBA = "BB";

    private static final String DEFAULT_STPRBA = "AA";
    private static final String UPDATED_STPRBA = "BB";

    private static final String DEFAULT_CITYBA = "AAAAAAAAAA";
    private static final String UPDATED_CITYBA = "BBBBBBBBBB";

    private static final String DEFAULT_ZIPBA = "AAAAAAAAAA";
    private static final String UPDATED_ZIPBA = "BBBBBBBBBB";

    private static final String DEFAULT_BAS_1 = "AAAAAAAAAA";
    private static final String UPDATED_BAS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_BAS_2 = "AAAAAAAAAA";
    private static final String UPDATED_BAS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_BAPH = "AAAAAAAAAA";
    private static final String UPDATED_BAPH = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRYMA = "AA";
    private static final String UPDATED_COUNTRYMA = "BB";

    private static final String DEFAULT_STPRMA = "AA";
    private static final String UPDATED_STPRMA = "BB";

    private static final String DEFAULT_CITYMA = "AAAAAAAAAA";
    private static final String UPDATED_CITYMA = "BBBBBBBBBB";

    private static final String DEFAULT_ZIPMA = "AAAAAAAAAA";
    private static final String UPDATED_ZIPMA = "BBBBBBBBBB";

    private static final String DEFAULT_MAS_1 = "AAAAAAAAAA";
    private static final String UPDATED_MAS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_MAS_2 = "AAAAAAAAAA";
    private static final String UPDATED_MAS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRYINC = "AAA";
    private static final String UPDATED_COUNTRYINC = "BBB";

    private static final String DEFAULT_STPRINC = "AA";
    private static final String UPDATED_STPRINC = "BB";

    private static final Long DEFAULT_EIN = 9999999999L;
    private static final Long UPDATED_EIN = 9999999998L;

    private static final String DEFAULT_FORMER = "AAAAAAAAAA";
    private static final String UPDATED_FORMER = "BBBBBBBBBB";

    private static final String DEFAULT_CHANGED = "AAAAAAAA";
    private static final String UPDATED_CHANGED = "BBBBBBBB";

    private static final String DEFAULT_AFS = "AAAAA";
    private static final String UPDATED_AFS = "BBBBB";

    private static final Boolean DEFAULT_WKSI = false;
    private static final Boolean UPDATED_WKSI = true;

    private static final String DEFAULT_FYE = "AAAA";
    private static final String UPDATED_FYE = "BBBB";

    private static final String DEFAULT_FORM = "AAAAAAAAAA";
    private static final String UPDATED_FORM = "BBBBBBBBBB";

    private static final Instant DEFAULT_PERIOD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PERIOD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_FY = 9999;
    private static final Integer UPDATED_FY = 9998;

    private static final String DEFAULT_FP = "AA";
    private static final String UPDATED_FP = "BB";

    private static final Instant DEFAULT_FILED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FILED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_ACCEPTED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ACCEPTED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_PREVRPT = false;
    private static final Boolean UPDATED_PREVRPT = true;

    private static final Boolean DEFAULT_DETAIL = false;
    private static final Boolean UPDATED_DETAIL = true;

    private static final String DEFAULT_INSTANCE = "AAAAAAAAAA";
    private static final String UPDATED_INSTANCE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NCIKS = 9999;
    private static final Integer UPDATED_NCIKS = 9998;

    private static final String DEFAULT_ACIKS = "AAAAAAAAAA";
    private static final String UPDATED_ACIKS = "BBBBBBBBBB";

    private static final String DEFAULT_YEAR_AND_QUARTER = "AAAAAA";
    private static final String UPDATED_YEAR_AND_QUARTER = "BBBBBB";

    @Autowired
    private SubRepository subRepository;

    @Autowired
    private SubMapper subMapper;

    @Autowired
    private SubService subService;

    @Autowired
    private SubQueryService subQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restSubMockMvc;

    private Sub sub;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SubResource subResource = new SubResource(subService, subQueryService);
        this.restSubMockMvc = MockMvcBuilders.standaloneSetup(subResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sub createEntity(EntityManager em) {
        Sub sub = new Sub()
            .adsh(DEFAULT_ADSH)
            .cik(DEFAULT_CIK)
            .name(DEFAULT_NAME)
            .sic(DEFAULT_SIC)
            .countryba(DEFAULT_COUNTRYBA)
            .stprba(DEFAULT_STPRBA)
            .cityba(DEFAULT_CITYBA)
            .zipba(DEFAULT_ZIPBA)
            .bas1(DEFAULT_BAS_1)
            .bas2(DEFAULT_BAS_2)
            .baph(DEFAULT_BAPH)
            .countryma(DEFAULT_COUNTRYMA)
            .stprma(DEFAULT_STPRMA)
            .cityma(DEFAULT_CITYMA)
            .zipma(DEFAULT_ZIPMA)
            .mas1(DEFAULT_MAS_1)
            .mas2(DEFAULT_MAS_2)
            .countryinc(DEFAULT_COUNTRYINC)
            .stprinc(DEFAULT_STPRINC)
            .ein(DEFAULT_EIN)
            .former(DEFAULT_FORMER)
            .changed(DEFAULT_CHANGED)
            .afs(DEFAULT_AFS)
            .wksi(DEFAULT_WKSI)
            .fye(DEFAULT_FYE)
            .form(DEFAULT_FORM)
            .period(DEFAULT_PERIOD)
            .fy(DEFAULT_FY)
            .fp(DEFAULT_FP)
            .filed(DEFAULT_FILED)
            .accepted(DEFAULT_ACCEPTED)
            .prevrpt(DEFAULT_PREVRPT)
            .detail(DEFAULT_DETAIL)
            .instance(DEFAULT_INSTANCE)
            .nciks(DEFAULT_NCIKS)
            .aciks(DEFAULT_ACIKS)
            .yearAndQuarter(DEFAULT_YEAR_AND_QUARTER);
        return sub;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sub createUpdatedEntity(EntityManager em) {
        Sub sub = new Sub()
            .adsh(UPDATED_ADSH)
            .cik(UPDATED_CIK)
            .name(UPDATED_NAME)
            .sic(UPDATED_SIC)
            .countryba(UPDATED_COUNTRYBA)
            .stprba(UPDATED_STPRBA)
            .cityba(UPDATED_CITYBA)
            .zipba(UPDATED_ZIPBA)
            .bas1(UPDATED_BAS_1)
            .bas2(UPDATED_BAS_2)
            .baph(UPDATED_BAPH)
            .countryma(UPDATED_COUNTRYMA)
            .stprma(UPDATED_STPRMA)
            .cityma(UPDATED_CITYMA)
            .zipma(UPDATED_ZIPMA)
            .mas1(UPDATED_MAS_1)
            .mas2(UPDATED_MAS_2)
            .countryinc(UPDATED_COUNTRYINC)
            .stprinc(UPDATED_STPRINC)
            .ein(UPDATED_EIN)
            .former(UPDATED_FORMER)
            .changed(UPDATED_CHANGED)
            .afs(UPDATED_AFS)
            .wksi(UPDATED_WKSI)
            .fye(UPDATED_FYE)
            .form(UPDATED_FORM)
            .period(UPDATED_PERIOD)
            .fy(UPDATED_FY)
            .fp(UPDATED_FP)
            .filed(UPDATED_FILED)
            .accepted(UPDATED_ACCEPTED)
            .prevrpt(UPDATED_PREVRPT)
            .detail(UPDATED_DETAIL)
            .instance(UPDATED_INSTANCE)
            .nciks(UPDATED_NCIKS)
            .aciks(UPDATED_ACIKS)
            .yearAndQuarter(UPDATED_YEAR_AND_QUARTER);
        return sub;
    }

    @BeforeEach
    public void initTest() {
        sub = createEntity(em);
    }

    @Test
    @Transactional
    public void createSub() throws Exception {
        int databaseSizeBeforeCreate = subRepository.findAll().size();

        // Create the Sub
        SubDTO subDTO = subMapper.toDto(sub);
        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isCreated());

        // Validate the Sub in the database
        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeCreate + 1);
        Sub testSub = subList.get(subList.size() - 1);
        assertThat(testSub.getAdsh()).isEqualTo(DEFAULT_ADSH);
        assertThat(testSub.getCik()).isEqualTo(DEFAULT_CIK);
        assertThat(testSub.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testSub.getSic()).isEqualTo(DEFAULT_SIC);
        assertThat(testSub.getCountryba()).isEqualTo(DEFAULT_COUNTRYBA);
        assertThat(testSub.getStprba()).isEqualTo(DEFAULT_STPRBA);
        assertThat(testSub.getCityba()).isEqualTo(DEFAULT_CITYBA);
        assertThat(testSub.getZipba()).isEqualTo(DEFAULT_ZIPBA);
        assertThat(testSub.getBas1()).isEqualTo(DEFAULT_BAS_1);
        assertThat(testSub.getBas2()).isEqualTo(DEFAULT_BAS_2);
        assertThat(testSub.getBaph()).isEqualTo(DEFAULT_BAPH);
        assertThat(testSub.getCountryma()).isEqualTo(DEFAULT_COUNTRYMA);
        assertThat(testSub.getStprma()).isEqualTo(DEFAULT_STPRMA);
        assertThat(testSub.getCityma()).isEqualTo(DEFAULT_CITYMA);
        assertThat(testSub.getZipma()).isEqualTo(DEFAULT_ZIPMA);
        assertThat(testSub.getMas1()).isEqualTo(DEFAULT_MAS_1);
        assertThat(testSub.getMas2()).isEqualTo(DEFAULT_MAS_2);
        assertThat(testSub.getCountryinc()).isEqualTo(DEFAULT_COUNTRYINC);
        assertThat(testSub.getStprinc()).isEqualTo(DEFAULT_STPRINC);
        assertThat(testSub.getEin()).isEqualTo(DEFAULT_EIN);
        assertThat(testSub.getFormer()).isEqualTo(DEFAULT_FORMER);
        assertThat(testSub.getChanged()).isEqualTo(DEFAULT_CHANGED);
        assertThat(testSub.getAfs()).isEqualTo(DEFAULT_AFS);
        assertThat(testSub.isWksi()).isEqualTo(DEFAULT_WKSI);
        assertThat(testSub.getFye()).isEqualTo(DEFAULT_FYE);
        assertThat(testSub.getForm()).isEqualTo(DEFAULT_FORM);
        assertThat(testSub.getPeriod()).isEqualTo(DEFAULT_PERIOD);
        assertThat(testSub.getFy()).isEqualTo(DEFAULT_FY);
        assertThat(testSub.getFp()).isEqualTo(DEFAULT_FP);
        assertThat(testSub.getFiled()).isEqualTo(DEFAULT_FILED);
        assertThat(testSub.getAccepted()).isEqualTo(DEFAULT_ACCEPTED);
        assertThat(testSub.isPrevrpt()).isEqualTo(DEFAULT_PREVRPT);
        assertThat(testSub.isDetail()).isEqualTo(DEFAULT_DETAIL);
        assertThat(testSub.getInstance()).isEqualTo(DEFAULT_INSTANCE);
        assertThat(testSub.getNciks()).isEqualTo(DEFAULT_NCIKS);
        assertThat(testSub.getAciks()).isEqualTo(DEFAULT_ACIKS);
        assertThat(testSub.getYearAndQuarter()).isEqualTo(DEFAULT_YEAR_AND_QUARTER);
    }

    @Test
    @Transactional
    public void createSubWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subRepository.findAll().size();

        // Create the Sub with an existing ID
        sub.setId(1L);
        SubDTO subDTO = subMapper.toDto(sub);

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sub in the database
        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkAdshIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setAdsh(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCikIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setCik(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setName(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountrybaIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setCountryba(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCitybaIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setCityba(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCountryincIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setCountryinc(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkEinIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setEin(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkWksiIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setWksi(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFyeIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setFye(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFormIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setForm(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPeriodIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setPeriod(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFyIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setFy(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFpIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setFp(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFiledIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setFiled(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAcceptedIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setAccepted(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrevrptIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setPrevrpt(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkDetailIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setDetail(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInstanceIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setInstance(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkNciksIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setNciks(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkYearAndQuarterIsRequired() throws Exception {
        int databaseSizeBeforeTest = subRepository.findAll().size();
        // set the field null
        sub.setYearAndQuarter(null);

        // Create the Sub, which fails.
        SubDTO subDTO = subMapper.toDto(sub);

        restSubMockMvc.perform(post("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllSubs() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList
        restSubMockMvc.perform(get("/api/subs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sub.getId().intValue())))
            .andExpect(jsonPath("$.[*].adsh").value(hasItem(DEFAULT_ADSH.toString())))
            .andExpect(jsonPath("$.[*].cik").value(hasItem(DEFAULT_CIK.intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].sic").value(hasItem(DEFAULT_SIC)))
            .andExpect(jsonPath("$.[*].countryba").value(hasItem(DEFAULT_COUNTRYBA.toString())))
            .andExpect(jsonPath("$.[*].stprba").value(hasItem(DEFAULT_STPRBA.toString())))
            .andExpect(jsonPath("$.[*].cityba").value(hasItem(DEFAULT_CITYBA.toString())))
            .andExpect(jsonPath("$.[*].zipba").value(hasItem(DEFAULT_ZIPBA.toString())))
            .andExpect(jsonPath("$.[*].bas1").value(hasItem(DEFAULT_BAS_1.toString())))
            .andExpect(jsonPath("$.[*].bas2").value(hasItem(DEFAULT_BAS_2.toString())))
            .andExpect(jsonPath("$.[*].baph").value(hasItem(DEFAULT_BAPH.toString())))
            .andExpect(jsonPath("$.[*].countryma").value(hasItem(DEFAULT_COUNTRYMA.toString())))
            .andExpect(jsonPath("$.[*].stprma").value(hasItem(DEFAULT_STPRMA.toString())))
            .andExpect(jsonPath("$.[*].cityma").value(hasItem(DEFAULT_CITYMA.toString())))
            .andExpect(jsonPath("$.[*].zipma").value(hasItem(DEFAULT_ZIPMA.toString())))
            .andExpect(jsonPath("$.[*].mas1").value(hasItem(DEFAULT_MAS_1.toString())))
            .andExpect(jsonPath("$.[*].mas2").value(hasItem(DEFAULT_MAS_2.toString())))
            .andExpect(jsonPath("$.[*].countryinc").value(hasItem(DEFAULT_COUNTRYINC.toString())))
            .andExpect(jsonPath("$.[*].stprinc").value(hasItem(DEFAULT_STPRINC.toString())))
            .andExpect(jsonPath("$.[*].ein").value(hasItem(DEFAULT_EIN.intValue())))
            .andExpect(jsonPath("$.[*].former").value(hasItem(DEFAULT_FORMER.toString())))
            .andExpect(jsonPath("$.[*].changed").value(hasItem(DEFAULT_CHANGED.toString())))
            .andExpect(jsonPath("$.[*].afs").value(hasItem(DEFAULT_AFS.toString())))
            .andExpect(jsonPath("$.[*].wksi").value(hasItem(DEFAULT_WKSI.booleanValue())))
            .andExpect(jsonPath("$.[*].fye").value(hasItem(DEFAULT_FYE.toString())))
            .andExpect(jsonPath("$.[*].form").value(hasItem(DEFAULT_FORM.toString())))
            .andExpect(jsonPath("$.[*].period").value(hasItem(DEFAULT_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].fy").value(hasItem(DEFAULT_FY)))
            .andExpect(jsonPath("$.[*].fp").value(hasItem(DEFAULT_FP.toString())))
            .andExpect(jsonPath("$.[*].filed").value(hasItem(DEFAULT_FILED.toString())))
            .andExpect(jsonPath("$.[*].accepted").value(hasItem(DEFAULT_ACCEPTED.toString())))
            .andExpect(jsonPath("$.[*].prevrpt").value(hasItem(DEFAULT_PREVRPT.booleanValue())))
            .andExpect(jsonPath("$.[*].detail").value(hasItem(DEFAULT_DETAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].instance").value(hasItem(DEFAULT_INSTANCE.toString())))
            .andExpect(jsonPath("$.[*].nciks").value(hasItem(DEFAULT_NCIKS)))
            .andExpect(jsonPath("$.[*].aciks").value(hasItem(DEFAULT_ACIKS.toString())))
            .andExpect(jsonPath("$.[*].yearAndQuarter").value(hasItem(DEFAULT_YEAR_AND_QUARTER.toString())));
    }
    
    @Test
    @Transactional
    public void getSub() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get the sub
        restSubMockMvc.perform(get("/api/subs/{id}", sub.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sub.getId().intValue()))
            .andExpect(jsonPath("$.adsh").value(DEFAULT_ADSH.toString()))
            .andExpect(jsonPath("$.cik").value(DEFAULT_CIK.intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.sic").value(DEFAULT_SIC))
            .andExpect(jsonPath("$.countryba").value(DEFAULT_COUNTRYBA.toString()))
            .andExpect(jsonPath("$.stprba").value(DEFAULT_STPRBA.toString()))
            .andExpect(jsonPath("$.cityba").value(DEFAULT_CITYBA.toString()))
            .andExpect(jsonPath("$.zipba").value(DEFAULT_ZIPBA.toString()))
            .andExpect(jsonPath("$.bas1").value(DEFAULT_BAS_1.toString()))
            .andExpect(jsonPath("$.bas2").value(DEFAULT_BAS_2.toString()))
            .andExpect(jsonPath("$.baph").value(DEFAULT_BAPH.toString()))
            .andExpect(jsonPath("$.countryma").value(DEFAULT_COUNTRYMA.toString()))
            .andExpect(jsonPath("$.stprma").value(DEFAULT_STPRMA.toString()))
            .andExpect(jsonPath("$.cityma").value(DEFAULT_CITYMA.toString()))
            .andExpect(jsonPath("$.zipma").value(DEFAULT_ZIPMA.toString()))
            .andExpect(jsonPath("$.mas1").value(DEFAULT_MAS_1.toString()))
            .andExpect(jsonPath("$.mas2").value(DEFAULT_MAS_2.toString()))
            .andExpect(jsonPath("$.countryinc").value(DEFAULT_COUNTRYINC.toString()))
            .andExpect(jsonPath("$.stprinc").value(DEFAULT_STPRINC.toString()))
            .andExpect(jsonPath("$.ein").value(DEFAULT_EIN.intValue()))
            .andExpect(jsonPath("$.former").value(DEFAULT_FORMER.toString()))
            .andExpect(jsonPath("$.changed").value(DEFAULT_CHANGED.toString()))
            .andExpect(jsonPath("$.afs").value(DEFAULT_AFS.toString()))
            .andExpect(jsonPath("$.wksi").value(DEFAULT_WKSI.booleanValue()))
            .andExpect(jsonPath("$.fye").value(DEFAULT_FYE.toString()))
            .andExpect(jsonPath("$.form").value(DEFAULT_FORM.toString()))
            .andExpect(jsonPath("$.period").value(DEFAULT_PERIOD.toString()))
            .andExpect(jsonPath("$.fy").value(DEFAULT_FY))
            .andExpect(jsonPath("$.fp").value(DEFAULT_FP.toString()))
            .andExpect(jsonPath("$.filed").value(DEFAULT_FILED.toString()))
            .andExpect(jsonPath("$.accepted").value(DEFAULT_ACCEPTED.toString()))
            .andExpect(jsonPath("$.prevrpt").value(DEFAULT_PREVRPT.booleanValue()))
            .andExpect(jsonPath("$.detail").value(DEFAULT_DETAIL.booleanValue()))
            .andExpect(jsonPath("$.instance").value(DEFAULT_INSTANCE.toString()))
            .andExpect(jsonPath("$.nciks").value(DEFAULT_NCIKS))
            .andExpect(jsonPath("$.aciks").value(DEFAULT_ACIKS.toString()))
            .andExpect(jsonPath("$.yearAndQuarter").value(DEFAULT_YEAR_AND_QUARTER.toString()));
    }

    @Test
    @Transactional
    public void getAllSubsByAdshIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where adsh equals to DEFAULT_ADSH
        defaultSubShouldBeFound("adsh.equals=" + DEFAULT_ADSH);

        // Get all the subList where adsh equals to UPDATED_ADSH
        defaultSubShouldNotBeFound("adsh.equals=" + UPDATED_ADSH);
    }

    @Test
    @Transactional
    public void getAllSubsByAdshIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where adsh in DEFAULT_ADSH or UPDATED_ADSH
        defaultSubShouldBeFound("adsh.in=" + DEFAULT_ADSH + "," + UPDATED_ADSH);

        // Get all the subList where adsh equals to UPDATED_ADSH
        defaultSubShouldNotBeFound("adsh.in=" + UPDATED_ADSH);
    }

    @Test
    @Transactional
    public void getAllSubsByAdshIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where adsh is not null
        defaultSubShouldBeFound("adsh.specified=true");

        // Get all the subList where adsh is null
        defaultSubShouldNotBeFound("adsh.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByCikIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cik equals to DEFAULT_CIK
        defaultSubShouldBeFound("cik.equals=" + DEFAULT_CIK);

        // Get all the subList where cik equals to UPDATED_CIK
        defaultSubShouldNotBeFound("cik.equals=" + UPDATED_CIK);
    }

    @Test
    @Transactional
    public void getAllSubsByCikIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cik in DEFAULT_CIK or UPDATED_CIK
        defaultSubShouldBeFound("cik.in=" + DEFAULT_CIK + "," + UPDATED_CIK);

        // Get all the subList where cik equals to UPDATED_CIK
        defaultSubShouldNotBeFound("cik.in=" + UPDATED_CIK);
    }

    @Test
    @Transactional
    public void getAllSubsByCikIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cik is not null
        defaultSubShouldBeFound("cik.specified=true");

        // Get all the subList where cik is null
        defaultSubShouldNotBeFound("cik.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByCikIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cik greater than or equals to DEFAULT_CIK
        defaultSubShouldBeFound("cik.greaterOrEqualThan=" + DEFAULT_CIK);

        // Get all the subList where cik greater than or equals to (DEFAULT_CIK + 1)
        defaultSubShouldNotBeFound("cik.greaterOrEqualThan=" + (DEFAULT_CIK + 1));
    }

    @Test
    @Transactional
    public void getAllSubsByCikIsLessThanSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cik less than or equals to DEFAULT_CIK
        defaultSubShouldNotBeFound("cik.lessThan=" + DEFAULT_CIK);

        // Get all the subList where cik less than or equals to (DEFAULT_CIK + 1)
        defaultSubShouldBeFound("cik.lessThan=" + (DEFAULT_CIK + 1));
    }


    @Test
    @Transactional
    public void getAllSubsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where name equals to DEFAULT_NAME
        defaultSubShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the subList where name equals to UPDATED_NAME
        defaultSubShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSubsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where name in DEFAULT_NAME or UPDATED_NAME
        defaultSubShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the subList where name equals to UPDATED_NAME
        defaultSubShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllSubsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where name is not null
        defaultSubShouldBeFound("name.specified=true");

        // Get all the subList where name is null
        defaultSubShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsBySicIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where sic equals to DEFAULT_SIC
        defaultSubShouldBeFound("sic.equals=" + DEFAULT_SIC);

        // Get all the subList where sic equals to UPDATED_SIC
        defaultSubShouldNotBeFound("sic.equals=" + UPDATED_SIC);
    }

    @Test
    @Transactional
    public void getAllSubsBySicIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where sic in DEFAULT_SIC or UPDATED_SIC
        defaultSubShouldBeFound("sic.in=" + DEFAULT_SIC + "," + UPDATED_SIC);

        // Get all the subList where sic equals to UPDATED_SIC
        defaultSubShouldNotBeFound("sic.in=" + UPDATED_SIC);
    }

    @Test
    @Transactional
    public void getAllSubsBySicIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where sic is not null
        defaultSubShouldBeFound("sic.specified=true");

        // Get all the subList where sic is null
        defaultSubShouldNotBeFound("sic.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsBySicIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where sic greater than or equals to DEFAULT_SIC
        defaultSubShouldBeFound("sic.greaterOrEqualThan=" + DEFAULT_SIC);

        // Get all the subList where sic greater than or equals to (DEFAULT_SIC + 1)
        defaultSubShouldNotBeFound("sic.greaterOrEqualThan=" + (DEFAULT_SIC + 1));
    }

    @Test
    @Transactional
    public void getAllSubsBySicIsLessThanSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where sic less than or equals to DEFAULT_SIC
        defaultSubShouldNotBeFound("sic.lessThan=" + DEFAULT_SIC);

        // Get all the subList where sic less than or equals to (DEFAULT_SIC + 1)
        defaultSubShouldBeFound("sic.lessThan=" + (DEFAULT_SIC + 1));
    }


    @Test
    @Transactional
    public void getAllSubsByCountrybaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryba equals to DEFAULT_COUNTRYBA
        defaultSubShouldBeFound("countryba.equals=" + DEFAULT_COUNTRYBA);

        // Get all the subList where countryba equals to UPDATED_COUNTRYBA
        defaultSubShouldNotBeFound("countryba.equals=" + UPDATED_COUNTRYBA);
    }

    @Test
    @Transactional
    public void getAllSubsByCountrybaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryba in DEFAULT_COUNTRYBA or UPDATED_COUNTRYBA
        defaultSubShouldBeFound("countryba.in=" + DEFAULT_COUNTRYBA + "," + UPDATED_COUNTRYBA);

        // Get all the subList where countryba equals to UPDATED_COUNTRYBA
        defaultSubShouldNotBeFound("countryba.in=" + UPDATED_COUNTRYBA);
    }

    @Test
    @Transactional
    public void getAllSubsByCountrybaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryba is not null
        defaultSubShouldBeFound("countryba.specified=true");

        // Get all the subList where countryba is null
        defaultSubShouldNotBeFound("countryba.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByStprbaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprba equals to DEFAULT_STPRBA
        defaultSubShouldBeFound("stprba.equals=" + DEFAULT_STPRBA);

        // Get all the subList where stprba equals to UPDATED_STPRBA
        defaultSubShouldNotBeFound("stprba.equals=" + UPDATED_STPRBA);
    }

    @Test
    @Transactional
    public void getAllSubsByStprbaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprba in DEFAULT_STPRBA or UPDATED_STPRBA
        defaultSubShouldBeFound("stprba.in=" + DEFAULT_STPRBA + "," + UPDATED_STPRBA);

        // Get all the subList where stprba equals to UPDATED_STPRBA
        defaultSubShouldNotBeFound("stprba.in=" + UPDATED_STPRBA);
    }

    @Test
    @Transactional
    public void getAllSubsByStprbaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprba is not null
        defaultSubShouldBeFound("stprba.specified=true");

        // Get all the subList where stprba is null
        defaultSubShouldNotBeFound("stprba.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByCitybaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cityba equals to DEFAULT_CITYBA
        defaultSubShouldBeFound("cityba.equals=" + DEFAULT_CITYBA);

        // Get all the subList where cityba equals to UPDATED_CITYBA
        defaultSubShouldNotBeFound("cityba.equals=" + UPDATED_CITYBA);
    }

    @Test
    @Transactional
    public void getAllSubsByCitybaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cityba in DEFAULT_CITYBA or UPDATED_CITYBA
        defaultSubShouldBeFound("cityba.in=" + DEFAULT_CITYBA + "," + UPDATED_CITYBA);

        // Get all the subList where cityba equals to UPDATED_CITYBA
        defaultSubShouldNotBeFound("cityba.in=" + UPDATED_CITYBA);
    }

    @Test
    @Transactional
    public void getAllSubsByCitybaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cityba is not null
        defaultSubShouldBeFound("cityba.specified=true");

        // Get all the subList where cityba is null
        defaultSubShouldNotBeFound("cityba.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByZipbaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where zipba equals to DEFAULT_ZIPBA
        defaultSubShouldBeFound("zipba.equals=" + DEFAULT_ZIPBA);

        // Get all the subList where zipba equals to UPDATED_ZIPBA
        defaultSubShouldNotBeFound("zipba.equals=" + UPDATED_ZIPBA);
    }

    @Test
    @Transactional
    public void getAllSubsByZipbaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where zipba in DEFAULT_ZIPBA or UPDATED_ZIPBA
        defaultSubShouldBeFound("zipba.in=" + DEFAULT_ZIPBA + "," + UPDATED_ZIPBA);

        // Get all the subList where zipba equals to UPDATED_ZIPBA
        defaultSubShouldNotBeFound("zipba.in=" + UPDATED_ZIPBA);
    }

    @Test
    @Transactional
    public void getAllSubsByZipbaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where zipba is not null
        defaultSubShouldBeFound("zipba.specified=true");

        // Get all the subList where zipba is null
        defaultSubShouldNotBeFound("zipba.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByBas1IsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where bas1 equals to DEFAULT_BAS_1
        defaultSubShouldBeFound("bas1.equals=" + DEFAULT_BAS_1);

        // Get all the subList where bas1 equals to UPDATED_BAS_1
        defaultSubShouldNotBeFound("bas1.equals=" + UPDATED_BAS_1);
    }

    @Test
    @Transactional
    public void getAllSubsByBas1IsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where bas1 in DEFAULT_BAS_1 or UPDATED_BAS_1
        defaultSubShouldBeFound("bas1.in=" + DEFAULT_BAS_1 + "," + UPDATED_BAS_1);

        // Get all the subList where bas1 equals to UPDATED_BAS_1
        defaultSubShouldNotBeFound("bas1.in=" + UPDATED_BAS_1);
    }

    @Test
    @Transactional
    public void getAllSubsByBas1IsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where bas1 is not null
        defaultSubShouldBeFound("bas1.specified=true");

        // Get all the subList where bas1 is null
        defaultSubShouldNotBeFound("bas1.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByBas2IsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where bas2 equals to DEFAULT_BAS_2
        defaultSubShouldBeFound("bas2.equals=" + DEFAULT_BAS_2);

        // Get all the subList where bas2 equals to UPDATED_BAS_2
        defaultSubShouldNotBeFound("bas2.equals=" + UPDATED_BAS_2);
    }

    @Test
    @Transactional
    public void getAllSubsByBas2IsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where bas2 in DEFAULT_BAS_2 or UPDATED_BAS_2
        defaultSubShouldBeFound("bas2.in=" + DEFAULT_BAS_2 + "," + UPDATED_BAS_2);

        // Get all the subList where bas2 equals to UPDATED_BAS_2
        defaultSubShouldNotBeFound("bas2.in=" + UPDATED_BAS_2);
    }

    @Test
    @Transactional
    public void getAllSubsByBas2IsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where bas2 is not null
        defaultSubShouldBeFound("bas2.specified=true");

        // Get all the subList where bas2 is null
        defaultSubShouldNotBeFound("bas2.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByBaphIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where baph equals to DEFAULT_BAPH
        defaultSubShouldBeFound("baph.equals=" + DEFAULT_BAPH);

        // Get all the subList where baph equals to UPDATED_BAPH
        defaultSubShouldNotBeFound("baph.equals=" + UPDATED_BAPH);
    }

    @Test
    @Transactional
    public void getAllSubsByBaphIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where baph in DEFAULT_BAPH or UPDATED_BAPH
        defaultSubShouldBeFound("baph.in=" + DEFAULT_BAPH + "," + UPDATED_BAPH);

        // Get all the subList where baph equals to UPDATED_BAPH
        defaultSubShouldNotBeFound("baph.in=" + UPDATED_BAPH);
    }

    @Test
    @Transactional
    public void getAllSubsByBaphIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where baph is not null
        defaultSubShouldBeFound("baph.specified=true");

        // Get all the subList where baph is null
        defaultSubShouldNotBeFound("baph.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByCountrymaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryma equals to DEFAULT_COUNTRYMA
        defaultSubShouldBeFound("countryma.equals=" + DEFAULT_COUNTRYMA);

        // Get all the subList where countryma equals to UPDATED_COUNTRYMA
        defaultSubShouldNotBeFound("countryma.equals=" + UPDATED_COUNTRYMA);
    }

    @Test
    @Transactional
    public void getAllSubsByCountrymaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryma in DEFAULT_COUNTRYMA or UPDATED_COUNTRYMA
        defaultSubShouldBeFound("countryma.in=" + DEFAULT_COUNTRYMA + "," + UPDATED_COUNTRYMA);

        // Get all the subList where countryma equals to UPDATED_COUNTRYMA
        defaultSubShouldNotBeFound("countryma.in=" + UPDATED_COUNTRYMA);
    }

    @Test
    @Transactional
    public void getAllSubsByCountrymaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryma is not null
        defaultSubShouldBeFound("countryma.specified=true");

        // Get all the subList where countryma is null
        defaultSubShouldNotBeFound("countryma.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByStprmaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprma equals to DEFAULT_STPRMA
        defaultSubShouldBeFound("stprma.equals=" + DEFAULT_STPRMA);

        // Get all the subList where stprma equals to UPDATED_STPRMA
        defaultSubShouldNotBeFound("stprma.equals=" + UPDATED_STPRMA);
    }

    @Test
    @Transactional
    public void getAllSubsByStprmaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprma in DEFAULT_STPRMA or UPDATED_STPRMA
        defaultSubShouldBeFound("stprma.in=" + DEFAULT_STPRMA + "," + UPDATED_STPRMA);

        // Get all the subList where stprma equals to UPDATED_STPRMA
        defaultSubShouldNotBeFound("stprma.in=" + UPDATED_STPRMA);
    }

    @Test
    @Transactional
    public void getAllSubsByStprmaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprma is not null
        defaultSubShouldBeFound("stprma.specified=true");

        // Get all the subList where stprma is null
        defaultSubShouldNotBeFound("stprma.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByCitymaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cityma equals to DEFAULT_CITYMA
        defaultSubShouldBeFound("cityma.equals=" + DEFAULT_CITYMA);

        // Get all the subList where cityma equals to UPDATED_CITYMA
        defaultSubShouldNotBeFound("cityma.equals=" + UPDATED_CITYMA);
    }

    @Test
    @Transactional
    public void getAllSubsByCitymaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cityma in DEFAULT_CITYMA or UPDATED_CITYMA
        defaultSubShouldBeFound("cityma.in=" + DEFAULT_CITYMA + "," + UPDATED_CITYMA);

        // Get all the subList where cityma equals to UPDATED_CITYMA
        defaultSubShouldNotBeFound("cityma.in=" + UPDATED_CITYMA);
    }

    @Test
    @Transactional
    public void getAllSubsByCitymaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where cityma is not null
        defaultSubShouldBeFound("cityma.specified=true");

        // Get all the subList where cityma is null
        defaultSubShouldNotBeFound("cityma.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByZipmaIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where zipma equals to DEFAULT_ZIPMA
        defaultSubShouldBeFound("zipma.equals=" + DEFAULT_ZIPMA);

        // Get all the subList where zipma equals to UPDATED_ZIPMA
        defaultSubShouldNotBeFound("zipma.equals=" + UPDATED_ZIPMA);
    }

    @Test
    @Transactional
    public void getAllSubsByZipmaIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where zipma in DEFAULT_ZIPMA or UPDATED_ZIPMA
        defaultSubShouldBeFound("zipma.in=" + DEFAULT_ZIPMA + "," + UPDATED_ZIPMA);

        // Get all the subList where zipma equals to UPDATED_ZIPMA
        defaultSubShouldNotBeFound("zipma.in=" + UPDATED_ZIPMA);
    }

    @Test
    @Transactional
    public void getAllSubsByZipmaIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where zipma is not null
        defaultSubShouldBeFound("zipma.specified=true");

        // Get all the subList where zipma is null
        defaultSubShouldNotBeFound("zipma.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByMas1IsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where mas1 equals to DEFAULT_MAS_1
        defaultSubShouldBeFound("mas1.equals=" + DEFAULT_MAS_1);

        // Get all the subList where mas1 equals to UPDATED_MAS_1
        defaultSubShouldNotBeFound("mas1.equals=" + UPDATED_MAS_1);
    }

    @Test
    @Transactional
    public void getAllSubsByMas1IsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where mas1 in DEFAULT_MAS_1 or UPDATED_MAS_1
        defaultSubShouldBeFound("mas1.in=" + DEFAULT_MAS_1 + "," + UPDATED_MAS_1);

        // Get all the subList where mas1 equals to UPDATED_MAS_1
        defaultSubShouldNotBeFound("mas1.in=" + UPDATED_MAS_1);
    }

    @Test
    @Transactional
    public void getAllSubsByMas1IsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where mas1 is not null
        defaultSubShouldBeFound("mas1.specified=true");

        // Get all the subList where mas1 is null
        defaultSubShouldNotBeFound("mas1.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByMas2IsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where mas2 equals to DEFAULT_MAS_2
        defaultSubShouldBeFound("mas2.equals=" + DEFAULT_MAS_2);

        // Get all the subList where mas2 equals to UPDATED_MAS_2
        defaultSubShouldNotBeFound("mas2.equals=" + UPDATED_MAS_2);
    }

    @Test
    @Transactional
    public void getAllSubsByMas2IsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where mas2 in DEFAULT_MAS_2 or UPDATED_MAS_2
        defaultSubShouldBeFound("mas2.in=" + DEFAULT_MAS_2 + "," + UPDATED_MAS_2);

        // Get all the subList where mas2 equals to UPDATED_MAS_2
        defaultSubShouldNotBeFound("mas2.in=" + UPDATED_MAS_2);
    }

    @Test
    @Transactional
    public void getAllSubsByMas2IsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where mas2 is not null
        defaultSubShouldBeFound("mas2.specified=true");

        // Get all the subList where mas2 is null
        defaultSubShouldNotBeFound("mas2.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByCountryincIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryinc equals to DEFAULT_COUNTRYINC
        defaultSubShouldBeFound("countryinc.equals=" + DEFAULT_COUNTRYINC);

        // Get all the subList where countryinc equals to UPDATED_COUNTRYINC
        defaultSubShouldNotBeFound("countryinc.equals=" + UPDATED_COUNTRYINC);
    }

    @Test
    @Transactional
    public void getAllSubsByCountryincIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryinc in DEFAULT_COUNTRYINC or UPDATED_COUNTRYINC
        defaultSubShouldBeFound("countryinc.in=" + DEFAULT_COUNTRYINC + "," + UPDATED_COUNTRYINC);

        // Get all the subList where countryinc equals to UPDATED_COUNTRYINC
        defaultSubShouldNotBeFound("countryinc.in=" + UPDATED_COUNTRYINC);
    }

    @Test
    @Transactional
    public void getAllSubsByCountryincIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where countryinc is not null
        defaultSubShouldBeFound("countryinc.specified=true");

        // Get all the subList where countryinc is null
        defaultSubShouldNotBeFound("countryinc.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByStprincIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprinc equals to DEFAULT_STPRINC
        defaultSubShouldBeFound("stprinc.equals=" + DEFAULT_STPRINC);

        // Get all the subList where stprinc equals to UPDATED_STPRINC
        defaultSubShouldNotBeFound("stprinc.equals=" + UPDATED_STPRINC);
    }

    @Test
    @Transactional
    public void getAllSubsByStprincIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprinc in DEFAULT_STPRINC or UPDATED_STPRINC
        defaultSubShouldBeFound("stprinc.in=" + DEFAULT_STPRINC + "," + UPDATED_STPRINC);

        // Get all the subList where stprinc equals to UPDATED_STPRINC
        defaultSubShouldNotBeFound("stprinc.in=" + UPDATED_STPRINC);
    }

    @Test
    @Transactional
    public void getAllSubsByStprincIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where stprinc is not null
        defaultSubShouldBeFound("stprinc.specified=true");

        // Get all the subList where stprinc is null
        defaultSubShouldNotBeFound("stprinc.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByEinIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where ein equals to DEFAULT_EIN
        defaultSubShouldBeFound("ein.equals=" + DEFAULT_EIN);

        // Get all the subList where ein equals to UPDATED_EIN
        defaultSubShouldNotBeFound("ein.equals=" + UPDATED_EIN);
    }

    @Test
    @Transactional
    public void getAllSubsByEinIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where ein in DEFAULT_EIN or UPDATED_EIN
        defaultSubShouldBeFound("ein.in=" + DEFAULT_EIN + "," + UPDATED_EIN);

        // Get all the subList where ein equals to UPDATED_EIN
        defaultSubShouldNotBeFound("ein.in=" + UPDATED_EIN);
    }

    @Test
    @Transactional
    public void getAllSubsByEinIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where ein is not null
        defaultSubShouldBeFound("ein.specified=true");

        // Get all the subList where ein is null
        defaultSubShouldNotBeFound("ein.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByEinIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where ein greater than or equals to DEFAULT_EIN
        defaultSubShouldBeFound("ein.greaterOrEqualThan=" + DEFAULT_EIN);

        // Get all the subList where ein greater than or equals to (DEFAULT_EIN + 1)
        defaultSubShouldNotBeFound("ein.greaterOrEqualThan=" + (DEFAULT_EIN + 1));
    }

    @Test
    @Transactional
    public void getAllSubsByEinIsLessThanSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where ein less than or equals to DEFAULT_EIN
        defaultSubShouldNotBeFound("ein.lessThan=" + DEFAULT_EIN);

        // Get all the subList where ein less than or equals to (DEFAULT_EIN + 1)
        defaultSubShouldBeFound("ein.lessThan=" + (DEFAULT_EIN + 1));
    }


    @Test
    @Transactional
    public void getAllSubsByFormerIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where former equals to DEFAULT_FORMER
        defaultSubShouldBeFound("former.equals=" + DEFAULT_FORMER);

        // Get all the subList where former equals to UPDATED_FORMER
        defaultSubShouldNotBeFound("former.equals=" + UPDATED_FORMER);
    }

    @Test
    @Transactional
    public void getAllSubsByFormerIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where former in DEFAULT_FORMER or UPDATED_FORMER
        defaultSubShouldBeFound("former.in=" + DEFAULT_FORMER + "," + UPDATED_FORMER);

        // Get all the subList where former equals to UPDATED_FORMER
        defaultSubShouldNotBeFound("former.in=" + UPDATED_FORMER);
    }

    @Test
    @Transactional
    public void getAllSubsByFormerIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where former is not null
        defaultSubShouldBeFound("former.specified=true");

        // Get all the subList where former is null
        defaultSubShouldNotBeFound("former.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByChangedIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where changed equals to DEFAULT_CHANGED
        defaultSubShouldBeFound("changed.equals=" + DEFAULT_CHANGED);

        // Get all the subList where changed equals to UPDATED_CHANGED
        defaultSubShouldNotBeFound("changed.equals=" + UPDATED_CHANGED);
    }

    @Test
    @Transactional
    public void getAllSubsByChangedIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where changed in DEFAULT_CHANGED or UPDATED_CHANGED
        defaultSubShouldBeFound("changed.in=" + DEFAULT_CHANGED + "," + UPDATED_CHANGED);

        // Get all the subList where changed equals to UPDATED_CHANGED
        defaultSubShouldNotBeFound("changed.in=" + UPDATED_CHANGED);
    }

    @Test
    @Transactional
    public void getAllSubsByChangedIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where changed is not null
        defaultSubShouldBeFound("changed.specified=true");

        // Get all the subList where changed is null
        defaultSubShouldNotBeFound("changed.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByAfsIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where afs equals to DEFAULT_AFS
        defaultSubShouldBeFound("afs.equals=" + DEFAULT_AFS);

        // Get all the subList where afs equals to UPDATED_AFS
        defaultSubShouldNotBeFound("afs.equals=" + UPDATED_AFS);
    }

    @Test
    @Transactional
    public void getAllSubsByAfsIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where afs in DEFAULT_AFS or UPDATED_AFS
        defaultSubShouldBeFound("afs.in=" + DEFAULT_AFS + "," + UPDATED_AFS);

        // Get all the subList where afs equals to UPDATED_AFS
        defaultSubShouldNotBeFound("afs.in=" + UPDATED_AFS);
    }

    @Test
    @Transactional
    public void getAllSubsByAfsIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where afs is not null
        defaultSubShouldBeFound("afs.specified=true");

        // Get all the subList where afs is null
        defaultSubShouldNotBeFound("afs.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByWksiIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where wksi equals to DEFAULT_WKSI
        defaultSubShouldBeFound("wksi.equals=" + DEFAULT_WKSI);

        // Get all the subList where wksi equals to UPDATED_WKSI
        defaultSubShouldNotBeFound("wksi.equals=" + UPDATED_WKSI);
    }

    @Test
    @Transactional
    public void getAllSubsByWksiIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where wksi in DEFAULT_WKSI or UPDATED_WKSI
        defaultSubShouldBeFound("wksi.in=" + DEFAULT_WKSI + "," + UPDATED_WKSI);

        // Get all the subList where wksi equals to UPDATED_WKSI
        defaultSubShouldNotBeFound("wksi.in=" + UPDATED_WKSI);
    }

    @Test
    @Transactional
    public void getAllSubsByWksiIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where wksi is not null
        defaultSubShouldBeFound("wksi.specified=true");

        // Get all the subList where wksi is null
        defaultSubShouldNotBeFound("wksi.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByFyeIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fye equals to DEFAULT_FYE
        defaultSubShouldBeFound("fye.equals=" + DEFAULT_FYE);

        // Get all the subList where fye equals to UPDATED_FYE
        defaultSubShouldNotBeFound("fye.equals=" + UPDATED_FYE);
    }

    @Test
    @Transactional
    public void getAllSubsByFyeIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fye in DEFAULT_FYE or UPDATED_FYE
        defaultSubShouldBeFound("fye.in=" + DEFAULT_FYE + "," + UPDATED_FYE);

        // Get all the subList where fye equals to UPDATED_FYE
        defaultSubShouldNotBeFound("fye.in=" + UPDATED_FYE);
    }

    @Test
    @Transactional
    public void getAllSubsByFyeIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fye is not null
        defaultSubShouldBeFound("fye.specified=true");

        // Get all the subList where fye is null
        defaultSubShouldNotBeFound("fye.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByFormIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where form equals to DEFAULT_FORM
        defaultSubShouldBeFound("form.equals=" + DEFAULT_FORM);

        // Get all the subList where form equals to UPDATED_FORM
        defaultSubShouldNotBeFound("form.equals=" + UPDATED_FORM);
    }

    @Test
    @Transactional
    public void getAllSubsByFormIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where form in DEFAULT_FORM or UPDATED_FORM
        defaultSubShouldBeFound("form.in=" + DEFAULT_FORM + "," + UPDATED_FORM);

        // Get all the subList where form equals to UPDATED_FORM
        defaultSubShouldNotBeFound("form.in=" + UPDATED_FORM);
    }

    @Test
    @Transactional
    public void getAllSubsByFormIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where form is not null
        defaultSubShouldBeFound("form.specified=true");

        // Get all the subList where form is null
        defaultSubShouldNotBeFound("form.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByPeriodIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where period equals to DEFAULT_PERIOD
        defaultSubShouldBeFound("period.equals=" + DEFAULT_PERIOD);

        // Get all the subList where period equals to UPDATED_PERIOD
        defaultSubShouldNotBeFound("period.equals=" + UPDATED_PERIOD);
    }

    @Test
    @Transactional
    public void getAllSubsByPeriodIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where period in DEFAULT_PERIOD or UPDATED_PERIOD
        defaultSubShouldBeFound("period.in=" + DEFAULT_PERIOD + "," + UPDATED_PERIOD);

        // Get all the subList where period equals to UPDATED_PERIOD
        defaultSubShouldNotBeFound("period.in=" + UPDATED_PERIOD);
    }

    @Test
    @Transactional
    public void getAllSubsByPeriodIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where period is not null
        defaultSubShouldBeFound("period.specified=true");

        // Get all the subList where period is null
        defaultSubShouldNotBeFound("period.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByFyIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fy equals to DEFAULT_FY
        defaultSubShouldBeFound("fy.equals=" + DEFAULT_FY);

        // Get all the subList where fy equals to UPDATED_FY
        defaultSubShouldNotBeFound("fy.equals=" + UPDATED_FY);
    }

    @Test
    @Transactional
    public void getAllSubsByFyIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fy in DEFAULT_FY or UPDATED_FY
        defaultSubShouldBeFound("fy.in=" + DEFAULT_FY + "," + UPDATED_FY);

        // Get all the subList where fy equals to UPDATED_FY
        defaultSubShouldNotBeFound("fy.in=" + UPDATED_FY);
    }

    @Test
    @Transactional
    public void getAllSubsByFyIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fy is not null
        defaultSubShouldBeFound("fy.specified=true");

        // Get all the subList where fy is null
        defaultSubShouldNotBeFound("fy.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByFyIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fy greater than or equals to DEFAULT_FY
        defaultSubShouldBeFound("fy.greaterOrEqualThan=" + DEFAULT_FY);

        // Get all the subList where fy greater than or equals to (DEFAULT_FY + 1)
        defaultSubShouldNotBeFound("fy.greaterOrEqualThan=" + (DEFAULT_FY + 1));
    }

    @Test
    @Transactional
    public void getAllSubsByFyIsLessThanSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fy less than or equals to DEFAULT_FY
        defaultSubShouldNotBeFound("fy.lessThan=" + DEFAULT_FY);

        // Get all the subList where fy less than or equals to (DEFAULT_FY + 1)
        defaultSubShouldBeFound("fy.lessThan=" + (DEFAULT_FY + 1));
    }


    @Test
    @Transactional
    public void getAllSubsByFpIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fp equals to DEFAULT_FP
        defaultSubShouldBeFound("fp.equals=" + DEFAULT_FP);

        // Get all the subList where fp equals to UPDATED_FP
        defaultSubShouldNotBeFound("fp.equals=" + UPDATED_FP);
    }

    @Test
    @Transactional
    public void getAllSubsByFpIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fp in DEFAULT_FP or UPDATED_FP
        defaultSubShouldBeFound("fp.in=" + DEFAULT_FP + "," + UPDATED_FP);

        // Get all the subList where fp equals to UPDATED_FP
        defaultSubShouldNotBeFound("fp.in=" + UPDATED_FP);
    }

    @Test
    @Transactional
    public void getAllSubsByFpIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where fp is not null
        defaultSubShouldBeFound("fp.specified=true");

        // Get all the subList where fp is null
        defaultSubShouldNotBeFound("fp.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByFiledIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where filed equals to DEFAULT_FILED
        defaultSubShouldBeFound("filed.equals=" + DEFAULT_FILED);

        // Get all the subList where filed equals to UPDATED_FILED
        defaultSubShouldNotBeFound("filed.equals=" + UPDATED_FILED);
    }

    @Test
    @Transactional
    public void getAllSubsByFiledIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where filed in DEFAULT_FILED or UPDATED_FILED
        defaultSubShouldBeFound("filed.in=" + DEFAULT_FILED + "," + UPDATED_FILED);

        // Get all the subList where filed equals to UPDATED_FILED
        defaultSubShouldNotBeFound("filed.in=" + UPDATED_FILED);
    }

    @Test
    @Transactional
    public void getAllSubsByFiledIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where filed is not null
        defaultSubShouldBeFound("filed.specified=true");

        // Get all the subList where filed is null
        defaultSubShouldNotBeFound("filed.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByAcceptedIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where accepted equals to DEFAULT_ACCEPTED
        defaultSubShouldBeFound("accepted.equals=" + DEFAULT_ACCEPTED);

        // Get all the subList where accepted equals to UPDATED_ACCEPTED
        defaultSubShouldNotBeFound("accepted.equals=" + UPDATED_ACCEPTED);
    }

    @Test
    @Transactional
    public void getAllSubsByAcceptedIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where accepted in DEFAULT_ACCEPTED or UPDATED_ACCEPTED
        defaultSubShouldBeFound("accepted.in=" + DEFAULT_ACCEPTED + "," + UPDATED_ACCEPTED);

        // Get all the subList where accepted equals to UPDATED_ACCEPTED
        defaultSubShouldNotBeFound("accepted.in=" + UPDATED_ACCEPTED);
    }

    @Test
    @Transactional
    public void getAllSubsByAcceptedIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where accepted is not null
        defaultSubShouldBeFound("accepted.specified=true");

        // Get all the subList where accepted is null
        defaultSubShouldNotBeFound("accepted.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByPrevrptIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where prevrpt equals to DEFAULT_PREVRPT
        defaultSubShouldBeFound("prevrpt.equals=" + DEFAULT_PREVRPT);

        // Get all the subList where prevrpt equals to UPDATED_PREVRPT
        defaultSubShouldNotBeFound("prevrpt.equals=" + UPDATED_PREVRPT);
    }

    @Test
    @Transactional
    public void getAllSubsByPrevrptIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where prevrpt in DEFAULT_PREVRPT or UPDATED_PREVRPT
        defaultSubShouldBeFound("prevrpt.in=" + DEFAULT_PREVRPT + "," + UPDATED_PREVRPT);

        // Get all the subList where prevrpt equals to UPDATED_PREVRPT
        defaultSubShouldNotBeFound("prevrpt.in=" + UPDATED_PREVRPT);
    }

    @Test
    @Transactional
    public void getAllSubsByPrevrptIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where prevrpt is not null
        defaultSubShouldBeFound("prevrpt.specified=true");

        // Get all the subList where prevrpt is null
        defaultSubShouldNotBeFound("prevrpt.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByDetailIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where detail equals to DEFAULT_DETAIL
        defaultSubShouldBeFound("detail.equals=" + DEFAULT_DETAIL);

        // Get all the subList where detail equals to UPDATED_DETAIL
        defaultSubShouldNotBeFound("detail.equals=" + UPDATED_DETAIL);
    }

    @Test
    @Transactional
    public void getAllSubsByDetailIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where detail in DEFAULT_DETAIL or UPDATED_DETAIL
        defaultSubShouldBeFound("detail.in=" + DEFAULT_DETAIL + "," + UPDATED_DETAIL);

        // Get all the subList where detail equals to UPDATED_DETAIL
        defaultSubShouldNotBeFound("detail.in=" + UPDATED_DETAIL);
    }

    @Test
    @Transactional
    public void getAllSubsByDetailIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where detail is not null
        defaultSubShouldBeFound("detail.specified=true");

        // Get all the subList where detail is null
        defaultSubShouldNotBeFound("detail.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByInstanceIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where instance equals to DEFAULT_INSTANCE
        defaultSubShouldBeFound("instance.equals=" + DEFAULT_INSTANCE);

        // Get all the subList where instance equals to UPDATED_INSTANCE
        defaultSubShouldNotBeFound("instance.equals=" + UPDATED_INSTANCE);
    }

    @Test
    @Transactional
    public void getAllSubsByInstanceIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where instance in DEFAULT_INSTANCE or UPDATED_INSTANCE
        defaultSubShouldBeFound("instance.in=" + DEFAULT_INSTANCE + "," + UPDATED_INSTANCE);

        // Get all the subList where instance equals to UPDATED_INSTANCE
        defaultSubShouldNotBeFound("instance.in=" + UPDATED_INSTANCE);
    }

    @Test
    @Transactional
    public void getAllSubsByInstanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where instance is not null
        defaultSubShouldBeFound("instance.specified=true");

        // Get all the subList where instance is null
        defaultSubShouldNotBeFound("instance.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByNciksIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where nciks equals to DEFAULT_NCIKS
        defaultSubShouldBeFound("nciks.equals=" + DEFAULT_NCIKS);

        // Get all the subList where nciks equals to UPDATED_NCIKS
        defaultSubShouldNotBeFound("nciks.equals=" + UPDATED_NCIKS);
    }

    @Test
    @Transactional
    public void getAllSubsByNciksIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where nciks in DEFAULT_NCIKS or UPDATED_NCIKS
        defaultSubShouldBeFound("nciks.in=" + DEFAULT_NCIKS + "," + UPDATED_NCIKS);

        // Get all the subList where nciks equals to UPDATED_NCIKS
        defaultSubShouldNotBeFound("nciks.in=" + UPDATED_NCIKS);
    }

    @Test
    @Transactional
    public void getAllSubsByNciksIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where nciks is not null
        defaultSubShouldBeFound("nciks.specified=true");

        // Get all the subList where nciks is null
        defaultSubShouldNotBeFound("nciks.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByNciksIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where nciks greater than or equals to DEFAULT_NCIKS
        defaultSubShouldBeFound("nciks.greaterOrEqualThan=" + DEFAULT_NCIKS);

        // Get all the subList where nciks greater than or equals to (DEFAULT_NCIKS + 1)
        defaultSubShouldNotBeFound("nciks.greaterOrEqualThan=" + (DEFAULT_NCIKS + 1));
    }

    @Test
    @Transactional
    public void getAllSubsByNciksIsLessThanSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where nciks less than or equals to DEFAULT_NCIKS
        defaultSubShouldNotBeFound("nciks.lessThan=" + DEFAULT_NCIKS);

        // Get all the subList where nciks less than or equals to (DEFAULT_NCIKS + 1)
        defaultSubShouldBeFound("nciks.lessThan=" + (DEFAULT_NCIKS + 1));
    }


    @Test
    @Transactional
    public void getAllSubsByAciksIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where aciks equals to DEFAULT_ACIKS
        defaultSubShouldBeFound("aciks.equals=" + DEFAULT_ACIKS);

        // Get all the subList where aciks equals to UPDATED_ACIKS
        defaultSubShouldNotBeFound("aciks.equals=" + UPDATED_ACIKS);
    }

    @Test
    @Transactional
    public void getAllSubsByAciksIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where aciks in DEFAULT_ACIKS or UPDATED_ACIKS
        defaultSubShouldBeFound("aciks.in=" + DEFAULT_ACIKS + "," + UPDATED_ACIKS);

        // Get all the subList where aciks equals to UPDATED_ACIKS
        defaultSubShouldNotBeFound("aciks.in=" + UPDATED_ACIKS);
    }

    @Test
    @Transactional
    public void getAllSubsByAciksIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where aciks is not null
        defaultSubShouldBeFound("aciks.specified=true");

        // Get all the subList where aciks is null
        defaultSubShouldNotBeFound("aciks.specified=false");
    }

    @Test
    @Transactional
    public void getAllSubsByYearAndQuarterIsEqualToSomething() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where yearAndQuarter equals to DEFAULT_YEAR_AND_QUARTER
        defaultSubShouldBeFound("yearAndQuarter.equals=" + DEFAULT_YEAR_AND_QUARTER);

        // Get all the subList where yearAndQuarter equals to UPDATED_YEAR_AND_QUARTER
        defaultSubShouldNotBeFound("yearAndQuarter.equals=" + UPDATED_YEAR_AND_QUARTER);
    }

    @Test
    @Transactional
    public void getAllSubsByYearAndQuarterIsInShouldWork() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where yearAndQuarter in DEFAULT_YEAR_AND_QUARTER or UPDATED_YEAR_AND_QUARTER
        defaultSubShouldBeFound("yearAndQuarter.in=" + DEFAULT_YEAR_AND_QUARTER + "," + UPDATED_YEAR_AND_QUARTER);

        // Get all the subList where yearAndQuarter equals to UPDATED_YEAR_AND_QUARTER
        defaultSubShouldNotBeFound("yearAndQuarter.in=" + UPDATED_YEAR_AND_QUARTER);
    }

    @Test
    @Transactional
    public void getAllSubsByYearAndQuarterIsNullOrNotNull() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        // Get all the subList where yearAndQuarter is not null
        defaultSubShouldBeFound("yearAndQuarter.specified=true");

        // Get all the subList where yearAndQuarter is null
        defaultSubShouldNotBeFound("yearAndQuarter.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSubShouldBeFound(String filter) throws Exception {
        restSubMockMvc.perform(get("/api/subs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sub.getId().intValue())))
            .andExpect(jsonPath("$.[*].adsh").value(hasItem(DEFAULT_ADSH)))
            .andExpect(jsonPath("$.[*].cik").value(hasItem(DEFAULT_CIK.intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].sic").value(hasItem(DEFAULT_SIC)))
            .andExpect(jsonPath("$.[*].countryba").value(hasItem(DEFAULT_COUNTRYBA)))
            .andExpect(jsonPath("$.[*].stprba").value(hasItem(DEFAULT_STPRBA)))
            .andExpect(jsonPath("$.[*].cityba").value(hasItem(DEFAULT_CITYBA)))
            .andExpect(jsonPath("$.[*].zipba").value(hasItem(DEFAULT_ZIPBA)))
            .andExpect(jsonPath("$.[*].bas1").value(hasItem(DEFAULT_BAS_1)))
            .andExpect(jsonPath("$.[*].bas2").value(hasItem(DEFAULT_BAS_2)))
            .andExpect(jsonPath("$.[*].baph").value(hasItem(DEFAULT_BAPH)))
            .andExpect(jsonPath("$.[*].countryma").value(hasItem(DEFAULT_COUNTRYMA)))
            .andExpect(jsonPath("$.[*].stprma").value(hasItem(DEFAULT_STPRMA)))
            .andExpect(jsonPath("$.[*].cityma").value(hasItem(DEFAULT_CITYMA)))
            .andExpect(jsonPath("$.[*].zipma").value(hasItem(DEFAULT_ZIPMA)))
            .andExpect(jsonPath("$.[*].mas1").value(hasItem(DEFAULT_MAS_1)))
            .andExpect(jsonPath("$.[*].mas2").value(hasItem(DEFAULT_MAS_2)))
            .andExpect(jsonPath("$.[*].countryinc").value(hasItem(DEFAULT_COUNTRYINC)))
            .andExpect(jsonPath("$.[*].stprinc").value(hasItem(DEFAULT_STPRINC)))
            .andExpect(jsonPath("$.[*].ein").value(hasItem(DEFAULT_EIN.intValue())))
            .andExpect(jsonPath("$.[*].former").value(hasItem(DEFAULT_FORMER)))
            .andExpect(jsonPath("$.[*].changed").value(hasItem(DEFAULT_CHANGED)))
            .andExpect(jsonPath("$.[*].afs").value(hasItem(DEFAULT_AFS)))
            .andExpect(jsonPath("$.[*].wksi").value(hasItem(DEFAULT_WKSI.booleanValue())))
            .andExpect(jsonPath("$.[*].fye").value(hasItem(DEFAULT_FYE)))
            .andExpect(jsonPath("$.[*].form").value(hasItem(DEFAULT_FORM)))
            .andExpect(jsonPath("$.[*].period").value(hasItem(DEFAULT_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].fy").value(hasItem(DEFAULT_FY)))
            .andExpect(jsonPath("$.[*].fp").value(hasItem(DEFAULT_FP)))
            .andExpect(jsonPath("$.[*].filed").value(hasItem(DEFAULT_FILED.toString())))
            .andExpect(jsonPath("$.[*].accepted").value(hasItem(DEFAULT_ACCEPTED.toString())))
            .andExpect(jsonPath("$.[*].prevrpt").value(hasItem(DEFAULT_PREVRPT.booleanValue())))
            .andExpect(jsonPath("$.[*].detail").value(hasItem(DEFAULT_DETAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].instance").value(hasItem(DEFAULT_INSTANCE)))
            .andExpect(jsonPath("$.[*].nciks").value(hasItem(DEFAULT_NCIKS)))
            .andExpect(jsonPath("$.[*].aciks").value(hasItem(DEFAULT_ACIKS)))
            .andExpect(jsonPath("$.[*].yearAndQuarter").value(hasItem(DEFAULT_YEAR_AND_QUARTER)));

        // Check, that the count call also returns 1
        restSubMockMvc.perform(get("/api/subs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSubShouldNotBeFound(String filter) throws Exception {
        restSubMockMvc.perform(get("/api/subs?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSubMockMvc.perform(get("/api/subs/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingSub() throws Exception {
        // Get the sub
        restSubMockMvc.perform(get("/api/subs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSub() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        int databaseSizeBeforeUpdate = subRepository.findAll().size();

        // Update the sub
        Sub updatedSub = subRepository.findById(sub.getId()).get();
        // Disconnect from session so that the updates on updatedSub are not directly saved in db
        em.detach(updatedSub);
        updatedSub
            .adsh(UPDATED_ADSH)
            .cik(UPDATED_CIK)
            .name(UPDATED_NAME)
            .sic(UPDATED_SIC)
            .countryba(UPDATED_COUNTRYBA)
            .stprba(UPDATED_STPRBA)
            .cityba(UPDATED_CITYBA)
            .zipba(UPDATED_ZIPBA)
            .bas1(UPDATED_BAS_1)
            .bas2(UPDATED_BAS_2)
            .baph(UPDATED_BAPH)
            .countryma(UPDATED_COUNTRYMA)
            .stprma(UPDATED_STPRMA)
            .cityma(UPDATED_CITYMA)
            .zipma(UPDATED_ZIPMA)
            .mas1(UPDATED_MAS_1)
            .mas2(UPDATED_MAS_2)
            .countryinc(UPDATED_COUNTRYINC)
            .stprinc(UPDATED_STPRINC)
            .ein(UPDATED_EIN)
            .former(UPDATED_FORMER)
            .changed(UPDATED_CHANGED)
            .afs(UPDATED_AFS)
            .wksi(UPDATED_WKSI)
            .fye(UPDATED_FYE)
            .form(UPDATED_FORM)
            .period(UPDATED_PERIOD)
            .fy(UPDATED_FY)
            .fp(UPDATED_FP)
            .filed(UPDATED_FILED)
            .accepted(UPDATED_ACCEPTED)
            .prevrpt(UPDATED_PREVRPT)
            .detail(UPDATED_DETAIL)
            .instance(UPDATED_INSTANCE)
            .nciks(UPDATED_NCIKS)
            .aciks(UPDATED_ACIKS)
            .yearAndQuarter(UPDATED_YEAR_AND_QUARTER);
        SubDTO subDTO = subMapper.toDto(updatedSub);

        restSubMockMvc.perform(put("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isOk());

        // Validate the Sub in the database
        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeUpdate);
        Sub testSub = subList.get(subList.size() - 1);
        assertThat(testSub.getAdsh()).isEqualTo(UPDATED_ADSH);
        assertThat(testSub.getCik()).isEqualTo(UPDATED_CIK);
        assertThat(testSub.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testSub.getSic()).isEqualTo(UPDATED_SIC);
        assertThat(testSub.getCountryba()).isEqualTo(UPDATED_COUNTRYBA);
        assertThat(testSub.getStprba()).isEqualTo(UPDATED_STPRBA);
        assertThat(testSub.getCityba()).isEqualTo(UPDATED_CITYBA);
        assertThat(testSub.getZipba()).isEqualTo(UPDATED_ZIPBA);
        assertThat(testSub.getBas1()).isEqualTo(UPDATED_BAS_1);
        assertThat(testSub.getBas2()).isEqualTo(UPDATED_BAS_2);
        assertThat(testSub.getBaph()).isEqualTo(UPDATED_BAPH);
        assertThat(testSub.getCountryma()).isEqualTo(UPDATED_COUNTRYMA);
        assertThat(testSub.getStprma()).isEqualTo(UPDATED_STPRMA);
        assertThat(testSub.getCityma()).isEqualTo(UPDATED_CITYMA);
        assertThat(testSub.getZipma()).isEqualTo(UPDATED_ZIPMA);
        assertThat(testSub.getMas1()).isEqualTo(UPDATED_MAS_1);
        assertThat(testSub.getMas2()).isEqualTo(UPDATED_MAS_2);
        assertThat(testSub.getCountryinc()).isEqualTo(UPDATED_COUNTRYINC);
        assertThat(testSub.getStprinc()).isEqualTo(UPDATED_STPRINC);
        assertThat(testSub.getEin()).isEqualTo(UPDATED_EIN);
        assertThat(testSub.getFormer()).isEqualTo(UPDATED_FORMER);
        assertThat(testSub.getChanged()).isEqualTo(UPDATED_CHANGED);
        assertThat(testSub.getAfs()).isEqualTo(UPDATED_AFS);
        assertThat(testSub.isWksi()).isEqualTo(UPDATED_WKSI);
        assertThat(testSub.getFye()).isEqualTo(UPDATED_FYE);
        assertThat(testSub.getForm()).isEqualTo(UPDATED_FORM);
        assertThat(testSub.getPeriod()).isEqualTo(UPDATED_PERIOD);
        assertThat(testSub.getFy()).isEqualTo(UPDATED_FY);
        assertThat(testSub.getFp()).isEqualTo(UPDATED_FP);
        assertThat(testSub.getFiled()).isEqualTo(UPDATED_FILED);
        assertThat(testSub.getAccepted()).isEqualTo(UPDATED_ACCEPTED);
        assertThat(testSub.isPrevrpt()).isEqualTo(UPDATED_PREVRPT);
        assertThat(testSub.isDetail()).isEqualTo(UPDATED_DETAIL);
        assertThat(testSub.getInstance()).isEqualTo(UPDATED_INSTANCE);
        assertThat(testSub.getNciks()).isEqualTo(UPDATED_NCIKS);
        assertThat(testSub.getAciks()).isEqualTo(UPDATED_ACIKS);
        assertThat(testSub.getYearAndQuarter()).isEqualTo(UPDATED_YEAR_AND_QUARTER);
    }

    @Test
    @Transactional
    public void updateNonExistingSub() throws Exception {
        int databaseSizeBeforeUpdate = subRepository.findAll().size();

        // Create the Sub
        SubDTO subDTO = subMapper.toDto(sub);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubMockMvc.perform(put("/api/subs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(subDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Sub in the database
        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteSub() throws Exception {
        // Initialize the database
        subRepository.saveAndFlush(sub);

        int databaseSizeBeforeDelete = subRepository.findAll().size();

        // Delete the sub
        restSubMockMvc.perform(delete("/api/subs/{id}", sub.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sub> subList = subRepository.findAll();
        assertThat(subList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sub.class);
        Sub sub1 = new Sub();
        sub1.setId(1L);
        Sub sub2 = new Sub();
        sub2.setId(sub1.getId());
        assertThat(sub1).isEqualTo(sub2);
        sub2.setId(2L);
        assertThat(sub1).isNotEqualTo(sub2);
        sub1.setId(null);
        assertThat(sub1).isNotEqualTo(sub2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubDTO.class);
        SubDTO subDTO1 = new SubDTO();
        subDTO1.setId(1L);
        SubDTO subDTO2 = new SubDTO();
        assertThat(subDTO1).isNotEqualTo(subDTO2);
        subDTO2.setId(subDTO1.getId());
        assertThat(subDTO1).isEqualTo(subDTO2);
        subDTO2.setId(2L);
        assertThat(subDTO1).isNotEqualTo(subDTO2);
        subDTO1.setId(null);
        assertThat(subDTO1).isNotEqualTo(subDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(subMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(subMapper.fromId(null)).isNull();
    }
}
