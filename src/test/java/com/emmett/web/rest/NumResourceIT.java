package com.emmett.web.rest;

import com.emmett.EdgarApp;
import com.emmett.domain.Num;
import com.emmett.domain.Sub;
import com.emmett.domain.Tag;
import com.emmett.repository.NumRepository;
import com.emmett.service.NumService;
import com.emmett.service.dto.NumDTO;
import com.emmett.service.mapper.NumMapper;
import com.emmett.web.rest.errors.ExceptionTranslator;
import com.emmett.service.dto.NumCriteria;
import com.emmett.service.NumQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.emmett.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link NumResource} REST controller.
 */
@SpringBootTest(classes = EdgarApp.class)
public class NumResourceIT {

    private static final Integer DEFAULT_COREG = 9999;
    private static final Integer UPDATED_COREG = 9998;

    private static final Instant DEFAULT_DDATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DDATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Long DEFAULT_QTRS = 99999999L;
    private static final Long UPDATED_QTRS = 99999998L;

    private static final String DEFAULT_UOM = "AAAAAAAAAA";
    private static final String UPDATED_UOM = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_VALUE = new BigDecimal(1e+30);
    private static final BigDecimal UPDATED_VALUE = new BigDecimal(0);

    private static final String DEFAULT_FOOTNOTE = "AAAAAAAAAA";
    private static final String UPDATED_FOOTNOTE = "BBBBBBBBBB";

    @Autowired
    private NumRepository numRepository;

    @Autowired
    private NumMapper numMapper;

    @Autowired
    private NumService numService;

    @Autowired
    private NumQueryService numQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNumMockMvc;

    private Num num;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NumResource numResource = new NumResource(numService, numQueryService);
        this.restNumMockMvc = MockMvcBuilders.standaloneSetup(numResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Num createEntity(EntityManager em) {
        Num num = new Num()
            .coreg(DEFAULT_COREG)
            .ddate(DEFAULT_DDATE)
            .qtrs(DEFAULT_QTRS)
            .uom(DEFAULT_UOM)
            .value(DEFAULT_VALUE)
            .footnote(DEFAULT_FOOTNOTE);
        return num;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Num createUpdatedEntity(EntityManager em) {
        Num num = new Num()
            .coreg(UPDATED_COREG)
            .ddate(UPDATED_DDATE)
            .qtrs(UPDATED_QTRS)
            .uom(UPDATED_UOM)
            .value(UPDATED_VALUE)
            .footnote(UPDATED_FOOTNOTE);
        return num;
    }

    @BeforeEach
    public void initTest() {
        num = createEntity(em);
    }

    @Test
    @Transactional
    public void createNum() throws Exception {
        int databaseSizeBeforeCreate = numRepository.findAll().size();

        // Create the Num
        NumDTO numDTO = numMapper.toDto(num);
        restNumMockMvc.perform(post("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isCreated());

        // Validate the Num in the database
        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeCreate + 1);
        Num testNum = numList.get(numList.size() - 1);
        assertThat(testNum.getCoreg()).isEqualTo(DEFAULT_COREG);
        assertThat(testNum.getDdate()).isEqualTo(DEFAULT_DDATE);
        assertThat(testNum.getQtrs()).isEqualTo(DEFAULT_QTRS);
        assertThat(testNum.getUom()).isEqualTo(DEFAULT_UOM);
        assertThat(testNum.getValue()).isEqualTo(DEFAULT_VALUE);
        assertThat(testNum.getFootnote()).isEqualTo(DEFAULT_FOOTNOTE);
    }

    @Test
    @Transactional
    public void createNumWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = numRepository.findAll().size();

        // Create the Num with an existing ID
        num.setId(1L);
        NumDTO numDTO = numMapper.toDto(num);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNumMockMvc.perform(post("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Num in the database
        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = numRepository.findAll().size();
        // set the field null
        num.setDdate(null);

        // Create the Num, which fails.
        NumDTO numDTO = numMapper.toDto(num);

        restNumMockMvc.perform(post("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isBadRequest());

        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkQtrsIsRequired() throws Exception {
        int databaseSizeBeforeTest = numRepository.findAll().size();
        // set the field null
        num.setQtrs(null);

        // Create the Num, which fails.
        NumDTO numDTO = numMapper.toDto(num);

        restNumMockMvc.perform(post("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isBadRequest());

        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUomIsRequired() throws Exception {
        int databaseSizeBeforeTest = numRepository.findAll().size();
        // set the field null
        num.setUom(null);

        // Create the Num, which fails.
        NumDTO numDTO = numMapper.toDto(num);

        restNumMockMvc.perform(post("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isBadRequest());

        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkValueIsRequired() throws Exception {
        int databaseSizeBeforeTest = numRepository.findAll().size();
        // set the field null
        num.setValue(null);

        // Create the Num, which fails.
        NumDTO numDTO = numMapper.toDto(num);

        restNumMockMvc.perform(post("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isBadRequest());

        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNums() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList
        restNumMockMvc.perform(get("/api/nums?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(num.getId().intValue())))
            .andExpect(jsonPath("$.[*].coreg").value(hasItem(DEFAULT_COREG)))
            .andExpect(jsonPath("$.[*].ddate").value(hasItem(DEFAULT_DDATE.toString())))
            .andExpect(jsonPath("$.[*].qtrs").value(hasItem(DEFAULT_QTRS.intValue())))
            .andExpect(jsonPath("$.[*].uom").value(hasItem(DEFAULT_UOM.toString())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].footnote").value(hasItem(DEFAULT_FOOTNOTE.toString())));
    }
    
    @Test
    @Transactional
    public void getNum() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get the num
        restNumMockMvc.perform(get("/api/nums/{id}", num.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(num.getId().intValue()))
            .andExpect(jsonPath("$.coreg").value(DEFAULT_COREG))
            .andExpect(jsonPath("$.ddate").value(DEFAULT_DDATE.toString()))
            .andExpect(jsonPath("$.qtrs").value(DEFAULT_QTRS.intValue()))
            .andExpect(jsonPath("$.uom").value(DEFAULT_UOM.toString()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE.intValue()))
            .andExpect(jsonPath("$.footnote").value(DEFAULT_FOOTNOTE.toString()));
    }

    @Test
    @Transactional
    public void getAllNumsByCoregIsEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where coreg equals to DEFAULT_COREG
        defaultNumShouldBeFound("coreg.equals=" + DEFAULT_COREG);

        // Get all the numList where coreg equals to UPDATED_COREG
        defaultNumShouldNotBeFound("coreg.equals=" + UPDATED_COREG);
    }

    @Test
    @Transactional
    public void getAllNumsByCoregIsInShouldWork() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where coreg in DEFAULT_COREG or UPDATED_COREG
        defaultNumShouldBeFound("coreg.in=" + DEFAULT_COREG + "," + UPDATED_COREG);

        // Get all the numList where coreg equals to UPDATED_COREG
        defaultNumShouldNotBeFound("coreg.in=" + UPDATED_COREG);
    }

    @Test
    @Transactional
    public void getAllNumsByCoregIsNullOrNotNull() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where coreg is not null
        defaultNumShouldBeFound("coreg.specified=true");

        // Get all the numList where coreg is null
        defaultNumShouldNotBeFound("coreg.specified=false");
    }

    @Test
    @Transactional
    public void getAllNumsByCoregIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where coreg greater than or equals to DEFAULT_COREG
        defaultNumShouldBeFound("coreg.greaterOrEqualThan=" + DEFAULT_COREG);

        // Get all the numList where coreg greater than or equals to (DEFAULT_COREG + 1)
        defaultNumShouldNotBeFound("coreg.greaterOrEqualThan=" + (DEFAULT_COREG + 1));
    }

    @Test
    @Transactional
    public void getAllNumsByCoregIsLessThanSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where coreg less than or equals to DEFAULT_COREG
        defaultNumShouldNotBeFound("coreg.lessThan=" + DEFAULT_COREG);

        // Get all the numList where coreg less than or equals to (DEFAULT_COREG + 1)
        defaultNumShouldBeFound("coreg.lessThan=" + (DEFAULT_COREG + 1));
    }


    @Test
    @Transactional
    public void getAllNumsByDdateIsEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where ddate equals to DEFAULT_DDATE
        defaultNumShouldBeFound("ddate.equals=" + DEFAULT_DDATE);

        // Get all the numList where ddate equals to UPDATED_DDATE
        defaultNumShouldNotBeFound("ddate.equals=" + UPDATED_DDATE);
    }

    @Test
    @Transactional
    public void getAllNumsByDdateIsInShouldWork() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where ddate in DEFAULT_DDATE or UPDATED_DDATE
        defaultNumShouldBeFound("ddate.in=" + DEFAULT_DDATE + "," + UPDATED_DDATE);

        // Get all the numList where ddate equals to UPDATED_DDATE
        defaultNumShouldNotBeFound("ddate.in=" + UPDATED_DDATE);
    }

    @Test
    @Transactional
    public void getAllNumsByDdateIsNullOrNotNull() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where ddate is not null
        defaultNumShouldBeFound("ddate.specified=true");

        // Get all the numList where ddate is null
        defaultNumShouldNotBeFound("ddate.specified=false");
    }

    @Test
    @Transactional
    public void getAllNumsByQtrsIsEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where qtrs equals to DEFAULT_QTRS
        defaultNumShouldBeFound("qtrs.equals=" + DEFAULT_QTRS);

        // Get all the numList where qtrs equals to UPDATED_QTRS
        defaultNumShouldNotBeFound("qtrs.equals=" + UPDATED_QTRS);
    }

    @Test
    @Transactional
    public void getAllNumsByQtrsIsInShouldWork() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where qtrs in DEFAULT_QTRS or UPDATED_QTRS
        defaultNumShouldBeFound("qtrs.in=" + DEFAULT_QTRS + "," + UPDATED_QTRS);

        // Get all the numList where qtrs equals to UPDATED_QTRS
        defaultNumShouldNotBeFound("qtrs.in=" + UPDATED_QTRS);
    }

    @Test
    @Transactional
    public void getAllNumsByQtrsIsNullOrNotNull() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where qtrs is not null
        defaultNumShouldBeFound("qtrs.specified=true");

        // Get all the numList where qtrs is null
        defaultNumShouldNotBeFound("qtrs.specified=false");
    }

    @Test
    @Transactional
    public void getAllNumsByQtrsIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where qtrs greater than or equals to DEFAULT_QTRS
        defaultNumShouldBeFound("qtrs.greaterOrEqualThan=" + DEFAULT_QTRS);

        // Get all the numList where qtrs greater than or equals to (DEFAULT_QTRS + 1)
        defaultNumShouldNotBeFound("qtrs.greaterOrEqualThan=" + (DEFAULT_QTRS + 1));
    }

    @Test
    @Transactional
    public void getAllNumsByQtrsIsLessThanSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where qtrs less than or equals to DEFAULT_QTRS
        defaultNumShouldNotBeFound("qtrs.lessThan=" + DEFAULT_QTRS);

        // Get all the numList where qtrs less than or equals to (DEFAULT_QTRS + 1)
        defaultNumShouldBeFound("qtrs.lessThan=" + (DEFAULT_QTRS + 1));
    }


    @Test
    @Transactional
    public void getAllNumsByUomIsEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where uom equals to DEFAULT_UOM
        defaultNumShouldBeFound("uom.equals=" + DEFAULT_UOM);

        // Get all the numList where uom equals to UPDATED_UOM
        defaultNumShouldNotBeFound("uom.equals=" + UPDATED_UOM);
    }

    @Test
    @Transactional
    public void getAllNumsByUomIsInShouldWork() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where uom in DEFAULT_UOM or UPDATED_UOM
        defaultNumShouldBeFound("uom.in=" + DEFAULT_UOM + "," + UPDATED_UOM);

        // Get all the numList where uom equals to UPDATED_UOM
        defaultNumShouldNotBeFound("uom.in=" + UPDATED_UOM);
    }

    @Test
    @Transactional
    public void getAllNumsByUomIsNullOrNotNull() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where uom is not null
        defaultNumShouldBeFound("uom.specified=true");

        // Get all the numList where uom is null
        defaultNumShouldNotBeFound("uom.specified=false");
    }

    @Test
    @Transactional
    public void getAllNumsByValueIsEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where value equals to DEFAULT_VALUE
        defaultNumShouldBeFound("value.equals=" + DEFAULT_VALUE);

        // Get all the numList where value equals to UPDATED_VALUE
        defaultNumShouldNotBeFound("value.equals=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void getAllNumsByValueIsInShouldWork() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where value in DEFAULT_VALUE or UPDATED_VALUE
        defaultNumShouldBeFound("value.in=" + DEFAULT_VALUE + "," + UPDATED_VALUE);

        // Get all the numList where value equals to UPDATED_VALUE
        defaultNumShouldNotBeFound("value.in=" + UPDATED_VALUE);
    }

    @Test
    @Transactional
    public void getAllNumsByValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where value is not null
        defaultNumShouldBeFound("value.specified=true");

        // Get all the numList where value is null
        defaultNumShouldNotBeFound("value.specified=false");
    }

    @Test
    @Transactional
    public void getAllNumsByFootnoteIsEqualToSomething() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where footnote equals to DEFAULT_FOOTNOTE
        defaultNumShouldBeFound("footnote.equals=" + DEFAULT_FOOTNOTE);

        // Get all the numList where footnote equals to UPDATED_FOOTNOTE
        defaultNumShouldNotBeFound("footnote.equals=" + UPDATED_FOOTNOTE);
    }

    @Test
    @Transactional
    public void getAllNumsByFootnoteIsInShouldWork() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where footnote in DEFAULT_FOOTNOTE or UPDATED_FOOTNOTE
        defaultNumShouldBeFound("footnote.in=" + DEFAULT_FOOTNOTE + "," + UPDATED_FOOTNOTE);

        // Get all the numList where footnote equals to UPDATED_FOOTNOTE
        defaultNumShouldNotBeFound("footnote.in=" + UPDATED_FOOTNOTE);
    }

    @Test
    @Transactional
    public void getAllNumsByFootnoteIsNullOrNotNull() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        // Get all the numList where footnote is not null
        defaultNumShouldBeFound("footnote.specified=true");

        // Get all the numList where footnote is null
        defaultNumShouldNotBeFound("footnote.specified=false");
    }

    @Test
    @Transactional
    public void getAllNumsByAdshIsEqualToSomething() throws Exception {
        // Initialize the database
        Sub adsh = SubResourceIT.createEntity(em);
        em.persist(adsh);
        em.flush();
        num.setAdsh(adsh);
        numRepository.saveAndFlush(num);
        Long adshId = adsh.getId();

        // Get all the numList where adsh equals to adshId
        defaultNumShouldBeFound("adshId.equals=" + adshId);

        // Get all the numList where adsh equals to adshId + 1
        defaultNumShouldNotBeFound("adshId.equals=" + (adshId + 1));
    }


    @Test
    @Transactional
    public void getAllNumsByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        Tag tag = TagResourceIT.createEntity(em);
        em.persist(tag);
        em.flush();
        num.setTag(tag);
        numRepository.saveAndFlush(num);
        Long tagId = tag.getId();

        // Get all the numList where tag equals to tagId
        defaultNumShouldBeFound("tagId.equals=" + tagId);

        // Get all the numList where tag equals to tagId + 1
        defaultNumShouldNotBeFound("tagId.equals=" + (tagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultNumShouldBeFound(String filter) throws Exception {
        restNumMockMvc.perform(get("/api/nums?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(num.getId().intValue())))
            .andExpect(jsonPath("$.[*].coreg").value(hasItem(DEFAULT_COREG)))
            .andExpect(jsonPath("$.[*].ddate").value(hasItem(DEFAULT_DDATE.toString())))
            .andExpect(jsonPath("$.[*].qtrs").value(hasItem(DEFAULT_QTRS.intValue())))
            .andExpect(jsonPath("$.[*].uom").value(hasItem(DEFAULT_UOM)))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE.intValue())))
            .andExpect(jsonPath("$.[*].footnote").value(hasItem(DEFAULT_FOOTNOTE)));

        // Check, that the count call also returns 1
        restNumMockMvc.perform(get("/api/nums/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultNumShouldNotBeFound(String filter) throws Exception {
        restNumMockMvc.perform(get("/api/nums?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restNumMockMvc.perform(get("/api/nums/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingNum() throws Exception {
        // Get the num
        restNumMockMvc.perform(get("/api/nums/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNum() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        int databaseSizeBeforeUpdate = numRepository.findAll().size();

        // Update the num
        Num updatedNum = numRepository.findById(num.getId()).get();
        // Disconnect from session so that the updates on updatedNum are not directly saved in db
        em.detach(updatedNum);
        updatedNum
            .coreg(UPDATED_COREG)
            .ddate(UPDATED_DDATE)
            .qtrs(UPDATED_QTRS)
            .uom(UPDATED_UOM)
            .value(UPDATED_VALUE)
            .footnote(UPDATED_FOOTNOTE);
        NumDTO numDTO = numMapper.toDto(updatedNum);

        restNumMockMvc.perform(put("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isOk());

        // Validate the Num in the database
        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeUpdate);
        Num testNum = numList.get(numList.size() - 1);
        assertThat(testNum.getCoreg()).isEqualTo(UPDATED_COREG);
        assertThat(testNum.getDdate()).isEqualTo(UPDATED_DDATE);
        assertThat(testNum.getQtrs()).isEqualTo(UPDATED_QTRS);
        assertThat(testNum.getUom()).isEqualTo(UPDATED_UOM);
        assertThat(testNum.getValue()).isEqualTo(UPDATED_VALUE);
        assertThat(testNum.getFootnote()).isEqualTo(UPDATED_FOOTNOTE);
    }

    @Test
    @Transactional
    public void updateNonExistingNum() throws Exception {
        int databaseSizeBeforeUpdate = numRepository.findAll().size();

        // Create the Num
        NumDTO numDTO = numMapper.toDto(num);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNumMockMvc.perform(put("/api/nums")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(numDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Num in the database
        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNum() throws Exception {
        // Initialize the database
        numRepository.saveAndFlush(num);

        int databaseSizeBeforeDelete = numRepository.findAll().size();

        // Delete the num
        restNumMockMvc.perform(delete("/api/nums/{id}", num.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Num> numList = numRepository.findAll();
        assertThat(numList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Num.class);
        Num num1 = new Num();
        num1.setId(1L);
        Num num2 = new Num();
        num2.setId(num1.getId());
        assertThat(num1).isEqualTo(num2);
        num2.setId(2L);
        assertThat(num1).isNotEqualTo(num2);
        num1.setId(null);
        assertThat(num1).isNotEqualTo(num2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(NumDTO.class);
        NumDTO numDTO1 = new NumDTO();
        numDTO1.setId(1L);
        NumDTO numDTO2 = new NumDTO();
        assertThat(numDTO1).isNotEqualTo(numDTO2);
        numDTO2.setId(numDTO1.getId());
        assertThat(numDTO1).isEqualTo(numDTO2);
        numDTO2.setId(2L);
        assertThat(numDTO1).isNotEqualTo(numDTO2);
        numDTO1.setId(null);
        assertThat(numDTO1).isNotEqualTo(numDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(numMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(numMapper.fromId(null)).isNull();
    }
}
