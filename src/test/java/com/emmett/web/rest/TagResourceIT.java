package com.emmett.web.rest;

import com.emmett.EdgarApp;
import com.emmett.domain.Tag;
import com.emmett.repository.TagRepository;
import com.emmett.service.TagService;
import com.emmett.service.dto.TagDTO;
import com.emmett.service.mapper.TagMapper;
import com.emmett.web.rest.errors.ExceptionTranslator;
import com.emmett.service.dto.TagCriteria;
import com.emmett.service.TagQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.emmett.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link TagResource} REST controller.
 */
@SpringBootTest(classes = EdgarApp.class)
public class TagResourceIT {

    private static final String DEFAULT_TAG = "AAAAAAAAAA";
    private static final String UPDATED_TAG = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CUSTOM = false;
    private static final Boolean UPDATED_CUSTOM = true;

    private static final Boolean DEFAULT_ABSTRCT = false;
    private static final Boolean UPDATED_ABSTRCT = true;

    private static final String DEFAULT_DATATYPE = "AAAAAAAAAA";
    private static final String UPDATED_DATATYPE = "BBBBBBBBBB";

    private static final String DEFAULT_IORD = "A";
    private static final String UPDATED_IORD = "B";

    private static final String DEFAULT_CRDR = "A";
    private static final String UPDATED_CRDR = "B";

    private static final String DEFAULT_TLABEL = "AAAAAAAAAA";
    private static final String UPDATED_TLABEL = "BBBBBBBBBB";

    private static final String DEFAULT_DOC = "AAAAAAAAAA";
    private static final String UPDATED_DOC = "BBBBBBBBBB";

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private TagMapper tagMapper;

    @Autowired
    private TagService tagService;

    @Autowired
    private TagQueryService tagQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTagMockMvc;

    private Tag tag;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TagResource tagResource = new TagResource(tagService, tagQueryService);
        this.restTagMockMvc = MockMvcBuilders.standaloneSetup(tagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tag createEntity(EntityManager em) {
        Tag tag = new Tag()
            .tag(DEFAULT_TAG)
            .version(DEFAULT_VERSION)
            .custom(DEFAULT_CUSTOM)
            .abstrct(DEFAULT_ABSTRCT)
            .datatype(DEFAULT_DATATYPE)
            .iord(DEFAULT_IORD)
            .crdr(DEFAULT_CRDR)
            .tlabel(DEFAULT_TLABEL)
            .doc(DEFAULT_DOC);
        return tag;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tag createUpdatedEntity(EntityManager em) {
        Tag tag = new Tag()
            .tag(UPDATED_TAG)
            .version(UPDATED_VERSION)
            .custom(UPDATED_CUSTOM)
            .abstrct(UPDATED_ABSTRCT)
            .datatype(UPDATED_DATATYPE)
            .iord(UPDATED_IORD)
            .crdr(UPDATED_CRDR)
            .tlabel(UPDATED_TLABEL)
            .doc(UPDATED_DOC);
        return tag;
    }

    @BeforeEach
    public void initTest() {
        tag = createEntity(em);
    }

    @Test
    @Transactional
    public void createTag() throws Exception {
        int databaseSizeBeforeCreate = tagRepository.findAll().size();

        // Create the Tag
        TagDTO tagDTO = tagMapper.toDto(tag);
        restTagMockMvc.perform(post("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isCreated());

        // Validate the Tag in the database
        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeCreate + 1);
        Tag testTag = tagList.get(tagList.size() - 1);
        assertThat(testTag.getTag()).isEqualTo(DEFAULT_TAG);
        assertThat(testTag.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testTag.isCustom()).isEqualTo(DEFAULT_CUSTOM);
        assertThat(testTag.isAbstrct()).isEqualTo(DEFAULT_ABSTRCT);
        assertThat(testTag.getDatatype()).isEqualTo(DEFAULT_DATATYPE);
        assertThat(testTag.getIord()).isEqualTo(DEFAULT_IORD);
        assertThat(testTag.getCrdr()).isEqualTo(DEFAULT_CRDR);
        assertThat(testTag.getTlabel()).isEqualTo(DEFAULT_TLABEL);
        assertThat(testTag.getDoc()).isEqualTo(DEFAULT_DOC);
    }

    @Test
    @Transactional
    public void createTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tagRepository.findAll().size();

        // Create the Tag with an existing ID
        tag.setId(1L);
        TagDTO tagDTO = tagMapper.toDto(tag);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTagMockMvc.perform(post("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tag in the database
        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkTagIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagRepository.findAll().size();
        // set the field null
        tag.setTag(null);

        // Create the Tag, which fails.
        TagDTO tagDTO = tagMapper.toDto(tag);

        restTagMockMvc.perform(post("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isBadRequest());

        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkVersionIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagRepository.findAll().size();
        // set the field null
        tag.setVersion(null);

        // Create the Tag, which fails.
        TagDTO tagDTO = tagMapper.toDto(tag);

        restTagMockMvc.perform(post("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isBadRequest());

        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCustomIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagRepository.findAll().size();
        // set the field null
        tag.setCustom(null);

        // Create the Tag, which fails.
        TagDTO tagDTO = tagMapper.toDto(tag);

        restTagMockMvc.perform(post("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isBadRequest());

        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkAbstrctIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagRepository.findAll().size();
        // set the field null
        tag.setAbstrct(null);

        // Create the Tag, which fails.
        TagDTO tagDTO = tagMapper.toDto(tag);

        restTagMockMvc.perform(post("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isBadRequest());

        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkIordIsRequired() throws Exception {
        int databaseSizeBeforeTest = tagRepository.findAll().size();
        // set the field null
        tag.setIord(null);

        // Create the Tag, which fails.
        TagDTO tagDTO = tagMapper.toDto(tag);

        restTagMockMvc.perform(post("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isBadRequest());

        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTags() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList
        restTagMockMvc.perform(get("/api/tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tag.getId().intValue())))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())))
            .andExpect(jsonPath("$.[*].custom").value(hasItem(DEFAULT_CUSTOM.booleanValue())))
            .andExpect(jsonPath("$.[*].abstrct").value(hasItem(DEFAULT_ABSTRCT.booleanValue())))
            .andExpect(jsonPath("$.[*].datatype").value(hasItem(DEFAULT_DATATYPE.toString())))
            .andExpect(jsonPath("$.[*].iord").value(hasItem(DEFAULT_IORD.toString())))
            .andExpect(jsonPath("$.[*].crdr").value(hasItem(DEFAULT_CRDR.toString())))
            .andExpect(jsonPath("$.[*].tlabel").value(hasItem(DEFAULT_TLABEL.toString())))
            .andExpect(jsonPath("$.[*].doc").value(hasItem(DEFAULT_DOC.toString())));
    }
    
    @Test
    @Transactional
    public void getTag() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get the tag
        restTagMockMvc.perform(get("/api/tags/{id}", tag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tag.getId().intValue()))
            .andExpect(jsonPath("$.tag").value(DEFAULT_TAG.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.toString()))
            .andExpect(jsonPath("$.custom").value(DEFAULT_CUSTOM.booleanValue()))
            .andExpect(jsonPath("$.abstrct").value(DEFAULT_ABSTRCT.booleanValue()))
            .andExpect(jsonPath("$.datatype").value(DEFAULT_DATATYPE.toString()))
            .andExpect(jsonPath("$.iord").value(DEFAULT_IORD.toString()))
            .andExpect(jsonPath("$.crdr").value(DEFAULT_CRDR.toString()))
            .andExpect(jsonPath("$.tlabel").value(DEFAULT_TLABEL.toString()))
            .andExpect(jsonPath("$.doc").value(DEFAULT_DOC.toString()));
    }

    @Test
    @Transactional
    public void getAllTagsByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where tag equals to DEFAULT_TAG
        defaultTagShouldBeFound("tag.equals=" + DEFAULT_TAG);

        // Get all the tagList where tag equals to UPDATED_TAG
        defaultTagShouldNotBeFound("tag.equals=" + UPDATED_TAG);
    }

    @Test
    @Transactional
    public void getAllTagsByTagIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where tag in DEFAULT_TAG or UPDATED_TAG
        defaultTagShouldBeFound("tag.in=" + DEFAULT_TAG + "," + UPDATED_TAG);

        // Get all the tagList where tag equals to UPDATED_TAG
        defaultTagShouldNotBeFound("tag.in=" + UPDATED_TAG);
    }

    @Test
    @Transactional
    public void getAllTagsByTagIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where tag is not null
        defaultTagShouldBeFound("tag.specified=true");

        // Get all the tagList where tag is null
        defaultTagShouldNotBeFound("tag.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where version equals to DEFAULT_VERSION
        defaultTagShouldBeFound("version.equals=" + DEFAULT_VERSION);

        // Get all the tagList where version equals to UPDATED_VERSION
        defaultTagShouldNotBeFound("version.equals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllTagsByVersionIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where version in DEFAULT_VERSION or UPDATED_VERSION
        defaultTagShouldBeFound("version.in=" + DEFAULT_VERSION + "," + UPDATED_VERSION);

        // Get all the tagList where version equals to UPDATED_VERSION
        defaultTagShouldNotBeFound("version.in=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllTagsByVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where version is not null
        defaultTagShouldBeFound("version.specified=true");

        // Get all the tagList where version is null
        defaultTagShouldNotBeFound("version.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByCustomIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where custom equals to DEFAULT_CUSTOM
        defaultTagShouldBeFound("custom.equals=" + DEFAULT_CUSTOM);

        // Get all the tagList where custom equals to UPDATED_CUSTOM
        defaultTagShouldNotBeFound("custom.equals=" + UPDATED_CUSTOM);
    }

    @Test
    @Transactional
    public void getAllTagsByCustomIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where custom in DEFAULT_CUSTOM or UPDATED_CUSTOM
        defaultTagShouldBeFound("custom.in=" + DEFAULT_CUSTOM + "," + UPDATED_CUSTOM);

        // Get all the tagList where custom equals to UPDATED_CUSTOM
        defaultTagShouldNotBeFound("custom.in=" + UPDATED_CUSTOM);
    }

    @Test
    @Transactional
    public void getAllTagsByCustomIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where custom is not null
        defaultTagShouldBeFound("custom.specified=true");

        // Get all the tagList where custom is null
        defaultTagShouldNotBeFound("custom.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByAbstrctIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where abstrct equals to DEFAULT_ABSTRCT
        defaultTagShouldBeFound("abstrct.equals=" + DEFAULT_ABSTRCT);

        // Get all the tagList where abstrct equals to UPDATED_ABSTRCT
        defaultTagShouldNotBeFound("abstrct.equals=" + UPDATED_ABSTRCT);
    }

    @Test
    @Transactional
    public void getAllTagsByAbstrctIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where abstrct in DEFAULT_ABSTRCT or UPDATED_ABSTRCT
        defaultTagShouldBeFound("abstrct.in=" + DEFAULT_ABSTRCT + "," + UPDATED_ABSTRCT);

        // Get all the tagList where abstrct equals to UPDATED_ABSTRCT
        defaultTagShouldNotBeFound("abstrct.in=" + UPDATED_ABSTRCT);
    }

    @Test
    @Transactional
    public void getAllTagsByAbstrctIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where abstrct is not null
        defaultTagShouldBeFound("abstrct.specified=true");

        // Get all the tagList where abstrct is null
        defaultTagShouldNotBeFound("abstrct.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByDatatypeIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where datatype equals to DEFAULT_DATATYPE
        defaultTagShouldBeFound("datatype.equals=" + DEFAULT_DATATYPE);

        // Get all the tagList where datatype equals to UPDATED_DATATYPE
        defaultTagShouldNotBeFound("datatype.equals=" + UPDATED_DATATYPE);
    }

    @Test
    @Transactional
    public void getAllTagsByDatatypeIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where datatype in DEFAULT_DATATYPE or UPDATED_DATATYPE
        defaultTagShouldBeFound("datatype.in=" + DEFAULT_DATATYPE + "," + UPDATED_DATATYPE);

        // Get all the tagList where datatype equals to UPDATED_DATATYPE
        defaultTagShouldNotBeFound("datatype.in=" + UPDATED_DATATYPE);
    }

    @Test
    @Transactional
    public void getAllTagsByDatatypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where datatype is not null
        defaultTagShouldBeFound("datatype.specified=true");

        // Get all the tagList where datatype is null
        defaultTagShouldNotBeFound("datatype.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByIordIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where iord equals to DEFAULT_IORD
        defaultTagShouldBeFound("iord.equals=" + DEFAULT_IORD);

        // Get all the tagList where iord equals to UPDATED_IORD
        defaultTagShouldNotBeFound("iord.equals=" + UPDATED_IORD);
    }

    @Test
    @Transactional
    public void getAllTagsByIordIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where iord in DEFAULT_IORD or UPDATED_IORD
        defaultTagShouldBeFound("iord.in=" + DEFAULT_IORD + "," + UPDATED_IORD);

        // Get all the tagList where iord equals to UPDATED_IORD
        defaultTagShouldNotBeFound("iord.in=" + UPDATED_IORD);
    }

    @Test
    @Transactional
    public void getAllTagsByIordIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where iord is not null
        defaultTagShouldBeFound("iord.specified=true");

        // Get all the tagList where iord is null
        defaultTagShouldNotBeFound("iord.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByCrdrIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where crdr equals to DEFAULT_CRDR
        defaultTagShouldBeFound("crdr.equals=" + DEFAULT_CRDR);

        // Get all the tagList where crdr equals to UPDATED_CRDR
        defaultTagShouldNotBeFound("crdr.equals=" + UPDATED_CRDR);
    }

    @Test
    @Transactional
    public void getAllTagsByCrdrIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where crdr in DEFAULT_CRDR or UPDATED_CRDR
        defaultTagShouldBeFound("crdr.in=" + DEFAULT_CRDR + "," + UPDATED_CRDR);

        // Get all the tagList where crdr equals to UPDATED_CRDR
        defaultTagShouldNotBeFound("crdr.in=" + UPDATED_CRDR);
    }

    @Test
    @Transactional
    public void getAllTagsByCrdrIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where crdr is not null
        defaultTagShouldBeFound("crdr.specified=true");

        // Get all the tagList where crdr is null
        defaultTagShouldNotBeFound("crdr.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByTlabelIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where tlabel equals to DEFAULT_TLABEL
        defaultTagShouldBeFound("tlabel.equals=" + DEFAULT_TLABEL);

        // Get all the tagList where tlabel equals to UPDATED_TLABEL
        defaultTagShouldNotBeFound("tlabel.equals=" + UPDATED_TLABEL);
    }

    @Test
    @Transactional
    public void getAllTagsByTlabelIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where tlabel in DEFAULT_TLABEL or UPDATED_TLABEL
        defaultTagShouldBeFound("tlabel.in=" + DEFAULT_TLABEL + "," + UPDATED_TLABEL);

        // Get all the tagList where tlabel equals to UPDATED_TLABEL
        defaultTagShouldNotBeFound("tlabel.in=" + UPDATED_TLABEL);
    }

    @Test
    @Transactional
    public void getAllTagsByTlabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where tlabel is not null
        defaultTagShouldBeFound("tlabel.specified=true");

        // Get all the tagList where tlabel is null
        defaultTagShouldNotBeFound("tlabel.specified=false");
    }

    @Test
    @Transactional
    public void getAllTagsByDocIsEqualToSomething() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where doc equals to DEFAULT_DOC
        defaultTagShouldBeFound("doc.equals=" + DEFAULT_DOC);

        // Get all the tagList where doc equals to UPDATED_DOC
        defaultTagShouldNotBeFound("doc.equals=" + UPDATED_DOC);
    }

    @Test
    @Transactional
    public void getAllTagsByDocIsInShouldWork() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where doc in DEFAULT_DOC or UPDATED_DOC
        defaultTagShouldBeFound("doc.in=" + DEFAULT_DOC + "," + UPDATED_DOC);

        // Get all the tagList where doc equals to UPDATED_DOC
        defaultTagShouldNotBeFound("doc.in=" + UPDATED_DOC);
    }

    @Test
    @Transactional
    public void getAllTagsByDocIsNullOrNotNull() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        // Get all the tagList where doc is not null
        defaultTagShouldBeFound("doc.specified=true");

        // Get all the tagList where doc is null
        defaultTagShouldNotBeFound("doc.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTagShouldBeFound(String filter) throws Exception {
        restTagMockMvc.perform(get("/api/tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tag.getId().intValue())))
            .andExpect(jsonPath("$.[*].tag").value(hasItem(DEFAULT_TAG)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION)))
            .andExpect(jsonPath("$.[*].custom").value(hasItem(DEFAULT_CUSTOM.booleanValue())))
            .andExpect(jsonPath("$.[*].abstrct").value(hasItem(DEFAULT_ABSTRCT.booleanValue())))
            .andExpect(jsonPath("$.[*].datatype").value(hasItem(DEFAULT_DATATYPE)))
            .andExpect(jsonPath("$.[*].iord").value(hasItem(DEFAULT_IORD)))
            .andExpect(jsonPath("$.[*].crdr").value(hasItem(DEFAULT_CRDR)))
            .andExpect(jsonPath("$.[*].tlabel").value(hasItem(DEFAULT_TLABEL)))
            .andExpect(jsonPath("$.[*].doc").value(hasItem(DEFAULT_DOC)));

        // Check, that the count call also returns 1
        restTagMockMvc.perform(get("/api/tags/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTagShouldNotBeFound(String filter) throws Exception {
        restTagMockMvc.perform(get("/api/tags?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTagMockMvc.perform(get("/api/tags/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTag() throws Exception {
        // Get the tag
        restTagMockMvc.perform(get("/api/tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTag() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        int databaseSizeBeforeUpdate = tagRepository.findAll().size();

        // Update the tag
        Tag updatedTag = tagRepository.findById(tag.getId()).get();
        // Disconnect from session so that the updates on updatedTag are not directly saved in db
        em.detach(updatedTag);
        updatedTag
            .tag(UPDATED_TAG)
            .version(UPDATED_VERSION)
            .custom(UPDATED_CUSTOM)
            .abstrct(UPDATED_ABSTRCT)
            .datatype(UPDATED_DATATYPE)
            .iord(UPDATED_IORD)
            .crdr(UPDATED_CRDR)
            .tlabel(UPDATED_TLABEL)
            .doc(UPDATED_DOC);
        TagDTO tagDTO = tagMapper.toDto(updatedTag);

        restTagMockMvc.perform(put("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isOk());

        // Validate the Tag in the database
        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeUpdate);
        Tag testTag = tagList.get(tagList.size() - 1);
        assertThat(testTag.getTag()).isEqualTo(UPDATED_TAG);
        assertThat(testTag.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testTag.isCustom()).isEqualTo(UPDATED_CUSTOM);
        assertThat(testTag.isAbstrct()).isEqualTo(UPDATED_ABSTRCT);
        assertThat(testTag.getDatatype()).isEqualTo(UPDATED_DATATYPE);
        assertThat(testTag.getIord()).isEqualTo(UPDATED_IORD);
        assertThat(testTag.getCrdr()).isEqualTo(UPDATED_CRDR);
        assertThat(testTag.getTlabel()).isEqualTo(UPDATED_TLABEL);
        assertThat(testTag.getDoc()).isEqualTo(UPDATED_DOC);
    }

    @Test
    @Transactional
    public void updateNonExistingTag() throws Exception {
        int databaseSizeBeforeUpdate = tagRepository.findAll().size();

        // Create the Tag
        TagDTO tagDTO = tagMapper.toDto(tag);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTagMockMvc.perform(put("/api/tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tagDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Tag in the database
        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTag() throws Exception {
        // Initialize the database
        tagRepository.saveAndFlush(tag);

        int databaseSizeBeforeDelete = tagRepository.findAll().size();

        // Delete the tag
        restTagMockMvc.perform(delete("/api/tags/{id}", tag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Tag> tagList = tagRepository.findAll();
        assertThat(tagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tag.class);
        Tag tag1 = new Tag();
        tag1.setId(1L);
        Tag tag2 = new Tag();
        tag2.setId(tag1.getId());
        assertThat(tag1).isEqualTo(tag2);
        tag2.setId(2L);
        assertThat(tag1).isNotEqualTo(tag2);
        tag1.setId(null);
        assertThat(tag1).isNotEqualTo(tag2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TagDTO.class);
        TagDTO tagDTO1 = new TagDTO();
        tagDTO1.setId(1L);
        TagDTO tagDTO2 = new TagDTO();
        assertThat(tagDTO1).isNotEqualTo(tagDTO2);
        tagDTO2.setId(tagDTO1.getId());
        assertThat(tagDTO1).isEqualTo(tagDTO2);
        tagDTO2.setId(2L);
        assertThat(tagDTO1).isNotEqualTo(tagDTO2);
        tagDTO1.setId(null);
        assertThat(tagDTO1).isNotEqualTo(tagDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(tagMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(tagMapper.fromId(null)).isNull();
    }
}
