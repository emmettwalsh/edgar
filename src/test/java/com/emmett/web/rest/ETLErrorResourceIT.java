package com.emmett.web.rest;

import com.emmett.EdgarApp;
import com.emmett.domain.ETLError;
import com.emmett.repository.ETLErrorRepository;
import com.emmett.service.ETLErrorService;
import com.emmett.service.dto.ETLErrorDTO;
import com.emmett.service.mapper.ETLErrorMapper;
import com.emmett.web.rest.errors.ExceptionTranslator;
import com.emmett.service.dto.ETLErrorCriteria;
import com.emmett.service.ETLErrorQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.emmett.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.emmett.domain.enumeration.EtlErrorType;
/**
 * Integration tests for the {@Link ETLErrorResource} REST controller.
 */
@SpringBootTest(classes = EdgarApp.class)
public class ETLErrorResourceIT {

    private static final String DEFAULT_DOMAIN = "AAAAAAAAAA";
    private static final String UPDATED_DOMAIN = "BBBBBBBBBB";

    private static final EtlErrorType DEFAULT_TYPE = EtlErrorType.Extract;
    private static final EtlErrorType UPDATED_TYPE = EtlErrorType.Transform;

    private static final String DEFAULT_YEAR_AND_QUARTER = "AAAAAAAA";
    private static final String UPDATED_YEAR_AND_QUARTER = "BBBBBBBB";

    private static final String DEFAULT_INPUT = "AAAAAAAAAA";
    private static final String UPDATED_INPUT = "BBBBBBBBBB";

    private static final String DEFAULT_OUTPUT = "AAAAAAAAAA";
    private static final String UPDATED_OUTPUT = "BBBBBBBBBB";

    @Autowired
    private ETLErrorRepository eTLErrorRepository;

    @Autowired
    private ETLErrorMapper eTLErrorMapper;

    @Autowired
    private ETLErrorService eTLErrorService;

    @Autowired
    private ETLErrorQueryService eTLErrorQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restETLErrorMockMvc;

    private ETLError eTLError;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ETLErrorResource eTLErrorResource = new ETLErrorResource(eTLErrorService, eTLErrorQueryService);
        this.restETLErrorMockMvc = MockMvcBuilders.standaloneSetup(eTLErrorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ETLError createEntity(EntityManager em) {
        ETLError eTLError = new ETLError()
            .domain(DEFAULT_DOMAIN)
            .type(DEFAULT_TYPE)
            .yearAndQuarter(DEFAULT_YEAR_AND_QUARTER)
            .input(DEFAULT_INPUT)
            .output(DEFAULT_OUTPUT);
        return eTLError;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ETLError createUpdatedEntity(EntityManager em) {
        ETLError eTLError = new ETLError()
            .domain(UPDATED_DOMAIN)
            .type(UPDATED_TYPE)
            .yearAndQuarter(UPDATED_YEAR_AND_QUARTER)
            .input(UPDATED_INPUT)
            .output(UPDATED_OUTPUT);
        return eTLError;
    }

    @BeforeEach
    public void initTest() {
        eTLError = createEntity(em);
    }

    @Test
    @Transactional
    public void createETLError() throws Exception {
        int databaseSizeBeforeCreate = eTLErrorRepository.findAll().size();

        // Create the ETLError
        ETLErrorDTO eTLErrorDTO = eTLErrorMapper.toDto(eTLError);
        restETLErrorMockMvc.perform(post("/api/etl-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eTLErrorDTO)))
            .andExpect(status().isCreated());

        // Validate the ETLError in the database
        List<ETLError> eTLErrorList = eTLErrorRepository.findAll();
        assertThat(eTLErrorList).hasSize(databaseSizeBeforeCreate + 1);
        ETLError testETLError = eTLErrorList.get(eTLErrorList.size() - 1);
        assertThat(testETLError.getDomain()).isEqualTo(DEFAULT_DOMAIN);
        assertThat(testETLError.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testETLError.getYearAndQuarter()).isEqualTo(DEFAULT_YEAR_AND_QUARTER);
        assertThat(testETLError.getInput()).isEqualTo(DEFAULT_INPUT);
        assertThat(testETLError.getOutput()).isEqualTo(DEFAULT_OUTPUT);
    }

    @Test
    @Transactional
    public void createETLErrorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = eTLErrorRepository.findAll().size();

        // Create the ETLError with an existing ID
        eTLError.setId(1L);
        ETLErrorDTO eTLErrorDTO = eTLErrorMapper.toDto(eTLError);

        // An entity with an existing ID cannot be created, so this API call must fail
        restETLErrorMockMvc.perform(post("/api/etl-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eTLErrorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ETLError in the database
        List<ETLError> eTLErrorList = eTLErrorRepository.findAll();
        assertThat(eTLErrorList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkDomainIsRequired() throws Exception {
        int databaseSizeBeforeTest = eTLErrorRepository.findAll().size();
        // set the field null
        eTLError.setDomain(null);

        // Create the ETLError, which fails.
        ETLErrorDTO eTLErrorDTO = eTLErrorMapper.toDto(eTLError);

        restETLErrorMockMvc.perform(post("/api/etl-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eTLErrorDTO)))
            .andExpect(status().isBadRequest());

        List<ETLError> eTLErrorList = eTLErrorRepository.findAll();
        assertThat(eTLErrorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = eTLErrorRepository.findAll().size();
        // set the field null
        eTLError.setType(null);

        // Create the ETLError, which fails.
        ETLErrorDTO eTLErrorDTO = eTLErrorMapper.toDto(eTLError);

        restETLErrorMockMvc.perform(post("/api/etl-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eTLErrorDTO)))
            .andExpect(status().isBadRequest());

        List<ETLError> eTLErrorList = eTLErrorRepository.findAll();
        assertThat(eTLErrorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllETLErrors() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList
        restETLErrorMockMvc.perform(get("/api/etl-errors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eTLError.getId().intValue())))
            .andExpect(jsonPath("$.[*].domain").value(hasItem(DEFAULT_DOMAIN.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].yearAndQuarter").value(hasItem(DEFAULT_YEAR_AND_QUARTER.toString())))
            .andExpect(jsonPath("$.[*].input").value(hasItem(DEFAULT_INPUT.toString())))
            .andExpect(jsonPath("$.[*].output").value(hasItem(DEFAULT_OUTPUT.toString())));
    }
    
    @Test
    @Transactional
    public void getETLError() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get the eTLError
        restETLErrorMockMvc.perform(get("/api/etl-errors/{id}", eTLError.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(eTLError.getId().intValue()))
            .andExpect(jsonPath("$.domain").value(DEFAULT_DOMAIN.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.yearAndQuarter").value(DEFAULT_YEAR_AND_QUARTER.toString()))
            .andExpect(jsonPath("$.input").value(DEFAULT_INPUT.toString()))
            .andExpect(jsonPath("$.output").value(DEFAULT_OUTPUT.toString()));
    }

    @Test
    @Transactional
    public void getAllETLErrorsByDomainIsEqualToSomething() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where domain equals to DEFAULT_DOMAIN
        defaultETLErrorShouldBeFound("domain.equals=" + DEFAULT_DOMAIN);

        // Get all the eTLErrorList where domain equals to UPDATED_DOMAIN
        defaultETLErrorShouldNotBeFound("domain.equals=" + UPDATED_DOMAIN);
    }

    @Test
    @Transactional
    public void getAllETLErrorsByDomainIsInShouldWork() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where domain in DEFAULT_DOMAIN or UPDATED_DOMAIN
        defaultETLErrorShouldBeFound("domain.in=" + DEFAULT_DOMAIN + "," + UPDATED_DOMAIN);

        // Get all the eTLErrorList where domain equals to UPDATED_DOMAIN
        defaultETLErrorShouldNotBeFound("domain.in=" + UPDATED_DOMAIN);
    }

    @Test
    @Transactional
    public void getAllETLErrorsByDomainIsNullOrNotNull() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where domain is not null
        defaultETLErrorShouldBeFound("domain.specified=true");

        // Get all the eTLErrorList where domain is null
        defaultETLErrorShouldNotBeFound("domain.specified=false");
    }

    @Test
    @Transactional
    public void getAllETLErrorsByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where type equals to DEFAULT_TYPE
        defaultETLErrorShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the eTLErrorList where type equals to UPDATED_TYPE
        defaultETLErrorShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllETLErrorsByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultETLErrorShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the eTLErrorList where type equals to UPDATED_TYPE
        defaultETLErrorShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    public void getAllETLErrorsByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where type is not null
        defaultETLErrorShouldBeFound("type.specified=true");

        // Get all the eTLErrorList where type is null
        defaultETLErrorShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    public void getAllETLErrorsByYearAndQuarterIsEqualToSomething() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where yearAndQuarter equals to DEFAULT_YEAR_AND_QUARTER
        defaultETLErrorShouldBeFound("yearAndQuarter.equals=" + DEFAULT_YEAR_AND_QUARTER);

        // Get all the eTLErrorList where yearAndQuarter equals to UPDATED_YEAR_AND_QUARTER
        defaultETLErrorShouldNotBeFound("yearAndQuarter.equals=" + UPDATED_YEAR_AND_QUARTER);
    }

    @Test
    @Transactional
    public void getAllETLErrorsByYearAndQuarterIsInShouldWork() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where yearAndQuarter in DEFAULT_YEAR_AND_QUARTER or UPDATED_YEAR_AND_QUARTER
        defaultETLErrorShouldBeFound("yearAndQuarter.in=" + DEFAULT_YEAR_AND_QUARTER + "," + UPDATED_YEAR_AND_QUARTER);

        // Get all the eTLErrorList where yearAndQuarter equals to UPDATED_YEAR_AND_QUARTER
        defaultETLErrorShouldNotBeFound("yearAndQuarter.in=" + UPDATED_YEAR_AND_QUARTER);
    }

    @Test
    @Transactional
    public void getAllETLErrorsByYearAndQuarterIsNullOrNotNull() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        // Get all the eTLErrorList where yearAndQuarter is not null
        defaultETLErrorShouldBeFound("yearAndQuarter.specified=true");

        // Get all the eTLErrorList where yearAndQuarter is null
        defaultETLErrorShouldNotBeFound("yearAndQuarter.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultETLErrorShouldBeFound(String filter) throws Exception {
        restETLErrorMockMvc.perform(get("/api/etl-errors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(eTLError.getId().intValue())))
            .andExpect(jsonPath("$.[*].domain").value(hasItem(DEFAULT_DOMAIN)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].yearAndQuarter").value(hasItem(DEFAULT_YEAR_AND_QUARTER)))
            .andExpect(jsonPath("$.[*].input").value(hasItem(DEFAULT_INPUT.toString())))
            .andExpect(jsonPath("$.[*].output").value(hasItem(DEFAULT_OUTPUT.toString())));

        // Check, that the count call also returns 1
        restETLErrorMockMvc.perform(get("/api/etl-errors/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultETLErrorShouldNotBeFound(String filter) throws Exception {
        restETLErrorMockMvc.perform(get("/api/etl-errors?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restETLErrorMockMvc.perform(get("/api/etl-errors/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingETLError() throws Exception {
        // Get the eTLError
        restETLErrorMockMvc.perform(get("/api/etl-errors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateETLError() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        int databaseSizeBeforeUpdate = eTLErrorRepository.findAll().size();

        // Update the eTLError
        ETLError updatedETLError = eTLErrorRepository.findById(eTLError.getId()).get();
        // Disconnect from session so that the updates on updatedETLError are not directly saved in db
        em.detach(updatedETLError);
        updatedETLError
            .domain(UPDATED_DOMAIN)
            .type(UPDATED_TYPE)
            .yearAndQuarter(UPDATED_YEAR_AND_QUARTER)
            .input(UPDATED_INPUT)
            .output(UPDATED_OUTPUT);
        ETLErrorDTO eTLErrorDTO = eTLErrorMapper.toDto(updatedETLError);

        restETLErrorMockMvc.perform(put("/api/etl-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eTLErrorDTO)))
            .andExpect(status().isOk());

        // Validate the ETLError in the database
        List<ETLError> eTLErrorList = eTLErrorRepository.findAll();
        assertThat(eTLErrorList).hasSize(databaseSizeBeforeUpdate);
        ETLError testETLError = eTLErrorList.get(eTLErrorList.size() - 1);
        assertThat(testETLError.getDomain()).isEqualTo(UPDATED_DOMAIN);
        assertThat(testETLError.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testETLError.getYearAndQuarter()).isEqualTo(UPDATED_YEAR_AND_QUARTER);
        assertThat(testETLError.getInput()).isEqualTo(UPDATED_INPUT);
        assertThat(testETLError.getOutput()).isEqualTo(UPDATED_OUTPUT);
    }

    @Test
    @Transactional
    public void updateNonExistingETLError() throws Exception {
        int databaseSizeBeforeUpdate = eTLErrorRepository.findAll().size();

        // Create the ETLError
        ETLErrorDTO eTLErrorDTO = eTLErrorMapper.toDto(eTLError);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restETLErrorMockMvc.perform(put("/api/etl-errors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(eTLErrorDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ETLError in the database
        List<ETLError> eTLErrorList = eTLErrorRepository.findAll();
        assertThat(eTLErrorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteETLError() throws Exception {
        // Initialize the database
        eTLErrorRepository.saveAndFlush(eTLError);

        int databaseSizeBeforeDelete = eTLErrorRepository.findAll().size();

        // Delete the eTLError
        restETLErrorMockMvc.perform(delete("/api/etl-errors/{id}", eTLError.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ETLError> eTLErrorList = eTLErrorRepository.findAll();
        assertThat(eTLErrorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ETLError.class);
        ETLError eTLError1 = new ETLError();
        eTLError1.setId(1L);
        ETLError eTLError2 = new ETLError();
        eTLError2.setId(eTLError1.getId());
        assertThat(eTLError1).isEqualTo(eTLError2);
        eTLError2.setId(2L);
        assertThat(eTLError1).isNotEqualTo(eTLError2);
        eTLError1.setId(null);
        assertThat(eTLError1).isNotEqualTo(eTLError2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ETLErrorDTO.class);
        ETLErrorDTO eTLErrorDTO1 = new ETLErrorDTO();
        eTLErrorDTO1.setId(1L);
        ETLErrorDTO eTLErrorDTO2 = new ETLErrorDTO();
        assertThat(eTLErrorDTO1).isNotEqualTo(eTLErrorDTO2);
        eTLErrorDTO2.setId(eTLErrorDTO1.getId());
        assertThat(eTLErrorDTO1).isEqualTo(eTLErrorDTO2);
        eTLErrorDTO2.setId(2L);
        assertThat(eTLErrorDTO1).isNotEqualTo(eTLErrorDTO2);
        eTLErrorDTO1.setId(null);
        assertThat(eTLErrorDTO1).isNotEqualTo(eTLErrorDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(eTLErrorMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(eTLErrorMapper.fromId(null)).isNull();
    }
}
