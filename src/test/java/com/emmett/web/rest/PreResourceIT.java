package com.emmett.web.rest;

import com.emmett.EdgarApp;
import com.emmett.domain.Pre;
import com.emmett.domain.Sub;
import com.emmett.domain.Tag;
import com.emmett.repository.PreRepository;
import com.emmett.service.PreService;
import com.emmett.service.dto.PreDTO;
import com.emmett.service.mapper.PreMapper;
import com.emmett.web.rest.errors.ExceptionTranslator;
import com.emmett.service.dto.PreCriteria;
import com.emmett.service.PreQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.emmett.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link PreResource} REST controller.
 */
@SpringBootTest(classes = EdgarApp.class)
public class PreResourceIT {

    private static final Integer DEFAULT_REPORT = 999999;
    private static final Integer UPDATED_REPORT = 999998;

    private static final Integer DEFAULT_LINE = 999999;
    private static final Integer UPDATED_LINE = 999998;

    private static final String DEFAULT_STATEMENT = "AA";
    private static final String UPDATED_STATEMENT = "BB";

    private static final Boolean DEFAULT_INPTH = false;
    private static final Boolean UPDATED_INPTH = true;

    private static final String DEFAULT_RFILE = "A";
    private static final String UPDATED_RFILE = "B";

    private static final String DEFAULT_PLABEL = "AAAAAAAAAA";
    private static final String UPDATED_PLABEL = "BBBBBBBBBB";

    @Autowired
    private PreRepository preRepository;

    @Autowired
    private PreMapper preMapper;

    @Autowired
    private PreService preService;

    @Autowired
    private PreQueryService preQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPreMockMvc;

    private Pre pre;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PreResource preResource = new PreResource(preService, preQueryService);
        this.restPreMockMvc = MockMvcBuilders.standaloneSetup(preResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pre createEntity(EntityManager em) {
        Pre pre = new Pre()
            .report(DEFAULT_REPORT)
            .line(DEFAULT_LINE)
            .statement(DEFAULT_STATEMENT)
            .inpth(DEFAULT_INPTH)
            .rfile(DEFAULT_RFILE)
            .plabel(DEFAULT_PLABEL);
        return pre;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Pre createUpdatedEntity(EntityManager em) {
        Pre pre = new Pre()
            .report(UPDATED_REPORT)
            .line(UPDATED_LINE)
            .statement(UPDATED_STATEMENT)
            .inpth(UPDATED_INPTH)
            .rfile(UPDATED_RFILE)
            .plabel(UPDATED_PLABEL);
        return pre;
    }

    @BeforeEach
    public void initTest() {
        pre = createEntity(em);
    }

    @Test
    @Transactional
    public void createPre() throws Exception {
        int databaseSizeBeforeCreate = preRepository.findAll().size();

        // Create the Pre
        PreDTO preDTO = preMapper.toDto(pre);
        restPreMockMvc.perform(post("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isCreated());

        // Validate the Pre in the database
        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeCreate + 1);
        Pre testPre = preList.get(preList.size() - 1);
        assertThat(testPre.getReport()).isEqualTo(DEFAULT_REPORT);
        assertThat(testPre.getLine()).isEqualTo(DEFAULT_LINE);
        assertThat(testPre.getStatement()).isEqualTo(DEFAULT_STATEMENT);
        assertThat(testPre.isInpth()).isEqualTo(DEFAULT_INPTH);
        assertThat(testPre.getRfile()).isEqualTo(DEFAULT_RFILE);
        assertThat(testPre.getPlabel()).isEqualTo(DEFAULT_PLABEL);
    }

    @Test
    @Transactional
    public void createPreWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = preRepository.findAll().size();

        // Create the Pre with an existing ID
        pre.setId(1L);
        PreDTO preDTO = preMapper.toDto(pre);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPreMockMvc.perform(post("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pre in the database
        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkStatementIsRequired() throws Exception {
        int databaseSizeBeforeTest = preRepository.findAll().size();
        // set the field null
        pre.setStatement(null);

        // Create the Pre, which fails.
        PreDTO preDTO = preMapper.toDto(pre);

        restPreMockMvc.perform(post("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isBadRequest());

        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInpthIsRequired() throws Exception {
        int databaseSizeBeforeTest = preRepository.findAll().size();
        // set the field null
        pre.setInpth(null);

        // Create the Pre, which fails.
        PreDTO preDTO = preMapper.toDto(pre);

        restPreMockMvc.perform(post("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isBadRequest());

        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRfileIsRequired() throws Exception {
        int databaseSizeBeforeTest = preRepository.findAll().size();
        // set the field null
        pre.setRfile(null);

        // Create the Pre, which fails.
        PreDTO preDTO = preMapper.toDto(pre);

        restPreMockMvc.perform(post("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isBadRequest());

        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPlabelIsRequired() throws Exception {
        int databaseSizeBeforeTest = preRepository.findAll().size();
        // set the field null
        pre.setPlabel(null);

        // Create the Pre, which fails.
        PreDTO preDTO = preMapper.toDto(pre);

        restPreMockMvc.perform(post("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isBadRequest());

        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPres() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList
        restPreMockMvc.perform(get("/api/pres?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pre.getId().intValue())))
            .andExpect(jsonPath("$.[*].report").value(hasItem(DEFAULT_REPORT)))
            .andExpect(jsonPath("$.[*].line").value(hasItem(DEFAULT_LINE)))
            .andExpect(jsonPath("$.[*].statement").value(hasItem(DEFAULT_STATEMENT.toString())))
            .andExpect(jsonPath("$.[*].inpth").value(hasItem(DEFAULT_INPTH.booleanValue())))
            .andExpect(jsonPath("$.[*].rfile").value(hasItem(DEFAULT_RFILE.toString())))
            .andExpect(jsonPath("$.[*].plabel").value(hasItem(DEFAULT_PLABEL.toString())));
    }
    
    @Test
    @Transactional
    public void getPre() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get the pre
        restPreMockMvc.perform(get("/api/pres/{id}", pre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(pre.getId().intValue()))
            .andExpect(jsonPath("$.report").value(DEFAULT_REPORT))
            .andExpect(jsonPath("$.line").value(DEFAULT_LINE))
            .andExpect(jsonPath("$.statement").value(DEFAULT_STATEMENT.toString()))
            .andExpect(jsonPath("$.inpth").value(DEFAULT_INPTH.booleanValue()))
            .andExpect(jsonPath("$.rfile").value(DEFAULT_RFILE.toString()))
            .andExpect(jsonPath("$.plabel").value(DEFAULT_PLABEL.toString()));
    }

    @Test
    @Transactional
    public void getAllPresByReportIsEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where report equals to DEFAULT_REPORT
        defaultPreShouldBeFound("report.equals=" + DEFAULT_REPORT);

        // Get all the preList where report equals to UPDATED_REPORT
        defaultPreShouldNotBeFound("report.equals=" + UPDATED_REPORT);
    }

    @Test
    @Transactional
    public void getAllPresByReportIsInShouldWork() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where report in DEFAULT_REPORT or UPDATED_REPORT
        defaultPreShouldBeFound("report.in=" + DEFAULT_REPORT + "," + UPDATED_REPORT);

        // Get all the preList where report equals to UPDATED_REPORT
        defaultPreShouldNotBeFound("report.in=" + UPDATED_REPORT);
    }

    @Test
    @Transactional
    public void getAllPresByReportIsNullOrNotNull() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where report is not null
        defaultPreShouldBeFound("report.specified=true");

        // Get all the preList where report is null
        defaultPreShouldNotBeFound("report.specified=false");
    }

    @Test
    @Transactional
    public void getAllPresByReportIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where report greater than or equals to DEFAULT_REPORT
        defaultPreShouldBeFound("report.greaterOrEqualThan=" + DEFAULT_REPORT);

        // Get all the preList where report greater than or equals to (DEFAULT_REPORT + 1)
        defaultPreShouldNotBeFound("report.greaterOrEqualThan=" + (DEFAULT_REPORT + 1));
    }

    @Test
    @Transactional
    public void getAllPresByReportIsLessThanSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where report less than or equals to DEFAULT_REPORT
        defaultPreShouldNotBeFound("report.lessThan=" + DEFAULT_REPORT);

        // Get all the preList where report less than or equals to (DEFAULT_REPORT + 1)
        defaultPreShouldBeFound("report.lessThan=" + (DEFAULT_REPORT + 1));
    }


    @Test
    @Transactional
    public void getAllPresByLineIsEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where line equals to DEFAULT_LINE
        defaultPreShouldBeFound("line.equals=" + DEFAULT_LINE);

        // Get all the preList where line equals to UPDATED_LINE
        defaultPreShouldNotBeFound("line.equals=" + UPDATED_LINE);
    }

    @Test
    @Transactional
    public void getAllPresByLineIsInShouldWork() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where line in DEFAULT_LINE or UPDATED_LINE
        defaultPreShouldBeFound("line.in=" + DEFAULT_LINE + "," + UPDATED_LINE);

        // Get all the preList where line equals to UPDATED_LINE
        defaultPreShouldNotBeFound("line.in=" + UPDATED_LINE);
    }

    @Test
    @Transactional
    public void getAllPresByLineIsNullOrNotNull() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where line is not null
        defaultPreShouldBeFound("line.specified=true");

        // Get all the preList where line is null
        defaultPreShouldNotBeFound("line.specified=false");
    }

    @Test
    @Transactional
    public void getAllPresByLineIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where line greater than or equals to DEFAULT_LINE
        defaultPreShouldBeFound("line.greaterOrEqualThan=" + DEFAULT_LINE);

        // Get all the preList where line greater than or equals to (DEFAULT_LINE + 1)
        defaultPreShouldNotBeFound("line.greaterOrEqualThan=" + (DEFAULT_LINE + 1));
    }

    @Test
    @Transactional
    public void getAllPresByLineIsLessThanSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where line less than or equals to DEFAULT_LINE
        defaultPreShouldNotBeFound("line.lessThan=" + DEFAULT_LINE);

        // Get all the preList where line less than or equals to (DEFAULT_LINE + 1)
        defaultPreShouldBeFound("line.lessThan=" + (DEFAULT_LINE + 1));
    }


    @Test
    @Transactional
    public void getAllPresByStatementIsEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where statement equals to DEFAULT_STATEMENT
        defaultPreShouldBeFound("statement.equals=" + DEFAULT_STATEMENT);

        // Get all the preList where statement equals to UPDATED_STATEMENT
        defaultPreShouldNotBeFound("statement.equals=" + UPDATED_STATEMENT);
    }

    @Test
    @Transactional
    public void getAllPresByStatementIsInShouldWork() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where statement in DEFAULT_STATEMENT or UPDATED_STATEMENT
        defaultPreShouldBeFound("statement.in=" + DEFAULT_STATEMENT + "," + UPDATED_STATEMENT);

        // Get all the preList where statement equals to UPDATED_STATEMENT
        defaultPreShouldNotBeFound("statement.in=" + UPDATED_STATEMENT);
    }

    @Test
    @Transactional
    public void getAllPresByStatementIsNullOrNotNull() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where statement is not null
        defaultPreShouldBeFound("statement.specified=true");

        // Get all the preList where statement is null
        defaultPreShouldNotBeFound("statement.specified=false");
    }

    @Test
    @Transactional
    public void getAllPresByInpthIsEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where inpth equals to DEFAULT_INPTH
        defaultPreShouldBeFound("inpth.equals=" + DEFAULT_INPTH);

        // Get all the preList where inpth equals to UPDATED_INPTH
        defaultPreShouldNotBeFound("inpth.equals=" + UPDATED_INPTH);
    }

    @Test
    @Transactional
    public void getAllPresByInpthIsInShouldWork() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where inpth in DEFAULT_INPTH or UPDATED_INPTH
        defaultPreShouldBeFound("inpth.in=" + DEFAULT_INPTH + "," + UPDATED_INPTH);

        // Get all the preList where inpth equals to UPDATED_INPTH
        defaultPreShouldNotBeFound("inpth.in=" + UPDATED_INPTH);
    }

    @Test
    @Transactional
    public void getAllPresByInpthIsNullOrNotNull() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where inpth is not null
        defaultPreShouldBeFound("inpth.specified=true");

        // Get all the preList where inpth is null
        defaultPreShouldNotBeFound("inpth.specified=false");
    }

    @Test
    @Transactional
    public void getAllPresByRfileIsEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where rfile equals to DEFAULT_RFILE
        defaultPreShouldBeFound("rfile.equals=" + DEFAULT_RFILE);

        // Get all the preList where rfile equals to UPDATED_RFILE
        defaultPreShouldNotBeFound("rfile.equals=" + UPDATED_RFILE);
    }

    @Test
    @Transactional
    public void getAllPresByRfileIsInShouldWork() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where rfile in DEFAULT_RFILE or UPDATED_RFILE
        defaultPreShouldBeFound("rfile.in=" + DEFAULT_RFILE + "," + UPDATED_RFILE);

        // Get all the preList where rfile equals to UPDATED_RFILE
        defaultPreShouldNotBeFound("rfile.in=" + UPDATED_RFILE);
    }

    @Test
    @Transactional
    public void getAllPresByRfileIsNullOrNotNull() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where rfile is not null
        defaultPreShouldBeFound("rfile.specified=true");

        // Get all the preList where rfile is null
        defaultPreShouldNotBeFound("rfile.specified=false");
    }

    @Test
    @Transactional
    public void getAllPresByPlabelIsEqualToSomething() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where plabel equals to DEFAULT_PLABEL
        defaultPreShouldBeFound("plabel.equals=" + DEFAULT_PLABEL);

        // Get all the preList where plabel equals to UPDATED_PLABEL
        defaultPreShouldNotBeFound("plabel.equals=" + UPDATED_PLABEL);
    }

    @Test
    @Transactional
    public void getAllPresByPlabelIsInShouldWork() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where plabel in DEFAULT_PLABEL or UPDATED_PLABEL
        defaultPreShouldBeFound("plabel.in=" + DEFAULT_PLABEL + "," + UPDATED_PLABEL);

        // Get all the preList where plabel equals to UPDATED_PLABEL
        defaultPreShouldNotBeFound("plabel.in=" + UPDATED_PLABEL);
    }

    @Test
    @Transactional
    public void getAllPresByPlabelIsNullOrNotNull() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        // Get all the preList where plabel is not null
        defaultPreShouldBeFound("plabel.specified=true");

        // Get all the preList where plabel is null
        defaultPreShouldNotBeFound("plabel.specified=false");
    }

    @Test
    @Transactional
    public void getAllPresByAdshIsEqualToSomething() throws Exception {
        // Initialize the database
        Sub adsh = SubResourceIT.createEntity(em);
        em.persist(adsh);
        em.flush();
        pre.setAdsh(adsh);
        preRepository.saveAndFlush(pre);
        Long adshId = adsh.getId();

        // Get all the preList where adsh equals to adshId
        defaultPreShouldBeFound("adshId.equals=" + adshId);

        // Get all the preList where adsh equals to adshId + 1
        defaultPreShouldNotBeFound("adshId.equals=" + (adshId + 1));
    }


    @Test
    @Transactional
    public void getAllPresByTagIsEqualToSomething() throws Exception {
        // Initialize the database
        Tag tag = TagResourceIT.createEntity(em);
        em.persist(tag);
        em.flush();
        pre.setTag(tag);
        preRepository.saveAndFlush(pre);
        Long tagId = tag.getId();

        // Get all the preList where tag equals to tagId
        defaultPreShouldBeFound("tagId.equals=" + tagId);

        // Get all the preList where tag equals to tagId + 1
        defaultPreShouldNotBeFound("tagId.equals=" + (tagId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPreShouldBeFound(String filter) throws Exception {
        restPreMockMvc.perform(get("/api/pres?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(pre.getId().intValue())))
            .andExpect(jsonPath("$.[*].report").value(hasItem(DEFAULT_REPORT)))
            .andExpect(jsonPath("$.[*].line").value(hasItem(DEFAULT_LINE)))
            .andExpect(jsonPath("$.[*].statement").value(hasItem(DEFAULT_STATEMENT)))
            .andExpect(jsonPath("$.[*].inpth").value(hasItem(DEFAULT_INPTH.booleanValue())))
            .andExpect(jsonPath("$.[*].rfile").value(hasItem(DEFAULT_RFILE)))
            .andExpect(jsonPath("$.[*].plabel").value(hasItem(DEFAULT_PLABEL)));

        // Check, that the count call also returns 1
        restPreMockMvc.perform(get("/api/pres/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPreShouldNotBeFound(String filter) throws Exception {
        restPreMockMvc.perform(get("/api/pres?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPreMockMvc.perform(get("/api/pres/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPre() throws Exception {
        // Get the pre
        restPreMockMvc.perform(get("/api/pres/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePre() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        int databaseSizeBeforeUpdate = preRepository.findAll().size();

        // Update the pre
        Pre updatedPre = preRepository.findById(pre.getId()).get();
        // Disconnect from session so that the updates on updatedPre are not directly saved in db
        em.detach(updatedPre);
        updatedPre
            .report(UPDATED_REPORT)
            .line(UPDATED_LINE)
            .statement(UPDATED_STATEMENT)
            .inpth(UPDATED_INPTH)
            .rfile(UPDATED_RFILE)
            .plabel(UPDATED_PLABEL);
        PreDTO preDTO = preMapper.toDto(updatedPre);

        restPreMockMvc.perform(put("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isOk());

        // Validate the Pre in the database
        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeUpdate);
        Pre testPre = preList.get(preList.size() - 1);
        assertThat(testPre.getReport()).isEqualTo(UPDATED_REPORT);
        assertThat(testPre.getLine()).isEqualTo(UPDATED_LINE);
        assertThat(testPre.getStatement()).isEqualTo(UPDATED_STATEMENT);
        assertThat(testPre.isInpth()).isEqualTo(UPDATED_INPTH);
        assertThat(testPre.getRfile()).isEqualTo(UPDATED_RFILE);
        assertThat(testPre.getPlabel()).isEqualTo(UPDATED_PLABEL);
    }

    @Test
    @Transactional
    public void updateNonExistingPre() throws Exception {
        int databaseSizeBeforeUpdate = preRepository.findAll().size();

        // Create the Pre
        PreDTO preDTO = preMapper.toDto(pre);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPreMockMvc.perform(put("/api/pres")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(preDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Pre in the database
        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePre() throws Exception {
        // Initialize the database
        preRepository.saveAndFlush(pre);

        int databaseSizeBeforeDelete = preRepository.findAll().size();

        // Delete the pre
        restPreMockMvc.perform(delete("/api/pres/{id}", pre.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Pre> preList = preRepository.findAll();
        assertThat(preList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Pre.class);
        Pre pre1 = new Pre();
        pre1.setId(1L);
        Pre pre2 = new Pre();
        pre2.setId(pre1.getId());
        assertThat(pre1).isEqualTo(pre2);
        pre2.setId(2L);
        assertThat(pre1).isNotEqualTo(pre2);
        pre1.setId(null);
        assertThat(pre1).isNotEqualTo(pre2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PreDTO.class);
        PreDTO preDTO1 = new PreDTO();
        preDTO1.setId(1L);
        PreDTO preDTO2 = new PreDTO();
        assertThat(preDTO1).isNotEqualTo(preDTO2);
        preDTO2.setId(preDTO1.getId());
        assertThat(preDTO1).isEqualTo(preDTO2);
        preDTO2.setId(2L);
        assertThat(preDTO1).isNotEqualTo(preDTO2);
        preDTO1.setId(null);
        assertThat(preDTO1).isNotEqualTo(preDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(preMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(preMapper.fromId(null)).isNull();
    }
}
