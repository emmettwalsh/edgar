##Running

- install Java 8 first

- clone app cd into top level

- from top level run `./gradlew` to build and start the app

- Navigate to http://localhost:8080/

- Log in with given username and password using top right Account menu

- After logging in, click Entities menu to see Subs, Tags, Nums, Pres and ETL Errors

- View database using Administration -> Database menu (Connect with the pre-populated config)

- View Swagger rest docs using Administration -> API menu

## Notes

- right now it starts an in memory sql database that gets flushed on every restart
- the app reads the edgar files from a folder inside the app for now : `./src/main/resources/edgar-data`
- the app reads the folder when it is starting up (in EdgarApp.java), and runs ETLs on the files and stores the data in the database
- I have just put one edgar folder in there for now and drastically reduced the data

* main ETL code is in `./src/main/java/com/emmett/service/edgar`
